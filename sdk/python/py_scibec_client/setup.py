from distutils.core import setup

setup(
    name="scibec",
    version="1.0",
    description="SciBEC SDK",
    author="Christian Appel",
    author_email="christian.appel@psi.ch",
    packages=["py_scibec_client"],
    install_requires=["requests", "typeguard", "bec_lib"],
)
