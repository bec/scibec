""" HTTP client client"""

import functools

import requests
from bec_lib import bec_logger
from py_scibec_client.authmixin import HEADER_JSON, AuthError, AuthMixin
from requests import HTTPError

logger = bec_logger.logger


def authenticated(func):
    """Decorator to authenticate the user before making the request"""

    @functools.wraps(func)
    def authenticated_call(client, *args, **kwargs):
        """Authenticate the user before making the request

        Args:
            client (HttpClient): The client
        """
        if not isinstance(client, HttpClient):
            raise AttributeError("First argument must be an instance of HttpClient")
        if "headers" in kwargs:
            kwargs["headers"] = kwargs["headers"].copy()
        else:
            kwargs["headers"] = {}
        kwargs["headers"]["Authorization"] = client.token
        return func(client, *args, **kwargs)

    return authenticated_call


def formatted_http_error(func):
    """Decorator to format the HTTP error"""

    @functools.wraps(func)
    def formatted_call(*args, **kwargs):
        try:
            out = func(*args, **kwargs)
        except HTTPError as exc:
            raise HTTPError(f"{exc.response.reason} Error Message: {exc.response.text}") from exc
        return out

    return formatted_call


class HttpClient(AuthMixin):
    """HTTP client for the SciBEC API.

    Args:
        host (str): The host of the API
        options (dict): The options for the API, considered are the token, username, password and login_path as entries
    """

    def __init__(self, *args, **kwargs):
        self._verify_certificate = True
        super().__init__(*args, **kwargs)
        self.login_path = self._login_path or (self.host + "/users/login")

    def authenticate(self, username: str, password: str) -> str:
        """Authenticate the user and return the token

        Args:
            username (str): The username
            password (str): The password

        Returns:
            str: The token
        """
        auth_payload = {"principal": username, "password": password}
        res = self._login(auth_payload, HEADER_JSON)
        try:
            token = "Bearer " + res["token"]
        except KeyError as e:
            raise AuthError(res) from e
        else:
            return token

    @authenticated
    @formatted_http_error
    def get_request(self, url, params=None, headers: dict = None, timeout=10) -> dict:
        """Make a GET request to the API

        Args:
            url (str): The URL of the API
            params (dict): The parameters for the request
            headers (dict): The headers for the request
            timeout (int): The timeout for the request

        Returns:
            dict: The response of the request
        """
        logger.debug(f"Getting data request: {url} , {params}")
        response = requests.get(
            url, params=params, headers=headers, timeout=timeout, verify=self._verify_certificate
        )
        if response.ok:
            logger.debug(f"Getting data response: {response.json()}")
            return response.json()
        else:
            # TODO should the username, password and token be deleted in this case? - Maybe I just can't access the data
            # if response.reason == "Unauthorized":
            # self.config.delete()
            raise response.raise_for_status()

    @authenticated
    @formatted_http_error
    def post_request(self, url, payload=None, files=None, headers=None, timeout=10) -> dict:
        """Make a POST request to the API

        Args:
            url (str): The URL of the API
            payload (dict): The payload for the request
            files (dict): The files for the request
            headers (dict): The headers for the request
            timeout (int): The timeout for the request

        Returns:
            dict: The response of the request
        """
        req = requests.post(
            url,
            json=payload,
            files=files,
            headers=headers,
            timeout=timeout,
            verify=self._verify_certificate,
        )
        req.raise_for_status()
        return req.json()

    @authenticated
    @formatted_http_error
    def patch_request(self, url, payload=None, files=None, headers=None, timeout=10):
        req = requests.patch(
            url,
            json=payload,
            files=files,
            headers=headers,
            timeout=timeout,
            verify=self._verify_certificate,
        )
        req.raise_for_status()
        return {}

    def _login(self, payload=None, headers=None, timeout=10):
        x = requests.post(
            self.login_path,
            json=payload,
            headers=headers,
            timeout=timeout,
            verify=self._verify_certificate,
        )
        return x.json()
