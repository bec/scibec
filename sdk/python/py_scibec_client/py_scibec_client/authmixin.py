"""Http client mixin for authentication."""

import getpass
from abc import ABC, abstractmethod

from py_scibec_client.utils import typename

HEADER_JSON = {"Content-type": "application/json", "Accept": "application/json"}


class AuthMixin(ABC):
    """Abstract class for authentication mixin.

    Args:
        host (str): The host of the API
        options (dict): The options for the API, considered are the token, username, password and login_path as entries
    """

    def __init__(self, host: str, options: dict = None):
        self.host = host.rstrip("/")
        if not options:
            options = {}
        self._token = options.get("token")
        self._username = options.get("username")
        self._password = options.get("password")
        self._login_path = options.get("login_path")

    def __repr__(self):
        tn = typename(self)
        return f"{tn} @ {self.host}"

    @abstractmethod
    def authenticate(self, username: str, password: str) -> str:
        """Abstract method to authenticate the user and return the token.

        Args:
            username (str): The username
            password (str): The password
        Returns:
            str: The token
        """
        raise NotImplementedError

    @property
    def token(self):
        """HTTP client token"""
        return self._retrieve_token()

    def _retrieve_token(self):
        """Retrieve the token from the configuration"""
        username = self._username or getpass.getuser()
        token = self._token
        if token is None:
            tn = typename(self)
            password = self._password or getpass.getpass(prompt=f"{tn} password for {username}: ")
            token = self.authenticate(username, password)
        self._token = token
        return token


class AuthError(Exception):
    """Exception for authentication errors."""
