""" The SciBEC API client is a Python client for the BEC datebase.
It provides an API wrapper around the loopback API, and expose the 
most important functioniality to the user in a high level interface.

Expert users can use the low level interface to access the API directly,
avaialable through the SciBECAPI class, i.e. SciBEC.http_client.get_request(...).
"""

from __future__ import annotations

import enum
import json
from collections import defaultdict
from typing import Literal

from bec_lib import bec_logger
from bec_lib.messages import ScanBaselineMessage, ScanMessage
from bec_lib.scan_items import ScanItem
from py_scibec_client.http_client import HttpClient

# ACLS = ["createACL", "readACL", "updateACL", "deleteACL", "shareACL", "adminACL"]


logger = bec_logger.logger


class DataSorting(str, enum.Enum):
    """The sorting options for the API"""

    NEWEST = "createdAt ASC"
    OLDEST = "createdAt DESC"
    NAME = "name"


class SciBECAPI(HttpClient):
    """HTTP client for the SciBEC API."""


class SciBECCore:
    """Core functionality and utilities for interacting with the SciBEC API.

    Optionally, token or username/password can be passed to the API via kwargs to
    directly initialize the HTTP client with the token or username/password.

    Args:
        high_level_interface (SciBEC): The high level interface to the API
    """

    def __init__(self, high_level_interface: SciBEC, **kwargs) -> None:
        self._hli = high_level_interface

    @property
    def http_client(self) -> SciBECAPI:
        """The http_client exposes the low level API client to the database"""
        return self._hli.http_client

    def _make_filter(
        self,
        where: dict = None,
        offset: int = 0,
        limit: int = 100,
        skip: int = 0,
        fields: dict = None,
        order: str = "string",
    ) -> str:
        """Utility wrapper to make filters for the API calls."""
        filt = dict()
        if offset > 0:
            filt["offset"] = offset
        if limit > 0:
            filt["limit"] = limit
        if skip > 0:
            filt["skip"] = skip
        if order:
            filt["order"] = order
        if where is not None:
            filt["where"] = where
        if fields is not None:
            filt["fields"] = fields
        return filt

    def make_filter(
        self,
        where: dict = None,
        offset: int = 0,
        limit: int = 100,
        skip: int = 0,
        fields: dict = None,
        order: str = "string",
        relation: str = None,
        scope: dict = None,
    ) -> str:
        """Create a filter for the API. Note, only a single nested include is supported.

        Args:
            where (dict): Condition match for the filter
            offset (int): The offset
            limit (int): The maximum number of results
            skip (int): The number of results to skip
            fields (dict): The fields to include from teh set
            include (dict): If subsets are available, whether to include them.
            order (list): The order of the results
            relation (str): To pass to the include filter the relation to include in the API call, i.e. scan_data for scans
            scope (dict): The scope of the include relation, i.e. where, offset, limit, skip, fields

        Returns:
            str: The filter dict as a json formatted string
        """
        filt = dict()
        filt.update(self._make_filter(where, offset, limit, skip, fields, order))
        if relation is not None:
            include_filt = dict()
            include_filt["relation"] = relation
            include_filt["scope"] = self._make_filter(**scope)
            filt["include"] = [include_filt]
        filt = json.dumps(filt)
        return {"filter": filt}

    def get_beamline_id(self, beamline: str) -> str:
        """Get the beamline id

        Args:
            beamline (str): The beamline name
        Returns:
            str: The beamline id
        """
        url = f"{self._hli.http_client.host}/beamlines"
        filt = self.make_filter(where={"name": beamline})
        return self.http_client.get_request(url, params=filt)[0]["id"]

    def get_experiment_id(self, beamline: str, experiment: str = None, p_group: str = None) -> str:
        """Get the experiment id. Either experiment or p_group must be provided, experiment is prioritised.

        Args:
            beamline (str): The beamline name
            experiment Optional(str): The experiment name
            p_group Optional(str): The proposal group name
        Returns:
            str: The experiment id
        """
        url = f"{self._hli.http_client.host}/experiments"
        bl_id = self.get_beamline_id(beamline)
        if experiment:
            filt = self.make_filter(where={"name": experiment, "beamlineId": bl_id})
            return self.http_client.get_request(url, params=filt)[0]["id"]
        if p_group:
            filt = self.make_filter(where={"writeAccount": p_group, "beamlineId": bl_id})
            return self.http_client.get_request(url, params=filt)[0]["id"]
        raise ValueError(f"Either experiment {experiment} or p_group {p_group} must be provided.")

    def get_dataset_id(self, dataset_num: int, beamline: str, experiment: str) -> str:
        """Get the dataset id

        Args:
            dataset_num (int): The dataset number
            beamline (str): The beamline name
            experiment (str): The experiment name
        Returns:
            str: The dataset id
        """
        url = f"{self._hli.http_client.host}/datasets"
        exp_id = self.get_experiment_id(beamline, experiment)
        filt = self.make_filter(where={"number": dataset_num, "experimentId": exp_id})
        return self.http_client.get_request(url, params=filt)[0]["id"]


class SciBEC:
    """High level interface to the SciBEC API.

    Args:
        host (str): The host of the API

    """

    def __init__(self, host: str, *args, **kwargs):
        self.http_client = SciBECAPI(host=host, *args, **kwargs)
        self.core = SciBECCore(high_level_interface=self)
        self._current_experiment = None

    def login(self, username: str, password: str) -> None:
        """Login to the API

        Args:
            username (str): The username
            password (str): The password
        """
        # pylint: disable=protected-access
        self.http_client._username = username
        self.http_client._password = password
        self.http_client._retrieve_token()

    def logout(self) -> None:
        """Logout from the API"""
        # pylint: disable=protected-access
        self.http_client._token = None
        self.http_client._username = None
        self.http_client._password = None

    def get_scan_by_id(self, scan_id: str, device_data: list[str] = None) -> ScanItem:
        """Get Scnas by the scan id. Can include device data.

        Args:
            scan_id (str): The scan id
            device_data (list[str]): The device data to include
        Returns:
            ScanItem: ScanItem matching the scan id
        """
        rtr = self._get_scan(scan_id=scan_id, device_data=device_data)
        if len(rtr) == 0:
            # TODO should this be a warning, or raise. Also, authentification vs actually no data...
            logger.warning(f"No scans found for scan_id {scan_id}.")
            return None
        return self._create_scan_item(rtr[0])

    def get_scan_by_scan_num(
        self,
        scan_num: int,
        beamline: str = None,
        experiment: str = None,
        p_group: str = None,
        device_data: list[str] = None,
    ) -> ScanItem:
        """Get scans by the scan number. Beamline and either experiment or p_group must be provided.
        Optionally device data can be included.

        Args:
            scan_num (int): The scan number
            beamline (str): The beamline name
            experiment (str): The experiment name
            p_group (str): The proposal group name
            device_data (list[str]): The device data to include
        Returns:
            ScanItem: The scan item
        """
        rtr = self._get_scan(
            scan_num=scan_num,
            beamline=beamline,
            experiment=experiment,
            p_group=p_group,
            device_data=device_data,
        )
        if len(rtr) == 0:
            logger.warning(
                f"No scans found for scan number {scan_num}, beamline {beamline}, experiment {experiment} and p_group {p_group}"
            )
            return None
        return self._create_scan_item(rtr[0])

    def get_scan_by_dataset_num(
        self,
        dataset_num: int,
        beamline: str = None,
        experiment: str = None,
        p_group: str = None,
        device_data: list[str] = None,
    ) -> ScanItem:
        """Get scans by the dataset number. Beamline and either experiment or p_group must be provided.
        Optionally device data can be included.

        Args:
            dataset_num (int): The scan number
            beamline (str): The beamline name
            experiment (str): The experiment name
            p_group (str): The proposal group name
            device_data (list[str]): The device data to include
        Returns:
            ScanItem: The scan item
        """
        rtr = self._get_scan(
            dataset_num=dataset_num,
            beamline=beamline,
            experiment=experiment,
            p_group=p_group,
            device_data=device_data,
        )
        if len(rtr) == 0:
            logger.warning(
                f"No scans found for dataset number {dataset_num}, beamline {beamline}, experiment {experiment} and p_group {p_group}"
            )
            return None
        return self._create_scan_item(rtr[0])

    def _get_scan(
        self,
        scan_id: str = None,
        dataset_num: int = None,
        scan_num: int = None,
        p_group: str = None,
        beamline: str = None,
        experiment: str = None,
        device_data: list[str] = None,
        **kwargs,
    ):
        """Utility function to get scans by various parameters.

        Args:
            scan_id (str): The scan id
            dataset_num (int): The dataset number
            scan_num (int): The scan number
            p_group (str): The proposal group name
            beamline (str): The beamline name
            experiment (str): The experiment name
            device_data (list[str]): The device data to include
            kwargs: Additional filter parameters for device_data
        Returns:
            list: The list of scans
        """
        host = self.http_client.host
        if device_data is not None:
            kwargs.update(self._create_device_kwargs(device_data))

        if scan_id:
            filt = self.core.make_filter(where={"scanId": scan_id}, **kwargs)
            return self.http_client.get_request(f"{host}/scans", params=filt)

        if experiment or p_group:
            exp_id = self.core.get_experiment_id(beamline, experiment=experiment, p_group=p_group)
            if scan_num:
                filt = self.core.make_filter(
                    where={"scanNumber": scan_num, "experimentId": exp_id}, **kwargs
                )
                return self.http_client.get_request(f"{host}/scans", params=filt)
            if dataset_num:
                dset_id = self.core.get_dataset_id(
                    dataset_num, beamline=beamline, experiment=experiment
                )
                filt = self.core.make_filter(where={"datasetId": dset_id}, **kwargs)
                return self.http_client.get_request(f"{host}/scans", params=filt)
        filt = self.core.make_filter(where={"experimentId": exp_id}, **kwargs)
        return self.http_client.get_request(f"{host}/scans", params=filt)

    def _create_device_kwargs(self, device_data: list[str]) -> dict:
        """Create the kwargs for the device data filter.

        Args:
            order (DataSorting): The sorting order
            limit (int): The limit of the results
        Returns:
            dict: The kwargs
        """
        rtr = dict()
        rtr["relation"] = "scanData"
        rtr["scope"] = {
            "order": DataSorting.NAME.value,
            "where": {"or": [{"name": dev} for dev in device_data]},
            "limit": 0,
        }
        return rtr

    def get_device_data(
        self,
        devices: list[str],
        beamline: str = None,
        experiment: str = None,
        p_group: str = None,
        sorting: Literal["newest", "oldest", "name"] = None,
        limit: int = 10,
    ) -> list:
        """Get device data for the given devices. Optionally filter by beamline and experiment.

        Args:
            device (list[str]): The list of devices
            beamline (str): The beamline name
            experiment (str): The experiment name
        Returns:
            list: The list of device data
        """
        host = self.http_client.host
        exp_id = self.core.get_experiment_id(beamline, experiment=experiment, p_group=p_group)
        if sorting is not None:
            order = DataSorting(sorting).value
        else:
            order = DataSorting.NEWEST.value
        self._get_scan(
            experiment=experiment,
            beamline=beamline,
            p_group=p_group,
            device_data=devices,
            order=order,
            limit=limit,
        )

    def _create_scan_item(self, db_entry: dict) -> ScanItem:
        """Recreate the ScanItem structure from database entry.

        Args:
            db_entry (dict): The database entry
        Returns:
            ScanItem: The scan item
        """
        scan_item = ScanItem(
            queue_id=db_entry["queueId"],
            scan_id=db_entry["scanId"],
            scan_number=db_entry["scanNumber"],
            status=db_entry["exitStatus"],
        )
        scan_item.num_points = db_entry["metadata"]["num_points"]
        scan_item.status = db_entry["exitStatus"]
        if len(db_entry.get("scanData", [])) == 0:
            logger.warning(f"No scan data found for scan_id {db_entry['scanId']}.")
            return scan_item
        monitored_devices = db_entry["metadata"]["readout_priority"]["monitored"]
        baseline_devices = db_entry["metadata"]["readout_priority"]["baseline"]
        async_devices = db_entry["metadata"]["readout_priority"]["async"]
        baseline_data = defaultdict(lambda: {})
        monitored_data = defaultdict(lambda: {})
        async_data = defaultdict(lambda: {})
        for data in db_entry["scanData"]:
            name = data["name"]
            if name in baseline_devices:
                baseline_data[name].update(data["data"])
            elif name in monitored_devices:
                monitored_data[name].update(data["data"])
            elif name in async_devices:
                async_data[name].update(data["data"])
        # Baseline data
        # TODO what if there is multiple points in the baseline data?- logic would have to be adapted
        if len(baseline_data) > 0 and isinstance(
            baseline_data[list(baseline_data.keys())[0]][
                list(baseline_data[list(baseline_data.keys())[0]].keys())[0]
            ]["value"],
            list,
        ):
            logger.warning(f"Multiple baseline data points found for scan_id {db_entry['scanId']}.")
        scan_item.baseline.set(
            0,
            ScanBaselineMessage(
                scan_id=db_entry["scanId"], data=dict(baseline_data), metadata=db_entry["metadata"]
            ),
        )
        # Async data
        scan_item.async_data = dict(async_data)

        # Monitored data
        if len(monitored_data) == 0:
            return scan_item
        num_points = len(
            monitored_data[list(monitored_data.keys())[0]][
                list(monitored_data[list(monitored_data.keys())[0]].keys())[0]
            ]["value"]
        )
        tmp_data = {}
        for point_id in range(num_points):
            for k, data in monitored_data.items():
                tmp_data[k] = dict(
                    [
                        (
                            signal_name,
                            {
                                "value": signal_data["value"][point_id],
                                "timestamp": signal_data["timestamp"][point_id],
                            },
                        )
                        for signal_name, signal_data in data.items()
                    ]
                )
            scan_item.data.set(
                point_id,
                ScanMessage(
                    point_id=point_id,
                    scan_id=db_entry["scanId"],
                    data=tmp_data,
                    metadata={
                        "scan_id": db_entry["scanId"],
                        "scan_type": db_entry["scanType"],
                        "scan_report_devices": db_entry["metadata"]["scan_report_devices"],
                    },
                ),
            )
        return scan_item
