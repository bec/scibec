# coding: utf-8

"""
    scibec

    scibec

    The version of the OpenAPI document: 1.14.2
    Contact: klaus.wakonig@psi.ch
    Generated by OpenAPI Generator (https://openapi-generator.tech)

    Do not edit the class manually.
"""  # noqa: E501


import unittest

from py_scibec_openapi_client.models.scan_filter1 import ScanFilter1

class TestScanFilter1(unittest.TestCase):
    """ScanFilter1 unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def make_instance(self, include_optional) -> ScanFilter1:
        """Test ScanFilter1
            include_option is a boolean, when False only required
            params are included, when True both required and
            optional params are included """
        # uncomment below to create an instance of `ScanFilter1`
        """
        model = ScanFilter1()
        if include_optional:
            return ScanFilter1(
                offset = 0,
                limit = 100,
                skip = 0,
                order = None,
                where = { },
                fields = None,
                include = [
                    null
                    ]
            )
        else:
            return ScanFilter1(
        )
        """

    def testScanFilter1(self):
        """Test ScanFilter1"""
        # inst_req_only = self.make_instance(include_optional=False)
        # inst_req_and_optional = self.make_instance(include_optional=True)

if __name__ == '__main__':
    unittest.main()
