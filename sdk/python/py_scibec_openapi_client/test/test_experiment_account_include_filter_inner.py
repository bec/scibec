# coding: utf-8

"""
    scibec

    scibec

    The version of the OpenAPI document: 1.14.2
    Contact: klaus.wakonig@psi.ch
    Generated by OpenAPI Generator (https://openapi-generator.tech)

    Do not edit the class manually.
"""  # noqa: E501


import unittest

from py_scibec_openapi_client.models.experiment_account_include_filter_inner import ExperimentAccountIncludeFilterInner

class TestExperimentAccountIncludeFilterInner(unittest.TestCase):
    """ExperimentAccountIncludeFilterInner unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def make_instance(self, include_optional) -> ExperimentAccountIncludeFilterInner:
        """Test ExperimentAccountIncludeFilterInner
            include_option is a boolean, when False only required
            params are included, when True both required and
            optional params are included """
        # uncomment below to create an instance of `ExperimentAccountIncludeFilterInner`
        """
        model = ExperimentAccountIncludeFilterInner()
        if include_optional:
            return ExperimentAccountIncludeFilterInner(
                relation = 'experiment',
                scope = py_scibec_openapi_client.models.experiment_account/scope_filter.ExperimentAccount.ScopeFilter(
                    offset = 0, 
                    limit = 100, 
                    skip = 0, 
                    order = null, 
                    where = { }, 
                    fields = null, 
                    include = [
                        { }
                        ], )
            )
        else:
            return ExperimentAccountIncludeFilterInner(
        )
        """

    def testExperimentAccountIncludeFilterInner(self):
        """Test ExperimentAccountIncludeFilterInner"""
        # inst_req_only = self.make_instance(include_optional=False)
        # inst_req_and_optional = self.make_instance(include_optional=True)

if __name__ == '__main__':
    unittest.main()
