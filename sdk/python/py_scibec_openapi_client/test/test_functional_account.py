# coding: utf-8

"""
    scibec

    scibec

    The version of the OpenAPI document: 1.14.2
    Contact: klaus.wakonig@psi.ch
    Generated by OpenAPI Generator (https://openapi-generator.tech)

    Do not edit the class manually.
"""  # noqa: E501


import unittest

from py_scibec_openapi_client.models.functional_account import FunctionalAccount

class TestFunctionalAccount(unittest.TestCase):
    """FunctionalAccount unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def make_instance(self, include_optional) -> FunctionalAccount:
        """Test FunctionalAccount
            include_option is a boolean, when False only required
            params are included, when True both required and
            optional params are included """
        # uncomment below to create an instance of `FunctionalAccount`
        """
        model = FunctionalAccount()
        if include_optional:
            return FunctionalAccount(
                id = '',
                read_acl = [
                    ''
                    ],
                write_acl = [
                    ''
                    ],
                owner = [
                    ''
                    ],
                created_at = datetime.datetime.strptime('2013-10-20 19:20:30.00', '%Y-%m-%d %H:%M:%S.%f'),
                created_by = '',
                name = '',
                read = True,
                write = True,
                remote = True,
                token = '',
                is_functional = True,
                access_config_id = ''
            )
        else:
            return FunctionalAccount(
                name = '',
                read = True,
                write = True,
                remote = True,
                token = '',
        )
        """

    def testFunctionalAccount(self):
        """Test FunctionalAccount"""
        # inst_req_only = self.make_instance(include_optional=False)
        # inst_req_and_optional = self.make_instance(include_optional=True)

if __name__ == '__main__':
    unittest.main()
