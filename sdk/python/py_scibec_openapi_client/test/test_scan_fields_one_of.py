# coding: utf-8

"""
    scibec

    scibec

    The version of the OpenAPI document: 1.14.2
    Contact: klaus.wakonig@psi.ch
    Generated by OpenAPI Generator (https://openapi-generator.tech)

    Do not edit the class manually.
"""  # noqa: E501


import unittest

from py_scibec_openapi_client.models.scan_fields_one_of import ScanFieldsOneOf

class TestScanFieldsOneOf(unittest.TestCase):
    """ScanFieldsOneOf unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def make_instance(self, include_optional) -> ScanFieldsOneOf:
        """Test ScanFieldsOneOf
            include_option is a boolean, when False only required
            params are included, when True both required and
            optional params are included """
        # uncomment below to create an instance of `ScanFieldsOneOf`
        """
        model = ScanFieldsOneOf()
        if include_optional:
            return ScanFieldsOneOf(
                id = True,
                read_acl = True,
                write_acl = True,
                owner = True,
                created_at = True,
                created_by = True,
                scan_type = True,
                scan_parameter = True,
                user_parameter = True,
                scan_id = True,
                request_id = True,
                queue_id = True,
                exit_status = True,
                scan_number = True,
                metadata = True,
                files = True,
                session_id = True,
                dataset_id = True,
                experiment_id = True
            )
        else:
            return ScanFieldsOneOf(
        )
        """

    def testScanFieldsOneOf(self):
        """Test ScanFieldsOneOf"""
        # inst_req_only = self.make_instance(include_optional=False)
        # inst_req_and_optional = self.make_instance(include_optional=True)

if __name__ == '__main__':
    unittest.main()
