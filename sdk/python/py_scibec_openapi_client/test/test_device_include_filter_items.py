# coding: utf-8

"""
    scibec

    scibec

    The version of the OpenAPI document: 1.14.2
    Contact: klaus.wakonig@psi.ch
    Generated by OpenAPI Generator (https://openapi-generator.tech)

    Do not edit the class manually.
"""  # noqa: E501


import unittest

from py_scibec_openapi_client.models.device_include_filter_items import DeviceIncludeFilterItems

class TestDeviceIncludeFilterItems(unittest.TestCase):
    """DeviceIncludeFilterItems unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def make_instance(self, include_optional) -> DeviceIncludeFilterItems:
        """Test DeviceIncludeFilterItems
            include_option is a boolean, when False only required
            params are included, when True both required and
            optional params are included """
        # uncomment below to create an instance of `DeviceIncludeFilterItems`
        """
        model = DeviceIncludeFilterItems()
        if include_optional:
            return DeviceIncludeFilterItems(
                relation = 'subdevices',
                scope = py_scibec_openapi_client.models.device/scope_filter.Device.ScopeFilter(
                    offset = 0, 
                    limit = 100, 
                    skip = 0, 
                    order = null, 
                    where = { }, 
                    fields = null, 
                    include = [
                        { }
                        ], )
            )
        else:
            return DeviceIncludeFilterItems(
        )
        """

    def testDeviceIncludeFilterItems(self):
        """Test DeviceIncludeFilterItems"""
        # inst_req_only = self.make_instance(include_optional=False)
        # inst_req_and_optional = self.make_instance(include_optional=True)

if __name__ == '__main__':
    unittest.main()
