# coding: utf-8

"""
    scibec

    scibec

    The version of the OpenAPI document: 1.14.2
    Contact: klaus.wakonig@psi.ch
    Generated by OpenAPI Generator (https://openapi-generator.tech)

    Do not edit the class manually.
"""  # noqa: E501


from __future__ import annotations
import pprint
import re  # noqa: F401
import json

from pydantic import BaseModel, ConfigDict, Field, StrictBool
from typing import Any, ClassVar, Dict, List, Optional
from typing import Optional, Set
from typing_extensions import Self

class DeviceFieldsOneOf(BaseModel):
    """
    DeviceFieldsOneOf
    """ # noqa: E501
    id: Optional[StrictBool] = None
    read_acl: Optional[StrictBool] = Field(default=None, alias="readACL")
    write_acl: Optional[StrictBool] = Field(default=None, alias="writeACL")
    owner: Optional[StrictBool] = None
    created_at: Optional[StrictBool] = Field(default=None, alias="createdAt")
    created_by: Optional[StrictBool] = Field(default=None, alias="createdBy")
    name: Optional[StrictBool] = None
    description: Optional[StrictBool] = None
    parent_id: Optional[StrictBool] = Field(default=None, alias="parentId")
    session_id: Optional[StrictBool] = Field(default=None, alias="sessionId")
    enabled: Optional[StrictBool] = None
    read_only: Optional[StrictBool] = Field(default=None, alias="readOnly")
    software_trigger: Optional[StrictBool] = Field(default=None, alias="softwareTrigger")
    device_class: Optional[StrictBool] = Field(default=None, alias="deviceClass")
    device_tags: Optional[StrictBool] = Field(default=None, alias="deviceTags")
    device_config: Optional[StrictBool] = Field(default=None, alias="deviceConfig")
    readout_priority: Optional[StrictBool] = Field(default=None, alias="readoutPriority")
    on_failure: Optional[StrictBool] = Field(default=None, alias="onFailure")
    user_parameter: Optional[StrictBool] = Field(default=None, alias="userParameter")
    __properties: ClassVar[List[str]] = ["id", "readACL", "writeACL", "owner", "createdAt", "createdBy", "name", "description", "parentId", "sessionId", "enabled", "readOnly", "softwareTrigger", "deviceClass", "deviceTags", "deviceConfig", "readoutPriority", "onFailure", "userParameter"]

    model_config = ConfigDict(
        populate_by_name=True,
        validate_assignment=True,
        protected_namespaces=(),
    )


    def to_str(self) -> str:
        """Returns the string representation of the model using alias"""
        return pprint.pformat(self.model_dump(by_alias=True))

    def to_json(self) -> str:
        """Returns the JSON representation of the model using alias"""
        # TODO: pydantic v2: use .model_dump_json(by_alias=True, exclude_unset=True) instead
        return json.dumps(self.to_dict())

    @classmethod
    def from_json(cls, json_str: str) -> Optional[Self]:
        """Create an instance of DeviceFieldsOneOf from a JSON string"""
        return cls.from_dict(json.loads(json_str))

    def to_dict(self) -> Dict[str, Any]:
        """Return the dictionary representation of the model using alias.

        This has the following differences from calling pydantic's
        `self.model_dump(by_alias=True)`:

        * `None` is only added to the output dict for nullable fields that
          were set at model initialization. Other fields with value `None`
          are ignored.
        """
        excluded_fields: Set[str] = set([
        ])

        _dict = self.model_dump(
            by_alias=True,
            exclude=excluded_fields,
            exclude_none=True,
        )
        return _dict

    @classmethod
    def from_dict(cls, obj: Optional[Dict[str, Any]]) -> Optional[Self]:
        """Create an instance of DeviceFieldsOneOf from a dict"""
        if obj is None:
            return None

        if not isinstance(obj, dict):
            return cls.model_validate(obj)

        _obj = cls.model_validate({
            "id": obj.get("id"),
            "readACL": obj.get("readACL"),
            "writeACL": obj.get("writeACL"),
            "owner": obj.get("owner"),
            "createdAt": obj.get("createdAt"),
            "createdBy": obj.get("createdBy"),
            "name": obj.get("name"),
            "description": obj.get("description"),
            "parentId": obj.get("parentId"),
            "sessionId": obj.get("sessionId"),
            "enabled": obj.get("enabled"),
            "readOnly": obj.get("readOnly"),
            "softwareTrigger": obj.get("softwareTrigger"),
            "deviceClass": obj.get("deviceClass"),
            "deviceTags": obj.get("deviceTags"),
            "deviceConfig": obj.get("deviceConfig"),
            "readoutPriority": obj.get("readoutPriority"),
            "onFailure": obj.get("onFailure"),
            "userParameter": obj.get("userParameter")
        })
        return _obj


