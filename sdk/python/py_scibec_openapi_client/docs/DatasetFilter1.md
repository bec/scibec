# DatasetFilter1


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**offset** | **int** |  | [optional] 
**limit** | **int** |  | [optional] 
**skip** | **int** |  | [optional] 
**order** | [**AccessAccountFilterOrder**](AccessAccountFilterOrder.md) |  | [optional] 
**where** | **Dict[str, object]** |  | [optional] 
**fields** | [**DatasetFields**](DatasetFields.md) |  | [optional] 
**include** | [**List[DatasetIncludeFilterInner]**](DatasetIncludeFilterInner.md) |  | [optional] 

## Example

```python
from py_scibec_openapi_client.models.dataset_filter1 import DatasetFilter1

# TODO update the JSON string below
json = "{}"
# create an instance of DatasetFilter1 from a JSON string
dataset_filter1_instance = DatasetFilter1.from_json(json)
# print the JSON string representation of the object
print(DatasetFilter1.to_json())

# convert the object into a dict
dataset_filter1_dict = dataset_filter1_instance.to_dict()
# create an instance of DatasetFilter1 from a dict
dataset_filter1_form_dict = dataset_filter1.from_dict(dataset_filter1_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


