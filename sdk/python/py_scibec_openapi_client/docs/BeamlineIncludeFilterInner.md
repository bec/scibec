# BeamlineIncludeFilterInner


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**relation** | **str** |  | [optional] 
**scope** | [**BeamlineScopeFilter**](BeamlineScopeFilter.md) |  | [optional] 

## Example

```python
from py_scibec_openapi_client.models.beamline_include_filter_inner import BeamlineIncludeFilterInner

# TODO update the JSON string below
json = "{}"
# create an instance of BeamlineIncludeFilterInner from a JSON string
beamline_include_filter_inner_instance = BeamlineIncludeFilterInner.from_json(json)
# print the JSON string representation of the object
print(BeamlineIncludeFilterInner.to_json())

# convert the object into a dict
beamline_include_filter_inner_dict = beamline_include_filter_inner_instance.to_dict()
# create an instance of BeamlineIncludeFilterInner from a dict
beamline_include_filter_inner_form_dict = beamline_include_filter_inner.from_dict(beamline_include_filter_inner_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


