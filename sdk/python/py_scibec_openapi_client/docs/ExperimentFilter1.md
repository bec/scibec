# ExperimentFilter1


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**offset** | **int** |  | [optional] 
**limit** | **int** |  | [optional] 
**skip** | **int** |  | [optional] 
**order** | [**AccessAccountFilterOrder**](AccessAccountFilterOrder.md) |  | [optional] 
**where** | **Dict[str, object]** |  | [optional] 
**fields** | [**ExperimentFields**](ExperimentFields.md) |  | [optional] 
**include** | [**List[ExperimentIncludeFilterInner]**](ExperimentIncludeFilterInner.md) |  | [optional] 

## Example

```python
from py_scibec_openapi_client.models.experiment_filter1 import ExperimentFilter1

# TODO update the JSON string below
json = "{}"
# create an instance of ExperimentFilter1 from a JSON string
experiment_filter1_instance = ExperimentFilter1.from_json(json)
# print the JSON string representation of the object
print(ExperimentFilter1.to_json())

# convert the object into a dict
experiment_filter1_dict = experiment_filter1_instance.to_dict()
# create an instance of ExperimentFilter1 from a dict
experiment_filter1_form_dict = experiment_filter1.from_dict(experiment_filter1_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


