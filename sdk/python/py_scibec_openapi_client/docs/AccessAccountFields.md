# AccessAccountFields


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **bool** |  | [optional] 
**read_acl** | **bool** |  | [optional] 
**write_acl** | **bool** |  | [optional] 
**owner** | **bool** |  | [optional] 
**created_at** | **bool** |  | [optional] 
**created_by** | **bool** |  | [optional] 
**name** | **bool** |  | [optional] 
**read** | **bool** |  | [optional] 
**write** | **bool** |  | [optional] 
**remote** | **bool** |  | [optional] 
**token** | **bool** |  | [optional] 
**is_functional** | **bool** |  | [optional] 

## Example

```python
from py_scibec_openapi_client.models.access_account_fields import AccessAccountFields

# TODO update the JSON string below
json = "{}"
# create an instance of AccessAccountFields from a JSON string
access_account_fields_instance = AccessAccountFields.from_json(json)
# print the JSON string representation of the object
print(AccessAccountFields.to_json())

# convert the object into a dict
access_account_fields_dict = access_account_fields_instance.to_dict()
# create an instance of AccessAccountFields from a dict
access_account_fields_form_dict = access_account_fields.from_dict(access_account_fields_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


