# DeviceIncludeFilterInner


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**relation** | **str** |  | [optional] 
**scope** | [**DeviceScopeFilter**](DeviceScopeFilter.md) |  | [optional] 

## Example

```python
from py_scibec_openapi_client.models.device_include_filter_inner import DeviceIncludeFilterInner

# TODO update the JSON string below
json = "{}"
# create an instance of DeviceIncludeFilterInner from a JSON string
device_include_filter_inner_instance = DeviceIncludeFilterInner.from_json(json)
# print the JSON string representation of the object
print(DeviceIncludeFilterInner.to_json())

# convert the object into a dict
device_include_filter_inner_dict = device_include_filter_inner_instance.to_dict()
# create an instance of DeviceIncludeFilterInner from a dict
device_include_filter_inner_form_dict = device_include_filter_inner.from_dict(device_include_filter_inner_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


