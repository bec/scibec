# NXEntityFilter1


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**offset** | **int** |  | [optional] 
**limit** | **int** |  | [optional] 
**skip** | **int** |  | [optional] 
**order** | [**AccessAccountFilterOrder**](AccessAccountFilterOrder.md) |  | [optional] 
**where** | **Dict[str, object]** |  | [optional] 
**fields** | [**NXEntityFields**](NXEntityFields.md) |  | [optional] 
**include** | [**List[NXEntityIncludeFilterInner]**](NXEntityIncludeFilterInner.md) |  | [optional] 

## Example

```python
from py_scibec_openapi_client.models.nx_entity_filter1 import NXEntityFilter1

# TODO update the JSON string below
json = "{}"
# create an instance of NXEntityFilter1 from a JSON string
nx_entity_filter1_instance = NXEntityFilter1.from_json(json)
# print the JSON string representation of the object
print(NXEntityFilter1.to_json())

# convert the object into a dict
nx_entity_filter1_dict = nx_entity_filter1_instance.to_dict()
# create an instance of NXEntityFilter1 from a dict
nx_entity_filter1_form_dict = nx_entity_filter1.from_dict(nx_entity_filter1_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


