# py_scibec_openapi_client.ExperimentControllerApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**experiment_controller_count**](ExperimentControllerApi.md#experiment_controller_count) | **GET** /experiments/count | 
[**experiment_controller_create**](ExperimentControllerApi.md#experiment_controller_create) | **POST** /experiments | 
[**experiment_controller_delete_by_id**](ExperimentControllerApi.md#experiment_controller_delete_by_id) | **DELETE** /experiments/{id} | 
[**experiment_controller_find**](ExperimentControllerApi.md#experiment_controller_find) | **GET** /experiments | 
[**experiment_controller_find_by_id**](ExperimentControllerApi.md#experiment_controller_find_by_id) | **GET** /experiments/{id} | 
[**experiment_controller_update_all**](ExperimentControllerApi.md#experiment_controller_update_all) | **PATCH** /experiments | 
[**experiment_controller_update_by_id**](ExperimentControllerApi.md#experiment_controller_update_by_id) | **PATCH** /experiments/{id} | 


# **experiment_controller_count**
> LoopbackCount experiment_controller_count(where=where)



### Example

* Bearer (JWT) Authentication (jwt):

```python
import py_scibec_openapi_client
from py_scibec_openapi_client.models.loopback_count import LoopbackCount
from py_scibec_openapi_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = py_scibec_openapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization (JWT): jwt
configuration = py_scibec_openapi_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
with py_scibec_openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = py_scibec_openapi_client.ExperimentControllerApi(api_client)
    where = None # Dict[str, object] |  (optional)

    try:
        api_response = api_instance.experiment_controller_count(where=where)
        print("The response of ExperimentControllerApi->experiment_controller_count:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling ExperimentControllerApi->experiment_controller_count: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | [**Dict[str, object]**](object.md)|  | [optional] 

### Return type

[**LoopbackCount**](LoopbackCount.md)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Experiment model count |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **experiment_controller_create**
> Experiment experiment_controller_create(new_experiment=new_experiment)



### Example

* Bearer (JWT) Authentication (jwt):

```python
import py_scibec_openapi_client
from py_scibec_openapi_client.models.experiment import Experiment
from py_scibec_openapi_client.models.new_experiment import NewExperiment
from py_scibec_openapi_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = py_scibec_openapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization (JWT): jwt
configuration = py_scibec_openapi_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
with py_scibec_openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = py_scibec_openapi_client.ExperimentControllerApi(api_client)
    new_experiment = py_scibec_openapi_client.NewExperiment() # NewExperiment |  (optional)

    try:
        api_response = api_instance.experiment_controller_create(new_experiment=new_experiment)
        print("The response of ExperimentControllerApi->experiment_controller_create:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling ExperimentControllerApi->experiment_controller_create: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **new_experiment** | [**NewExperiment**](NewExperiment.md)|  | [optional] 

### Return type

[**Experiment**](Experiment.md)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Experiment model instance |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **experiment_controller_delete_by_id**
> experiment_controller_delete_by_id(id)



### Example

* Bearer (JWT) Authentication (jwt):

```python
import py_scibec_openapi_client
from py_scibec_openapi_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = py_scibec_openapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization (JWT): jwt
configuration = py_scibec_openapi_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
with py_scibec_openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = py_scibec_openapi_client.ExperimentControllerApi(api_client)
    id = 'id_example' # str | 

    try:
        api_instance.experiment_controller_delete_by_id(id)
    except Exception as e:
        print("Exception when calling ExperimentControllerApi->experiment_controller_delete_by_id: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**|  | 

### Return type

void (empty response body)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | Experiment DELETE success |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **experiment_controller_find**
> List[ExperimentWithRelations] experiment_controller_find(filter=filter)



### Example

* Bearer (JWT) Authentication (jwt):

```python
import py_scibec_openapi_client
from py_scibec_openapi_client.models.experiment_filter1 import ExperimentFilter1
from py_scibec_openapi_client.models.experiment_with_relations import ExperimentWithRelations
from py_scibec_openapi_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = py_scibec_openapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization (JWT): jwt
configuration = py_scibec_openapi_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
with py_scibec_openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = py_scibec_openapi_client.ExperimentControllerApi(api_client)
    filter = py_scibec_openapi_client.ExperimentFilter1() # ExperimentFilter1 |  (optional)

    try:
        api_response = api_instance.experiment_controller_find(filter=filter)
        print("The response of ExperimentControllerApi->experiment_controller_find:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling ExperimentControllerApi->experiment_controller_find: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | [**ExperimentFilter1**](.md)|  | [optional] 

### Return type

[**List[ExperimentWithRelations]**](ExperimentWithRelations.md)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Array of Experiment model instances |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **experiment_controller_find_by_id**
> ExperimentWithRelations experiment_controller_find_by_id(id, filter=filter)



### Example

* Bearer (JWT) Authentication (jwt):

```python
import py_scibec_openapi_client
from py_scibec_openapi_client.models.experiment_filter import ExperimentFilter
from py_scibec_openapi_client.models.experiment_with_relations import ExperimentWithRelations
from py_scibec_openapi_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = py_scibec_openapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization (JWT): jwt
configuration = py_scibec_openapi_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
with py_scibec_openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = py_scibec_openapi_client.ExperimentControllerApi(api_client)
    id = 'id_example' # str | 
    filter = py_scibec_openapi_client.ExperimentFilter() # ExperimentFilter |  (optional)

    try:
        api_response = api_instance.experiment_controller_find_by_id(id, filter=filter)
        print("The response of ExperimentControllerApi->experiment_controller_find_by_id:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling ExperimentControllerApi->experiment_controller_find_by_id: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**|  | 
 **filter** | [**ExperimentFilter**](.md)|  | [optional] 

### Return type

[**ExperimentWithRelations**](ExperimentWithRelations.md)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Experiment model instance |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **experiment_controller_update_all**
> LoopbackCount experiment_controller_update_all(where=where, experiment_partial=experiment_partial)



### Example

* Bearer (JWT) Authentication (jwt):

```python
import py_scibec_openapi_client
from py_scibec_openapi_client.models.experiment_partial import ExperimentPartial
from py_scibec_openapi_client.models.loopback_count import LoopbackCount
from py_scibec_openapi_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = py_scibec_openapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization (JWT): jwt
configuration = py_scibec_openapi_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
with py_scibec_openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = py_scibec_openapi_client.ExperimentControllerApi(api_client)
    where = None # Dict[str, object] |  (optional)
    experiment_partial = py_scibec_openapi_client.ExperimentPartial() # ExperimentPartial |  (optional)

    try:
        api_response = api_instance.experiment_controller_update_all(where=where, experiment_partial=experiment_partial)
        print("The response of ExperimentControllerApi->experiment_controller_update_all:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling ExperimentControllerApi->experiment_controller_update_all: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | [**Dict[str, object]**](object.md)|  | [optional] 
 **experiment_partial** | [**ExperimentPartial**](ExperimentPartial.md)|  | [optional] 

### Return type

[**LoopbackCount**](LoopbackCount.md)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Experiment PATCH success count |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **experiment_controller_update_by_id**
> experiment_controller_update_by_id(id, experiment_partial=experiment_partial)



### Example

* Bearer (JWT) Authentication (jwt):

```python
import py_scibec_openapi_client
from py_scibec_openapi_client.models.experiment_partial import ExperimentPartial
from py_scibec_openapi_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = py_scibec_openapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization (JWT): jwt
configuration = py_scibec_openapi_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
with py_scibec_openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = py_scibec_openapi_client.ExperimentControllerApi(api_client)
    id = 'id_example' # str | 
    experiment_partial = py_scibec_openapi_client.ExperimentPartial() # ExperimentPartial |  (optional)

    try:
        api_instance.experiment_controller_update_by_id(id, experiment_partial=experiment_partial)
    except Exception as e:
        print("Exception when calling ExperimentControllerApi->experiment_controller_update_by_id: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**|  | 
 **experiment_partial** | [**ExperimentPartial**](ExperimentPartial.md)|  | [optional] 

### Return type

void (empty response body)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | Experiment PATCH success |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

