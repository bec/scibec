# BeamlineWithRelations

(tsType: BeamlineWithRelations, schemaOptions: { includeRelations: true })

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** |  | [optional] 
**read_acl** | **List[str]** |  | [optional] 
**write_acl** | **List[str]** |  | [optional] 
**owner** | **List[str]** |  | [optional] 
**created_at** | **datetime** |  | [optional] 
**created_by** | **str** |  | [optional] 
**name** | **str** |  | 
**active_experiment** | **str** |  | [optional] 
**access_config** | [**AccessConfigWithRelations**](AccessConfigWithRelations.md) |  | [optional] 
**experiments** | [**List[ExperimentWithRelations]**](ExperimentWithRelations.md) |  | [optional] 

## Example

```python
from py_scibec_openapi_client.models.beamline_with_relations import BeamlineWithRelations

# TODO update the JSON string below
json = "{}"
# create an instance of BeamlineWithRelations from a JSON string
beamline_with_relations_instance = BeamlineWithRelations.from_json(json)
# print the JSON string representation of the object
print(BeamlineWithRelations.to_json())

# convert the object into a dict
beamline_with_relations_dict = beamline_with_relations_instance.to_dict()
# create an instance of BeamlineWithRelations from a dict
beamline_with_relations_form_dict = beamline_with_relations.from_dict(beamline_with_relations_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


