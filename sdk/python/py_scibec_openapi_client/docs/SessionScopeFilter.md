# SessionScopeFilter


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**offset** | **int** |  | [optional] 
**limit** | **int** |  | [optional] 
**skip** | **int** |  | [optional] 
**order** | [**AccessAccountFilterOrder**](AccessAccountFilterOrder.md) |  | [optional] 
**where** | **Dict[str, object]** |  | [optional] 
**fields** | [**AccessConfigScopeFilterFields**](AccessConfigScopeFilterFields.md) |  | [optional] 
**include** | **List[Dict[str, object]]** |  | [optional] 

## Example

```python
from py_scibec_openapi_client.models.session_scope_filter import SessionScopeFilter

# TODO update the JSON string below
json = "{}"
# create an instance of SessionScopeFilter from a JSON string
session_scope_filter_instance = SessionScopeFilter.from_json(json)
# print the JSON string representation of the object
print(SessionScopeFilter.to_json())

# convert the object into a dict
session_scope_filter_dict = session_scope_filter_instance.to_dict()
# create an instance of SessionScopeFilter from a dict
session_scope_filter_form_dict = session_scope_filter.from_dict(session_scope_filter_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


