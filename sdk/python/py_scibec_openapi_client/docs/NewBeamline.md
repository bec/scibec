# NewBeamline

(tsType: Omit<Beamline, 'id'>, schemaOptions: { title: 'NewBeamline', exclude: [ 'id' ] })

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**read_acl** | **List[str]** |  | [optional] 
**write_acl** | **List[str]** |  | [optional] 
**owner** | **List[str]** |  | [optional] 
**created_at** | **datetime** |  | [optional] 
**created_by** | **str** |  | [optional] 
**name** | **str** |  | 
**active_experiment** | **str** |  | [optional] 

## Example

```python
from py_scibec_openapi_client.models.new_beamline import NewBeamline

# TODO update the JSON string below
json = "{}"
# create an instance of NewBeamline from a JSON string
new_beamline_instance = NewBeamline.from_json(json)
# print the JSON string representation of the object
print(NewBeamline.to_json())

# convert the object into a dict
new_beamline_dict = new_beamline_instance.to_dict()
# create an instance of NewBeamline from a dict
new_beamline_form_dict = new_beamline.from_dict(new_beamline_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


