# py_scibec_openapi_client.NXEntityControllerApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**n_x_entity_controller_count**](NXEntityControllerApi.md#n_x_entity_controller_count) | **GET** /nxentities/count | 
[**n_x_entity_controller_create**](NXEntityControllerApi.md#n_x_entity_controller_create) | **POST** /nxentities | 
[**n_x_entity_controller_create_entry**](NXEntityControllerApi.md#n_x_entity_controller_create_entry) | **POST** /nxentities/entry | 
[**n_x_entity_controller_delete_by_id**](NXEntityControllerApi.md#n_x_entity_controller_delete_by_id) | **DELETE** /nxentities/{id} | 
[**n_x_entity_controller_find**](NXEntityControllerApi.md#n_x_entity_controller_find) | **GET** /nxentities | 
[**n_x_entity_controller_find_by_id**](NXEntityControllerApi.md#n_x_entity_controller_find_by_id) | **GET** /nxentities/{id} | 
[**n_x_entity_controller_replace_by_id**](NXEntityControllerApi.md#n_x_entity_controller_replace_by_id) | **PUT** /nxentities/{id} | 
[**n_x_entity_controller_update_all**](NXEntityControllerApi.md#n_x_entity_controller_update_all) | **PATCH** /nxentities | 
[**n_x_entity_controller_update_by_id**](NXEntityControllerApi.md#n_x_entity_controller_update_by_id) | **PATCH** /nxentities/{id} | 


# **n_x_entity_controller_count**
> LoopbackCount n_x_entity_controller_count(where=where)



### Example

* Bearer (JWT) Authentication (jwt):

```python
import py_scibec_openapi_client
from py_scibec_openapi_client.models.loopback_count import LoopbackCount
from py_scibec_openapi_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = py_scibec_openapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization (JWT): jwt
configuration = py_scibec_openapi_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
with py_scibec_openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = py_scibec_openapi_client.NXEntityControllerApi(api_client)
    where = None # Dict[str, object] |  (optional)

    try:
        api_response = api_instance.n_x_entity_controller_count(where=where)
        print("The response of NXEntityControllerApi->n_x_entity_controller_count:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling NXEntityControllerApi->n_x_entity_controller_count: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | [**Dict[str, object]**](object.md)|  | [optional] 

### Return type

[**LoopbackCount**](LoopbackCount.md)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | NXEntity model count |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **n_x_entity_controller_create**
> NXEntity n_x_entity_controller_create(new_nx_entity=new_nx_entity)



### Example

* Bearer (JWT) Authentication (jwt):

```python
import py_scibec_openapi_client
from py_scibec_openapi_client.models.nx_entity import NXEntity
from py_scibec_openapi_client.models.new_nx_entity import NewNXEntity
from py_scibec_openapi_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = py_scibec_openapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization (JWT): jwt
configuration = py_scibec_openapi_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
with py_scibec_openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = py_scibec_openapi_client.NXEntityControllerApi(api_client)
    new_nx_entity = py_scibec_openapi_client.NewNXEntity() # NewNXEntity |  (optional)

    try:
        api_response = api_instance.n_x_entity_controller_create(new_nx_entity=new_nx_entity)
        print("The response of NXEntityControllerApi->n_x_entity_controller_create:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling NXEntityControllerApi->n_x_entity_controller_create: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **new_nx_entity** | [**NewNXEntity**](NewNXEntity.md)|  | [optional] 

### Return type

[**NXEntity**](NXEntity.md)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | NXEntity model instance |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **n_x_entity_controller_create_entry**
> NXEntityNested n_x_entity_controller_create_entry(new_nx_entity_nested=new_nx_entity_nested)



### Example

* Bearer (JWT) Authentication (jwt):

```python
import py_scibec_openapi_client
from py_scibec_openapi_client.models.nx_entity_nested import NXEntityNested
from py_scibec_openapi_client.models.new_nx_entity_nested import NewNXEntityNested
from py_scibec_openapi_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = py_scibec_openapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization (JWT): jwt
configuration = py_scibec_openapi_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
with py_scibec_openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = py_scibec_openapi_client.NXEntityControllerApi(api_client)
    new_nx_entity_nested = py_scibec_openapi_client.NewNXEntityNested() # NewNXEntityNested |  (optional)

    try:
        api_response = api_instance.n_x_entity_controller_create_entry(new_nx_entity_nested=new_nx_entity_nested)
        print("The response of NXEntityControllerApi->n_x_entity_controller_create_entry:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling NXEntityControllerApi->n_x_entity_controller_create_entry: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **new_nx_entity_nested** | [**NewNXEntityNested**](NewNXEntityNested.md)|  | [optional] 

### Return type

[**NXEntityNested**](NXEntityNested.md)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | NXEntityNested model instance |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **n_x_entity_controller_delete_by_id**
> n_x_entity_controller_delete_by_id(id)



### Example

* Bearer (JWT) Authentication (jwt):

```python
import py_scibec_openapi_client
from py_scibec_openapi_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = py_scibec_openapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization (JWT): jwt
configuration = py_scibec_openapi_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
with py_scibec_openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = py_scibec_openapi_client.NXEntityControllerApi(api_client)
    id = 'id_example' # str | 

    try:
        api_instance.n_x_entity_controller_delete_by_id(id)
    except Exception as e:
        print("Exception when calling NXEntityControllerApi->n_x_entity_controller_delete_by_id: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**|  | 

### Return type

void (empty response body)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | NXEntity DELETE success |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **n_x_entity_controller_find**
> List[NXEntityWithRelations] n_x_entity_controller_find(filter=filter)



### Example

* Bearer (JWT) Authentication (jwt):

```python
import py_scibec_openapi_client
from py_scibec_openapi_client.models.nx_entity_filter1 import NXEntityFilter1
from py_scibec_openapi_client.models.nx_entity_with_relations import NXEntityWithRelations
from py_scibec_openapi_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = py_scibec_openapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization (JWT): jwt
configuration = py_scibec_openapi_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
with py_scibec_openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = py_scibec_openapi_client.NXEntityControllerApi(api_client)
    filter = py_scibec_openapi_client.NXEntityFilter1() # NXEntityFilter1 |  (optional)

    try:
        api_response = api_instance.n_x_entity_controller_find(filter=filter)
        print("The response of NXEntityControllerApi->n_x_entity_controller_find:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling NXEntityControllerApi->n_x_entity_controller_find: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | [**NXEntityFilter1**](.md)|  | [optional] 

### Return type

[**List[NXEntityWithRelations]**](NXEntityWithRelations.md)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Array of NXEntity model instances |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **n_x_entity_controller_find_by_id**
> NXEntityWithRelations n_x_entity_controller_find_by_id(id, filter=filter)



### Example

* Bearer (JWT) Authentication (jwt):

```python
import py_scibec_openapi_client
from py_scibec_openapi_client.models.nx_entity_filter import NXEntityFilter
from py_scibec_openapi_client.models.nx_entity_with_relations import NXEntityWithRelations
from py_scibec_openapi_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = py_scibec_openapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization (JWT): jwt
configuration = py_scibec_openapi_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
with py_scibec_openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = py_scibec_openapi_client.NXEntityControllerApi(api_client)
    id = 'id_example' # str | 
    filter = py_scibec_openapi_client.NXEntityFilter() # NXEntityFilter |  (optional)

    try:
        api_response = api_instance.n_x_entity_controller_find_by_id(id, filter=filter)
        print("The response of NXEntityControllerApi->n_x_entity_controller_find_by_id:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling NXEntityControllerApi->n_x_entity_controller_find_by_id: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**|  | 
 **filter** | [**NXEntityFilter**](.md)|  | [optional] 

### Return type

[**NXEntityWithRelations**](NXEntityWithRelations.md)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | NXEntity model instance |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **n_x_entity_controller_replace_by_id**
> n_x_entity_controller_replace_by_id(id, nx_entity=nx_entity)



### Example

* Bearer (JWT) Authentication (jwt):

```python
import py_scibec_openapi_client
from py_scibec_openapi_client.models.nx_entity import NXEntity
from py_scibec_openapi_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = py_scibec_openapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization (JWT): jwt
configuration = py_scibec_openapi_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
with py_scibec_openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = py_scibec_openapi_client.NXEntityControllerApi(api_client)
    id = 'id_example' # str | 
    nx_entity = py_scibec_openapi_client.NXEntity() # NXEntity |  (optional)

    try:
        api_instance.n_x_entity_controller_replace_by_id(id, nx_entity=nx_entity)
    except Exception as e:
        print("Exception when calling NXEntityControllerApi->n_x_entity_controller_replace_by_id: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**|  | 
 **nx_entity** | [**NXEntity**](NXEntity.md)|  | [optional] 

### Return type

void (empty response body)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | NXEntity PUT success |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **n_x_entity_controller_update_all**
> LoopbackCount n_x_entity_controller_update_all(where=where, nx_entity_partial=nx_entity_partial)



### Example

* Bearer (JWT) Authentication (jwt):

```python
import py_scibec_openapi_client
from py_scibec_openapi_client.models.loopback_count import LoopbackCount
from py_scibec_openapi_client.models.nx_entity_partial import NXEntityPartial
from py_scibec_openapi_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = py_scibec_openapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization (JWT): jwt
configuration = py_scibec_openapi_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
with py_scibec_openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = py_scibec_openapi_client.NXEntityControllerApi(api_client)
    where = None # Dict[str, object] |  (optional)
    nx_entity_partial = py_scibec_openapi_client.NXEntityPartial() # NXEntityPartial |  (optional)

    try:
        api_response = api_instance.n_x_entity_controller_update_all(where=where, nx_entity_partial=nx_entity_partial)
        print("The response of NXEntityControllerApi->n_x_entity_controller_update_all:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling NXEntityControllerApi->n_x_entity_controller_update_all: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | [**Dict[str, object]**](object.md)|  | [optional] 
 **nx_entity_partial** | [**NXEntityPartial**](NXEntityPartial.md)|  | [optional] 

### Return type

[**LoopbackCount**](LoopbackCount.md)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | NXEntity PATCH success count |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **n_x_entity_controller_update_by_id**
> n_x_entity_controller_update_by_id(id, nx_entity_partial=nx_entity_partial)



### Example

* Bearer (JWT) Authentication (jwt):

```python
import py_scibec_openapi_client
from py_scibec_openapi_client.models.nx_entity_partial import NXEntityPartial
from py_scibec_openapi_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = py_scibec_openapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization (JWT): jwt
configuration = py_scibec_openapi_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
with py_scibec_openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = py_scibec_openapi_client.NXEntityControllerApi(api_client)
    id = 'id_example' # str | 
    nx_entity_partial = py_scibec_openapi_client.NXEntityPartial() # NXEntityPartial |  (optional)

    try:
        api_instance.n_x_entity_controller_update_by_id(id, nx_entity_partial=nx_entity_partial)
    except Exception as e:
        print("Exception when calling NXEntityControllerApi->n_x_entity_controller_update_by_id: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**|  | 
 **nx_entity_partial** | [**NXEntityPartial**](NXEntityPartial.md)|  | [optional] 

### Return type

void (empty response body)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | NXEntity PATCH success |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

