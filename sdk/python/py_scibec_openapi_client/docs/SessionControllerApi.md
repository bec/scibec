# py_scibec_openapi_client.SessionControllerApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**session_controller_count**](SessionControllerApi.md#session_controller_count) | **GET** /sessions/count | 
[**session_controller_create**](SessionControllerApi.md#session_controller_create) | **POST** /sessions | 
[**session_controller_delete_by_id**](SessionControllerApi.md#session_controller_delete_by_id) | **DELETE** /sessions/{id} | 
[**session_controller_find**](SessionControllerApi.md#session_controller_find) | **GET** /sessions | 
[**session_controller_find_by_id**](SessionControllerApi.md#session_controller_find_by_id) | **GET** /sessions/{id} | 
[**session_controller_update_all**](SessionControllerApi.md#session_controller_update_all) | **PATCH** /sessions | 
[**session_controller_update_by_id**](SessionControllerApi.md#session_controller_update_by_id) | **PATCH** /sessions/{id} | 


# **session_controller_count**
> LoopbackCount session_controller_count(where=where)



### Example

* Bearer (JWT) Authentication (jwt):

```python
import py_scibec_openapi_client
from py_scibec_openapi_client.models.loopback_count import LoopbackCount
from py_scibec_openapi_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = py_scibec_openapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization (JWT): jwt
configuration = py_scibec_openapi_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
with py_scibec_openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = py_scibec_openapi_client.SessionControllerApi(api_client)
    where = None # Dict[str, object] |  (optional)

    try:
        api_response = api_instance.session_controller_count(where=where)
        print("The response of SessionControllerApi->session_controller_count:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling SessionControllerApi->session_controller_count: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | [**Dict[str, object]**](object.md)|  | [optional] 

### Return type

[**LoopbackCount**](LoopbackCount.md)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Session model count |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **session_controller_create**
> Session session_controller_create(new_session=new_session)



### Example

* Bearer (JWT) Authentication (jwt):

```python
import py_scibec_openapi_client
from py_scibec_openapi_client.models.new_session import NewSession
from py_scibec_openapi_client.models.session import Session
from py_scibec_openapi_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = py_scibec_openapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization (JWT): jwt
configuration = py_scibec_openapi_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
with py_scibec_openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = py_scibec_openapi_client.SessionControllerApi(api_client)
    new_session = py_scibec_openapi_client.NewSession() # NewSession |  (optional)

    try:
        api_response = api_instance.session_controller_create(new_session=new_session)
        print("The response of SessionControllerApi->session_controller_create:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling SessionControllerApi->session_controller_create: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **new_session** | [**NewSession**](NewSession.md)|  | [optional] 

### Return type

[**Session**](Session.md)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Session model instance |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **session_controller_delete_by_id**
> session_controller_delete_by_id(id)



### Example

* Bearer (JWT) Authentication (jwt):

```python
import py_scibec_openapi_client
from py_scibec_openapi_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = py_scibec_openapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization (JWT): jwt
configuration = py_scibec_openapi_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
with py_scibec_openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = py_scibec_openapi_client.SessionControllerApi(api_client)
    id = 'id_example' # str | 

    try:
        api_instance.session_controller_delete_by_id(id)
    except Exception as e:
        print("Exception when calling SessionControllerApi->session_controller_delete_by_id: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**|  | 

### Return type

void (empty response body)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | Session DELETE success |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **session_controller_find**
> List[SessionWithRelations] session_controller_find(filter=filter)



### Example

* Bearer (JWT) Authentication (jwt):

```python
import py_scibec_openapi_client
from py_scibec_openapi_client.models.session_filter1 import SessionFilter1
from py_scibec_openapi_client.models.session_with_relations import SessionWithRelations
from py_scibec_openapi_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = py_scibec_openapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization (JWT): jwt
configuration = py_scibec_openapi_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
with py_scibec_openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = py_scibec_openapi_client.SessionControllerApi(api_client)
    filter = py_scibec_openapi_client.SessionFilter1() # SessionFilter1 |  (optional)

    try:
        api_response = api_instance.session_controller_find(filter=filter)
        print("The response of SessionControllerApi->session_controller_find:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling SessionControllerApi->session_controller_find: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | [**SessionFilter1**](.md)|  | [optional] 

### Return type

[**List[SessionWithRelations]**](SessionWithRelations.md)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Array of Session model instances |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **session_controller_find_by_id**
> SessionWithRelations session_controller_find_by_id(id, filter=filter)



### Example

* Bearer (JWT) Authentication (jwt):

```python
import py_scibec_openapi_client
from py_scibec_openapi_client.models.session_filter import SessionFilter
from py_scibec_openapi_client.models.session_with_relations import SessionWithRelations
from py_scibec_openapi_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = py_scibec_openapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization (JWT): jwt
configuration = py_scibec_openapi_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
with py_scibec_openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = py_scibec_openapi_client.SessionControllerApi(api_client)
    id = 'id_example' # str | 
    filter = py_scibec_openapi_client.SessionFilter() # SessionFilter |  (optional)

    try:
        api_response = api_instance.session_controller_find_by_id(id, filter=filter)
        print("The response of SessionControllerApi->session_controller_find_by_id:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling SessionControllerApi->session_controller_find_by_id: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**|  | 
 **filter** | [**SessionFilter**](.md)|  | [optional] 

### Return type

[**SessionWithRelations**](SessionWithRelations.md)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Session model instance |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **session_controller_update_all**
> LoopbackCount session_controller_update_all(where=where, session_partial=session_partial)



### Example

* Bearer (JWT) Authentication (jwt):

```python
import py_scibec_openapi_client
from py_scibec_openapi_client.models.loopback_count import LoopbackCount
from py_scibec_openapi_client.models.session_partial import SessionPartial
from py_scibec_openapi_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = py_scibec_openapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization (JWT): jwt
configuration = py_scibec_openapi_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
with py_scibec_openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = py_scibec_openapi_client.SessionControllerApi(api_client)
    where = None # Dict[str, object] |  (optional)
    session_partial = py_scibec_openapi_client.SessionPartial() # SessionPartial |  (optional)

    try:
        api_response = api_instance.session_controller_update_all(where=where, session_partial=session_partial)
        print("The response of SessionControllerApi->session_controller_update_all:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling SessionControllerApi->session_controller_update_all: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | [**Dict[str, object]**](object.md)|  | [optional] 
 **session_partial** | [**SessionPartial**](SessionPartial.md)|  | [optional] 

### Return type

[**LoopbackCount**](LoopbackCount.md)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Session PATCH success count |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **session_controller_update_by_id**
> session_controller_update_by_id(id, session_partial=session_partial)



### Example

* Bearer (JWT) Authentication (jwt):

```python
import py_scibec_openapi_client
from py_scibec_openapi_client.models.session_partial import SessionPartial
from py_scibec_openapi_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = py_scibec_openapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization (JWT): jwt
configuration = py_scibec_openapi_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
with py_scibec_openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = py_scibec_openapi_client.SessionControllerApi(api_client)
    id = 'id_example' # str | 
    session_partial = py_scibec_openapi_client.SessionPartial() # SessionPartial |  (optional)

    try:
        api_instance.session_controller_update_by_id(id, session_partial=session_partial)
    except Exception as e:
        print("Exception when calling SessionControllerApi->session_controller_update_by_id: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**|  | 
 **session_partial** | [**SessionPartial**](SessionPartial.md)|  | [optional] 

### Return type

void (empty response body)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | Session PATCH success |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

