# FunctionalAccountFilter


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**offset** | **int** |  | [optional] 
**limit** | **int** |  | [optional] 
**skip** | **int** |  | [optional] 
**order** | [**AccessAccountFilterOrder**](AccessAccountFilterOrder.md) |  | [optional] 
**fields** | [**FunctionalAccountFields**](FunctionalAccountFields.md) |  | [optional] 
**include** | [**List[FunctionalAccountIncludeFilterInner]**](FunctionalAccountIncludeFilterInner.md) |  | [optional] 

## Example

```python
from py_scibec_openapi_client.models.functional_account_filter import FunctionalAccountFilter

# TODO update the JSON string below
json = "{}"
# create an instance of FunctionalAccountFilter from a JSON string
functional_account_filter_instance = FunctionalAccountFilter.from_json(json)
# print the JSON string representation of the object
print(FunctionalAccountFilter.to_json())

# convert the object into a dict
functional_account_filter_dict = functional_account_filter_instance.to_dict()
# create an instance of FunctionalAccountFilter from a dict
functional_account_filter_form_dict = functional_account_filter.from_dict(functional_account_filter_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


