# AccessConfigFieldsOneOf


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **bool** |  | [optional] 
**read_acl** | **bool** |  | [optional] 
**write_acl** | **bool** |  | [optional] 
**owner** | **bool** |  | [optional] 
**created_at** | **bool** |  | [optional] 
**created_by** | **bool** |  | [optional] 
**target_account** | **bool** |  | [optional] 
**beamline_id** | **bool** |  | [optional] 
**auth_enabled** | **bool** |  | [optional] 
**active_accounts** | **bool** |  | [optional] 
**use_passwords** | **bool** |  | [optional] 

## Example

```python
from py_scibec_openapi_client.models.access_config_fields_one_of import AccessConfigFieldsOneOf

# TODO update the JSON string below
json = "{}"
# create an instance of AccessConfigFieldsOneOf from a JSON string
access_config_fields_one_of_instance = AccessConfigFieldsOneOf.from_json(json)
# print the JSON string representation of the object
print(AccessConfigFieldsOneOf.to_json())

# convert the object into a dict
access_config_fields_one_of_dict = access_config_fields_one_of_instance.to_dict()
# create an instance of AccessConfigFieldsOneOf from a dict
access_config_fields_one_of_form_dict = access_config_fields_one_of.from_dict(access_config_fields_one_of_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


