# AccessConfigFilter1


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**offset** | **int** |  | [optional] 
**limit** | **int** |  | [optional] 
**skip** | **int** |  | [optional] 
**order** | [**AccessAccountFilterOrder**](AccessAccountFilterOrder.md) |  | [optional] 
**where** | **Dict[str, object]** |  | [optional] 
**fields** | [**AccessConfigFields**](AccessConfigFields.md) |  | [optional] 
**include** | [**List[AccessConfigIncludeFilterInner]**](AccessConfigIncludeFilterInner.md) |  | [optional] 

## Example

```python
from py_scibec_openapi_client.models.access_config_filter1 import AccessConfigFilter1

# TODO update the JSON string below
json = "{}"
# create an instance of AccessConfigFilter1 from a JSON string
access_config_filter1_instance = AccessConfigFilter1.from_json(json)
# print the JSON string representation of the object
print(AccessConfigFilter1.to_json())

# convert the object into a dict
access_config_filter1_dict = access_config_filter1_instance.to_dict()
# create an instance of AccessConfigFilter1 from a dict
access_config_filter1_form_dict = access_config_filter1.from_dict(access_config_filter1_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


