# ScanIncludeFilterInner


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**relation** | **str** |  | [optional] 
**scope** | [**ScanScopeFilter**](ScanScopeFilter.md) |  | [optional] 

## Example

```python
from py_scibec_openapi_client.models.scan_include_filter_inner import ScanIncludeFilterInner

# TODO update the JSON string below
json = "{}"
# create an instance of ScanIncludeFilterInner from a JSON string
scan_include_filter_inner_instance = ScanIncludeFilterInner.from_json(json)
# print the JSON string representation of the object
print(ScanIncludeFilterInner.to_json())

# convert the object into a dict
scan_include_filter_inner_dict = scan_include_filter_inner_instance.to_dict()
# create an instance of ScanIncludeFilterInner from a dict
scan_include_filter_inner_form_dict = scan_include_filter_inner.from_dict(scan_include_filter_inner_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


