# ScanFieldsOneOf


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **bool** |  | [optional] 
**read_acl** | **bool** |  | [optional] 
**write_acl** | **bool** |  | [optional] 
**owner** | **bool** |  | [optional] 
**created_at** | **bool** |  | [optional] 
**created_by** | **bool** |  | [optional] 
**scan_type** | **bool** |  | [optional] 
**scan_parameter** | **bool** |  | [optional] 
**user_parameter** | **bool** |  | [optional] 
**scan_id** | **bool** |  | [optional] 
**request_id** | **bool** |  | [optional] 
**queue_id** | **bool** |  | [optional] 
**exit_status** | **bool** |  | [optional] 
**scan_number** | **bool** |  | [optional] 
**metadata** | **bool** |  | [optional] 
**files** | **bool** |  | [optional] 
**session_id** | **bool** |  | [optional] 
**dataset_id** | **bool** |  | [optional] 
**experiment_id** | **bool** |  | [optional] 

## Example

```python
from py_scibec_openapi_client.models.scan_fields_one_of import ScanFieldsOneOf

# TODO update the JSON string below
json = "{}"
# create an instance of ScanFieldsOneOf from a JSON string
scan_fields_one_of_instance = ScanFieldsOneOf.from_json(json)
# print the JSON string representation of the object
print(ScanFieldsOneOf.to_json())

# convert the object into a dict
scan_fields_one_of_dict = scan_fields_one_of_instance.to_dict()
# create an instance of ScanFieldsOneOf from a dict
scan_fields_one_of_form_dict = scan_fields_one_of.from_dict(scan_fields_one_of_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


