# NXEntityIncludeFilterItems


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**relation** | **str** |  | [optional] 
**scope** | [**NXEntityScopeFilter**](NXEntityScopeFilter.md) |  | [optional] 

## Example

```python
from py_scibec_openapi_client.models.nx_entity_include_filter_items import NXEntityIncludeFilterItems

# TODO update the JSON string below
json = "{}"
# create an instance of NXEntityIncludeFilterItems from a JSON string
nx_entity_include_filter_items_instance = NXEntityIncludeFilterItems.from_json(json)
# print the JSON string representation of the object
print(NXEntityIncludeFilterItems.to_json())

# convert the object into a dict
nx_entity_include_filter_items_dict = nx_entity_include_filter_items_instance.to_dict()
# create an instance of NXEntityIncludeFilterItems from a dict
nx_entity_include_filter_items_form_dict = nx_entity_include_filter_items.from_dict(nx_entity_include_filter_items_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


