# AccessAccountWithRelations

(tsType: AccessAccountWithRelations, schemaOptions: { includeRelations: true })

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** |  | [optional] 
**read_acl** | **List[str]** |  | [optional] 
**write_acl** | **List[str]** |  | [optional] 
**owner** | **List[str]** |  | [optional] 
**created_at** | **datetime** |  | [optional] 
**created_by** | **str** |  | [optional] 
**name** | **str** |  | 
**read** | **bool** |  | 
**write** | **bool** |  | 
**remote** | **bool** |  | 
**token** | **str** |  | 
**is_functional** | **bool** |  | [optional] 

## Example

```python
from py_scibec_openapi_client.models.access_account_with_relations import AccessAccountWithRelations

# TODO update the JSON string below
json = "{}"
# create an instance of AccessAccountWithRelations from a JSON string
access_account_with_relations_instance = AccessAccountWithRelations.from_json(json)
# print the JSON string representation of the object
print(AccessAccountWithRelations.to_json())

# convert the object into a dict
access_account_with_relations_dict = access_account_with_relations_instance.to_dict()
# create an instance of AccessAccountWithRelations from a dict
access_account_with_relations_form_dict = access_account_with_relations.from_dict(access_account_with_relations_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


