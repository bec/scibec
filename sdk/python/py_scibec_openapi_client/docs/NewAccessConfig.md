# NewAccessConfig

(tsType: Omit<AccessConfig, 'id'>, schemaOptions: { title: 'NewAccessConfig', exclude: [ 'id' ] })

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**read_acl** | **List[str]** |  | [optional] 
**write_acl** | **List[str]** |  | [optional] 
**owner** | **List[str]** |  | [optional] 
**created_at** | **datetime** |  | [optional] 
**created_by** | **str** |  | [optional] 
**target_account** | **str** |  | [optional] 
**beamline_id** | **str** | Beamline to which this config belongs. | 
**auth_enabled** | **bool** |  | 
**active_accounts** | **List[str]** |  | [optional] 
**use_passwords** | **bool** |  | 

## Example

```python
from py_scibec_openapi_client.models.new_access_config import NewAccessConfig

# TODO update the JSON string below
json = "{}"
# create an instance of NewAccessConfig from a JSON string
new_access_config_instance = NewAccessConfig.from_json(json)
# print the JSON string representation of the object
print(NewAccessConfig.to_json())

# convert the object into a dict
new_access_config_dict = new_access_config_instance.to_dict()
# create an instance of NewAccessConfig from a dict
new_access_config_form_dict = new_access_config.from_dict(new_access_config_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


