# ExperimentFieldsOneOf


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **bool** |  | [optional] 
**read_acl** | **bool** |  | [optional] 
**write_acl** | **bool** |  | [optional] 
**owner** | **bool** |  | [optional] 
**created_at** | **bool** |  | [optional] 
**created_by** | **bool** |  | [optional] 
**name** | **bool** |  | [optional] 
**beamline_id** | **bool** |  | [optional] 
**write_account** | **bool** |  | [optional] 
**user_id** | **bool** |  | [optional] 
**logbook** | **bool** |  | [optional] 
**samples** | **bool** |  | [optional] 
**experiment_config** | **bool** |  | [optional] 
**experiment_info** | **bool** |  | [optional] 
**active_session** | **bool** |  | [optional] 

## Example

```python
from py_scibec_openapi_client.models.experiment_fields_one_of import ExperimentFieldsOneOf

# TODO update the JSON string below
json = "{}"
# create an instance of ExperimentFieldsOneOf from a JSON string
experiment_fields_one_of_instance = ExperimentFieldsOneOf.from_json(json)
# print the JSON string representation of the object
print(ExperimentFieldsOneOf.to_json())

# convert the object into a dict
experiment_fields_one_of_dict = experiment_fields_one_of_instance.to_dict()
# create an instance of ExperimentFieldsOneOf from a dict
experiment_fields_one_of_form_dict = experiment_fields_one_of.from_dict(experiment_fields_one_of_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


