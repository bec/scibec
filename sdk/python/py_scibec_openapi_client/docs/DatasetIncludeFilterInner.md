# DatasetIncludeFilterInner


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**relation** | **str** |  | [optional] 
**scope** | [**DatasetScopeFilter**](DatasetScopeFilter.md) |  | [optional] 

## Example

```python
from py_scibec_openapi_client.models.dataset_include_filter_inner import DatasetIncludeFilterInner

# TODO update the JSON string below
json = "{}"
# create an instance of DatasetIncludeFilterInner from a JSON string
dataset_include_filter_inner_instance = DatasetIncludeFilterInner.from_json(json)
# print the JSON string representation of the object
print(DatasetIncludeFilterInner.to_json())

# convert the object into a dict
dataset_include_filter_inner_dict = dataset_include_filter_inner_instance.to_dict()
# create an instance of DatasetIncludeFilterInner from a dict
dataset_include_filter_inner_form_dict = dataset_include_filter_inner.from_dict(dataset_include_filter_inner_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


