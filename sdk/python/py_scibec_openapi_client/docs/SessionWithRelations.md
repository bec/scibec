# SessionWithRelations

(tsType: SessionWithRelations, schemaOptions: { includeRelations: true })

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** |  | [optional] 
**read_acl** | **List[str]** |  | [optional] 
**write_acl** | **List[str]** |  | [optional] 
**owner** | **List[str]** |  | [optional] 
**created_at** | **datetime** |  | [optional] 
**created_by** | **str** |  | [optional] 
**name** | **str** |  | 
**experiment_id** | **str** | Session to which this device belongs. | [optional] 
**session_config** | **object** |  | [optional] 
**experiment** | [**ExperimentWithRelations**](ExperimentWithRelations.md) |  | [optional] 
**scans** | [**List[ScanWithRelations]**](ScanWithRelations.md) |  | [optional] 
**devices** | [**List[DeviceWithRelations]**](DeviceWithRelations.md) |  | [optional] 

## Example

```python
from py_scibec_openapi_client.models.session_with_relations import SessionWithRelations

# TODO update the JSON string below
json = "{}"
# create an instance of SessionWithRelations from a JSON string
session_with_relations_instance = SessionWithRelations.from_json(json)
# print the JSON string representation of the object
print(SessionWithRelations.to_json())

# convert the object into a dict
session_with_relations_dict = session_with_relations_instance.to_dict()
# create an instance of SessionWithRelations from a dict
session_with_relations_form_dict = session_with_relations.from_dict(session_with_relations_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


