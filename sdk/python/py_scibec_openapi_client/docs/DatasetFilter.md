# DatasetFilter


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**offset** | **int** |  | [optional] 
**limit** | **int** |  | [optional] 
**skip** | **int** |  | [optional] 
**order** | [**AccessAccountFilterOrder**](AccessAccountFilterOrder.md) |  | [optional] 
**fields** | [**DatasetFields**](DatasetFields.md) |  | [optional] 
**include** | [**List[DatasetIncludeFilterInner]**](DatasetIncludeFilterInner.md) |  | [optional] 

## Example

```python
from py_scibec_openapi_client.models.dataset_filter import DatasetFilter

# TODO update the JSON string below
json = "{}"
# create an instance of DatasetFilter from a JSON string
dataset_filter_instance = DatasetFilter.from_json(json)
# print the JSON string representation of the object
print(DatasetFilter.to_json())

# convert the object into a dict
dataset_filter_dict = dataset_filter_instance.to_dict()
# create an instance of DatasetFilter from a dict
dataset_filter_form_dict = dataset_filter.from_dict(dataset_filter_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


