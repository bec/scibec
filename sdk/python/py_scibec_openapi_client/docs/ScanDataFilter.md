# ScanDataFilter


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**offset** | **int** |  | [optional] 
**limit** | **int** |  | [optional] 
**skip** | **int** |  | [optional] 
**order** | [**AccessAccountFilterOrder**](AccessAccountFilterOrder.md) |  | [optional] 
**fields** | [**ScanDataFields**](ScanDataFields.md) |  | [optional] 
**include** | [**List[ScanDataIncludeFilterInner]**](ScanDataIncludeFilterInner.md) |  | [optional] 

## Example

```python
from py_scibec_openapi_client.models.scan_data_filter import ScanDataFilter

# TODO update the JSON string below
json = "{}"
# create an instance of ScanDataFilter from a JSON string
scan_data_filter_instance = ScanDataFilter.from_json(json)
# print the JSON string representation of the object
print(ScanDataFilter.to_json())

# convert the object into a dict
scan_data_filter_dict = scan_data_filter_instance.to_dict()
# create an instance of ScanDataFilter from a dict
scan_data_filter_form_dict = scan_data_filter.from_dict(scan_data_filter_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


