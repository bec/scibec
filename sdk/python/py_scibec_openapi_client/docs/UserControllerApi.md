# py_scibec_openapi_client.UserControllerApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**user_controller_create**](UserControllerApi.md#user_controller_create) | **POST** /users | 
[**user_controller_find_by_id**](UserControllerApi.md#user_controller_find_by_id) | **GET** /users/{userId} | 
[**user_controller_login**](UserControllerApi.md#user_controller_login) | **POST** /users/login | 
[**user_controller_print_current_user**](UserControllerApi.md#user_controller_print_current_user) | **GET** /users/me | 


# **user_controller_create**
> User user_controller_create(new_user=new_user)



### Example


```python
import py_scibec_openapi_client
from py_scibec_openapi_client.models.new_user import NewUser
from py_scibec_openapi_client.models.user import User
from py_scibec_openapi_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = py_scibec_openapi_client.Configuration(
    host = "http://localhost"
)


# Enter a context with an instance of the API client
with py_scibec_openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = py_scibec_openapi_client.UserControllerApi(api_client)
    new_user = py_scibec_openapi_client.NewUser() # NewUser |  (optional)

    try:
        api_response = api_instance.user_controller_create(new_user=new_user)
        print("The response of UserControllerApi->user_controller_create:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling UserControllerApi->user_controller_create: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **new_user** | [**NewUser**](NewUser.md)|  | [optional] 

### Return type

[**User**](User.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | User |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **user_controller_find_by_id**
> User user_controller_find_by_id(user_id)



### Example

* Bearer (JWT) Authentication (jwt):

```python
import py_scibec_openapi_client
from py_scibec_openapi_client.models.user import User
from py_scibec_openapi_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = py_scibec_openapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization (JWT): jwt
configuration = py_scibec_openapi_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
with py_scibec_openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = py_scibec_openapi_client.UserControllerApi(api_client)
    user_id = 'user_id_example' # str | 

    try:
        api_response = api_instance.user_controller_find_by_id(user_id)
        print("The response of UserControllerApi->user_controller_find_by_id:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling UserControllerApi->user_controller_find_by_id: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **user_id** | **str**|  | 

### Return type

[**User**](User.md)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | User |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **user_controller_login**
> UserControllerLogin200Response user_controller_login(user_controller_login_request)



### Example


```python
import py_scibec_openapi_client
from py_scibec_openapi_client.models.user_controller_login200_response import UserControllerLogin200Response
from py_scibec_openapi_client.models.user_controller_login_request import UserControllerLoginRequest
from py_scibec_openapi_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = py_scibec_openapi_client.Configuration(
    host = "http://localhost"
)


# Enter a context with an instance of the API client
with py_scibec_openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = py_scibec_openapi_client.UserControllerApi(api_client)
    user_controller_login_request = py_scibec_openapi_client.UserControllerLoginRequest() # UserControllerLoginRequest | The input of login function

    try:
        api_response = api_instance.user_controller_login(user_controller_login_request)
        print("The response of UserControllerApi->user_controller_login:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling UserControllerApi->user_controller_login: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **user_controller_login_request** | [**UserControllerLoginRequest**](UserControllerLoginRequest.md)| The input of login function | 

### Return type

[**UserControllerLogin200Response**](UserControllerLogin200Response.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Token |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **user_controller_print_current_user**
> UserControllerPrintCurrentUser200Response user_controller_print_current_user()



### Example

* Bearer (JWT) Authentication (jwt):

```python
import py_scibec_openapi_client
from py_scibec_openapi_client.models.user_controller_print_current_user200_response import UserControllerPrintCurrentUser200Response
from py_scibec_openapi_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = py_scibec_openapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization (JWT): jwt
configuration = py_scibec_openapi_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
with py_scibec_openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = py_scibec_openapi_client.UserControllerApi(api_client)

    try:
        api_response = api_instance.user_controller_print_current_user()
        print("The response of UserControllerApi->user_controller_print_current_user:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling UserControllerApi->user_controller_print_current_user: %s\n" % e)
```



### Parameters

This endpoint does not need any parameter.

### Return type

[**UserControllerPrintCurrentUser200Response**](UserControllerPrintCurrentUser200Response.md)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | The current user profile |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

