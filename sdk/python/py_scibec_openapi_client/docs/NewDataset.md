# NewDataset

(tsType: Omit<Dataset, 'id'>, schemaOptions: { title: 'NewDataset', exclude: [ 'id' ] })

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**read_acl** | **List[str]** |  | [optional] 
**write_acl** | **List[str]** |  | [optional] 
**owner** | **List[str]** |  | [optional] 
**created_at** | **datetime** |  | [optional] 
**created_by** | **str** |  | [optional] 
**name** | **str** |  | [optional] 
**number** | **float** |  | [optional] 
**experiment_id** | **str** |  | [optional] 

## Example

```python
from py_scibec_openapi_client.models.new_dataset import NewDataset

# TODO update the JSON string below
json = "{}"
# create an instance of NewDataset from a JSON string
new_dataset_instance = NewDataset.from_json(json)
# print the JSON string representation of the object
print(NewDataset.to_json())

# convert the object into a dict
new_dataset_dict = new_dataset_instance.to_dict()
# create an instance of NewDataset from a dict
new_dataset_form_dict = new_dataset.from_dict(new_dataset_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


