# DatasetFieldsOneOf


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **bool** |  | [optional] 
**read_acl** | **bool** |  | [optional] 
**write_acl** | **bool** |  | [optional] 
**owner** | **bool** |  | [optional] 
**created_at** | **bool** |  | [optional] 
**created_by** | **bool** |  | [optional] 
**name** | **bool** |  | [optional] 
**number** | **bool** |  | [optional] 
**experiment_id** | **bool** |  | [optional] 

## Example

```python
from py_scibec_openapi_client.models.dataset_fields_one_of import DatasetFieldsOneOf

# TODO update the JSON string below
json = "{}"
# create an instance of DatasetFieldsOneOf from a JSON string
dataset_fields_one_of_instance = DatasetFieldsOneOf.from_json(json)
# print the JSON string representation of the object
print(DatasetFieldsOneOf.to_json())

# convert the object into a dict
dataset_fields_one_of_dict = dataset_fields_one_of_instance.to_dict()
# create an instance of DatasetFieldsOneOf from a dict
dataset_fields_one_of_form_dict = dataset_fields_one_of.from_dict(dataset_fields_one_of_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


