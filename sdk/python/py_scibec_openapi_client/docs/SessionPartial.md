# SessionPartial

(tsType: Partial<Session>, schemaOptions: { partial: true })

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** |  | [optional] 
**read_acl** | **List[str]** |  | [optional] 
**write_acl** | **List[str]** |  | [optional] 
**owner** | **List[str]** |  | [optional] 
**created_at** | **datetime** |  | [optional] 
**created_by** | **str** |  | [optional] 
**name** | **str** |  | [optional] 
**experiment_id** | **str** | Session to which this device belongs. | [optional] 
**session_config** | **object** |  | [optional] 

## Example

```python
from py_scibec_openapi_client.models.session_partial import SessionPartial

# TODO update the JSON string below
json = "{}"
# create an instance of SessionPartial from a JSON string
session_partial_instance = SessionPartial.from_json(json)
# print the JSON string representation of the object
print(SessionPartial.to_json())

# convert the object into a dict
session_partial_dict = session_partial_instance.to_dict()
# create an instance of SessionPartial from a dict
session_partial_form_dict = session_partial.from_dict(session_partial_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


