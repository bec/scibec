# AccessAccountFilterOrder


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------

## Example

```python
from py_scibec_openapi_client.models.access_account_filter_order import AccessAccountFilterOrder

# TODO update the JSON string below
json = "{}"
# create an instance of AccessAccountFilterOrder from a JSON string
access_account_filter_order_instance = AccessAccountFilterOrder.from_json(json)
# print the JSON string representation of the object
print(AccessAccountFilterOrder.to_json())

# convert the object into a dict
access_account_filter_order_dict = access_account_filter_order_instance.to_dict()
# create an instance of AccessAccountFilterOrder from a dict
access_account_filter_order_form_dict = access_account_filter_order.from_dict(access_account_filter_order_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


