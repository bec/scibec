# ScanDataIncludeFilterItems


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**relation** | **str** |  | [optional] 
**scope** | [**ScanDataScopeFilter**](ScanDataScopeFilter.md) |  | [optional] 

## Example

```python
from py_scibec_openapi_client.models.scan_data_include_filter_items import ScanDataIncludeFilterItems

# TODO update the JSON string below
json = "{}"
# create an instance of ScanDataIncludeFilterItems from a JSON string
scan_data_include_filter_items_instance = ScanDataIncludeFilterItems.from_json(json)
# print the JSON string representation of the object
print(ScanDataIncludeFilterItems.to_json())

# convert the object into a dict
scan_data_include_filter_items_dict = scan_data_include_filter_items_instance.to_dict()
# create an instance of ScanDataIncludeFilterItems from a dict
scan_data_include_filter_items_form_dict = scan_data_include_filter_items.from_dict(scan_data_include_filter_items_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


