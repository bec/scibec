# SessionIncludeFilterItems


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**relation** | **str** |  | [optional] 
**scope** | [**SessionScopeFilter**](SessionScopeFilter.md) |  | [optional] 

## Example

```python
from py_scibec_openapi_client.models.session_include_filter_items import SessionIncludeFilterItems

# TODO update the JSON string below
json = "{}"
# create an instance of SessionIncludeFilterItems from a JSON string
session_include_filter_items_instance = SessionIncludeFilterItems.from_json(json)
# print the JSON string representation of the object
print(SessionIncludeFilterItems.to_json())

# convert the object into a dict
session_include_filter_items_dict = session_include_filter_items_instance.to_dict()
# create an instance of SessionIncludeFilterItems from a dict
session_include_filter_items_form_dict = session_include_filter_items.from_dict(session_include_filter_items_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


