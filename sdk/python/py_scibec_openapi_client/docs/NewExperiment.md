# NewExperiment

(tsType: Omit<Experiment, 'id'>, schemaOptions: { title: 'NewExperiment', exclude: [ 'id' ] })

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**read_acl** | **List[str]** |  | [optional] 
**write_acl** | **List[str]** |  | [optional] 
**owner** | **List[str]** |  | [optional] 
**created_at** | **datetime** |  | [optional] 
**created_by** | **str** |  | [optional] 
**name** | **str** |  | 
**beamline_id** | **str** |  | 
**write_account** | **str** |  | [optional] 
**user_id** | **str** |  | [optional] 
**logbook** | **str** |  | [optional] 
**samples** | **List[str]** |  | [optional] 
**experiment_config** | **object** |  | [optional] 
**experiment_info** | **object** |  | [optional] 
**active_session** | **str** |  | [optional] 

## Example

```python
from py_scibec_openapi_client.models.new_experiment import NewExperiment

# TODO update the JSON string below
json = "{}"
# create an instance of NewExperiment from a JSON string
new_experiment_instance = NewExperiment.from_json(json)
# print the JSON string representation of the object
print(NewExperiment.to_json())

# convert the object into a dict
new_experiment_dict = new_experiment_instance.to_dict()
# create an instance of NewExperiment from a dict
new_experiment_form_dict = new_experiment.from_dict(new_experiment_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


