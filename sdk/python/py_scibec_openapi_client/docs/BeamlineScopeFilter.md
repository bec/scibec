# BeamlineScopeFilter


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**offset** | **int** |  | [optional] 
**limit** | **int** |  | [optional] 
**skip** | **int** |  | [optional] 
**order** | [**AccessAccountFilterOrder**](AccessAccountFilterOrder.md) |  | [optional] 
**where** | **Dict[str, object]** |  | [optional] 
**fields** | [**AccessConfigScopeFilterFields**](AccessConfigScopeFilterFields.md) |  | [optional] 
**include** | **List[Dict[str, object]]** |  | [optional] 

## Example

```python
from py_scibec_openapi_client.models.beamline_scope_filter import BeamlineScopeFilter

# TODO update the JSON string below
json = "{}"
# create an instance of BeamlineScopeFilter from a JSON string
beamline_scope_filter_instance = BeamlineScopeFilter.from_json(json)
# print the JSON string representation of the object
print(BeamlineScopeFilter.to_json())

# convert the object into a dict
beamline_scope_filter_dict = beamline_scope_filter_instance.to_dict()
# create an instance of BeamlineScopeFilter from a dict
beamline_scope_filter_form_dict = beamline_scope_filter.from_dict(beamline_scope_filter_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


