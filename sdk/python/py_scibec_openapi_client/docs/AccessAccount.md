# AccessAccount


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** |  | [optional] 
**read_acl** | **List[str]** |  | [optional] 
**write_acl** | **List[str]** |  | [optional] 
**owner** | **List[str]** |  | [optional] 
**created_at** | **datetime** |  | [optional] 
**created_by** | **str** |  | [optional] 
**name** | **str** |  | 
**read** | **bool** |  | 
**write** | **bool** |  | 
**remote** | **bool** |  | 
**token** | **str** |  | 
**is_functional** | **bool** |  | [optional] 

## Example

```python
from py_scibec_openapi_client.models.access_account import AccessAccount

# TODO update the JSON string below
json = "{}"
# create an instance of AccessAccount from a JSON string
access_account_instance = AccessAccount.from_json(json)
# print the JSON string representation of the object
print(AccessAccount.to_json())

# convert the object into a dict
access_account_dict = access_account_instance.to_dict()
# create an instance of AccessAccount from a dict
access_account_form_dict = access_account.from_dict(access_account_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


