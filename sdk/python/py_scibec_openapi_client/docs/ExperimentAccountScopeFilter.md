# ExperimentAccountScopeFilter


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**offset** | **int** |  | [optional] 
**limit** | **int** |  | [optional] 
**skip** | **int** |  | [optional] 
**order** | [**AccessAccountFilterOrder**](AccessAccountFilterOrder.md) |  | [optional] 
**where** | **Dict[str, object]** |  | [optional] 
**fields** | [**AccessConfigScopeFilterFields**](AccessConfigScopeFilterFields.md) |  | [optional] 
**include** | **List[Dict[str, object]]** |  | [optional] 

## Example

```python
from py_scibec_openapi_client.models.experiment_account_scope_filter import ExperimentAccountScopeFilter

# TODO update the JSON string below
json = "{}"
# create an instance of ExperimentAccountScopeFilter from a JSON string
experiment_account_scope_filter_instance = ExperimentAccountScopeFilter.from_json(json)
# print the JSON string representation of the object
print(ExperimentAccountScopeFilter.to_json())

# convert the object into a dict
experiment_account_scope_filter_dict = experiment_account_scope_filter_instance.to_dict()
# create an instance of ExperimentAccountScopeFilter from a dict
experiment_account_scope_filter_form_dict = experiment_account_scope_filter.from_dict(experiment_account_scope_filter_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


