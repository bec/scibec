# py_scibec_openapi_client.ExperimentAccountControllerApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**experiment_account_controller_count**](ExperimentAccountControllerApi.md#experiment_account_controller_count) | **GET** /experiment-accounts/count | 
[**experiment_account_controller_create**](ExperimentAccountControllerApi.md#experiment_account_controller_create) | **POST** /experiment-accounts | 
[**experiment_account_controller_delete_by_id**](ExperimentAccountControllerApi.md#experiment_account_controller_delete_by_id) | **DELETE** /experiment-accounts/{id} | 
[**experiment_account_controller_find**](ExperimentAccountControllerApi.md#experiment_account_controller_find) | **GET** /experiment-accounts | 
[**experiment_account_controller_find_by_id**](ExperimentAccountControllerApi.md#experiment_account_controller_find_by_id) | **GET** /experiment-accounts/{id} | 
[**experiment_account_controller_update_all**](ExperimentAccountControllerApi.md#experiment_account_controller_update_all) | **PATCH** /experiment-accounts | 
[**experiment_account_controller_update_by_id**](ExperimentAccountControllerApi.md#experiment_account_controller_update_by_id) | **PATCH** /experiment-accounts/{id} | 


# **experiment_account_controller_count**
> LoopbackCount experiment_account_controller_count(where=where)



### Example

* Bearer (JWT) Authentication (jwt):

```python
import py_scibec_openapi_client
from py_scibec_openapi_client.models.loopback_count import LoopbackCount
from py_scibec_openapi_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = py_scibec_openapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization (JWT): jwt
configuration = py_scibec_openapi_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
with py_scibec_openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = py_scibec_openapi_client.ExperimentAccountControllerApi(api_client)
    where = None # Dict[str, object] |  (optional)

    try:
        api_response = api_instance.experiment_account_controller_count(where=where)
        print("The response of ExperimentAccountControllerApi->experiment_account_controller_count:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling ExperimentAccountControllerApi->experiment_account_controller_count: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | [**Dict[str, object]**](object.md)|  | [optional] 

### Return type

[**LoopbackCount**](LoopbackCount.md)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | ExperimentAccount model count |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **experiment_account_controller_create**
> ExperimentAccount experiment_account_controller_create(new_experiment_account=new_experiment_account)



### Example

* Bearer (JWT) Authentication (jwt):

```python
import py_scibec_openapi_client
from py_scibec_openapi_client.models.experiment_account import ExperimentAccount
from py_scibec_openapi_client.models.new_experiment_account import NewExperimentAccount
from py_scibec_openapi_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = py_scibec_openapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization (JWT): jwt
configuration = py_scibec_openapi_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
with py_scibec_openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = py_scibec_openapi_client.ExperimentAccountControllerApi(api_client)
    new_experiment_account = py_scibec_openapi_client.NewExperimentAccount() # NewExperimentAccount |  (optional)

    try:
        api_response = api_instance.experiment_account_controller_create(new_experiment_account=new_experiment_account)
        print("The response of ExperimentAccountControllerApi->experiment_account_controller_create:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling ExperimentAccountControllerApi->experiment_account_controller_create: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **new_experiment_account** | [**NewExperimentAccount**](NewExperimentAccount.md)|  | [optional] 

### Return type

[**ExperimentAccount**](ExperimentAccount.md)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | ExperimentAccount model instance |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **experiment_account_controller_delete_by_id**
> experiment_account_controller_delete_by_id(id)



### Example

* Bearer (JWT) Authentication (jwt):

```python
import py_scibec_openapi_client
from py_scibec_openapi_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = py_scibec_openapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization (JWT): jwt
configuration = py_scibec_openapi_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
with py_scibec_openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = py_scibec_openapi_client.ExperimentAccountControllerApi(api_client)
    id = 'id_example' # str | 

    try:
        api_instance.experiment_account_controller_delete_by_id(id)
    except Exception as e:
        print("Exception when calling ExperimentAccountControllerApi->experiment_account_controller_delete_by_id: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**|  | 

### Return type

void (empty response body)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | ExperimentAccount DELETE success |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **experiment_account_controller_find**
> List[ExperimentAccountWithRelations] experiment_account_controller_find(filter=filter)



### Example

* Bearer (JWT) Authentication (jwt):

```python
import py_scibec_openapi_client
from py_scibec_openapi_client.models.experiment_account_filter1 import ExperimentAccountFilter1
from py_scibec_openapi_client.models.experiment_account_with_relations import ExperimentAccountWithRelations
from py_scibec_openapi_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = py_scibec_openapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization (JWT): jwt
configuration = py_scibec_openapi_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
with py_scibec_openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = py_scibec_openapi_client.ExperimentAccountControllerApi(api_client)
    filter = py_scibec_openapi_client.ExperimentAccountFilter1() # ExperimentAccountFilter1 |  (optional)

    try:
        api_response = api_instance.experiment_account_controller_find(filter=filter)
        print("The response of ExperimentAccountControllerApi->experiment_account_controller_find:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling ExperimentAccountControllerApi->experiment_account_controller_find: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | [**ExperimentAccountFilter1**](.md)|  | [optional] 

### Return type

[**List[ExperimentAccountWithRelations]**](ExperimentAccountWithRelations.md)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Array of ExperimentAccount model instances |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **experiment_account_controller_find_by_id**
> ExperimentAccountWithRelations experiment_account_controller_find_by_id(id, filter=filter)



### Example

* Bearer (JWT) Authentication (jwt):

```python
import py_scibec_openapi_client
from py_scibec_openapi_client.models.experiment_account_filter import ExperimentAccountFilter
from py_scibec_openapi_client.models.experiment_account_with_relations import ExperimentAccountWithRelations
from py_scibec_openapi_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = py_scibec_openapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization (JWT): jwt
configuration = py_scibec_openapi_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
with py_scibec_openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = py_scibec_openapi_client.ExperimentAccountControllerApi(api_client)
    id = 'id_example' # str | 
    filter = py_scibec_openapi_client.ExperimentAccountFilter() # ExperimentAccountFilter |  (optional)

    try:
        api_response = api_instance.experiment_account_controller_find_by_id(id, filter=filter)
        print("The response of ExperimentAccountControllerApi->experiment_account_controller_find_by_id:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling ExperimentAccountControllerApi->experiment_account_controller_find_by_id: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**|  | 
 **filter** | [**ExperimentAccountFilter**](.md)|  | [optional] 

### Return type

[**ExperimentAccountWithRelations**](ExperimentAccountWithRelations.md)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | ExperimentAccount model instance |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **experiment_account_controller_update_all**
> LoopbackCount experiment_account_controller_update_all(where=where, experiment_account_partial=experiment_account_partial)



### Example

* Bearer (JWT) Authentication (jwt):

```python
import py_scibec_openapi_client
from py_scibec_openapi_client.models.experiment_account_partial import ExperimentAccountPartial
from py_scibec_openapi_client.models.loopback_count import LoopbackCount
from py_scibec_openapi_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = py_scibec_openapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization (JWT): jwt
configuration = py_scibec_openapi_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
with py_scibec_openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = py_scibec_openapi_client.ExperimentAccountControllerApi(api_client)
    where = None # Dict[str, object] |  (optional)
    experiment_account_partial = py_scibec_openapi_client.ExperimentAccountPartial() # ExperimentAccountPartial |  (optional)

    try:
        api_response = api_instance.experiment_account_controller_update_all(where=where, experiment_account_partial=experiment_account_partial)
        print("The response of ExperimentAccountControllerApi->experiment_account_controller_update_all:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling ExperimentAccountControllerApi->experiment_account_controller_update_all: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | [**Dict[str, object]**](object.md)|  | [optional] 
 **experiment_account_partial** | [**ExperimentAccountPartial**](ExperimentAccountPartial.md)|  | [optional] 

### Return type

[**LoopbackCount**](LoopbackCount.md)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | ExperimentAccount PATCH success count |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **experiment_account_controller_update_by_id**
> experiment_account_controller_update_by_id(id, experiment_account_partial=experiment_account_partial)



### Example

* Bearer (JWT) Authentication (jwt):

```python
import py_scibec_openapi_client
from py_scibec_openapi_client.models.experiment_account_partial import ExperimentAccountPartial
from py_scibec_openapi_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = py_scibec_openapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization (JWT): jwt
configuration = py_scibec_openapi_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
with py_scibec_openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = py_scibec_openapi_client.ExperimentAccountControllerApi(api_client)
    id = 'id_example' # str | 
    experiment_account_partial = py_scibec_openapi_client.ExperimentAccountPartial() # ExperimentAccountPartial |  (optional)

    try:
        api_instance.experiment_account_controller_update_by_id(id, experiment_account_partial=experiment_account_partial)
    except Exception as e:
        print("Exception when calling ExperimentAccountControllerApi->experiment_account_controller_update_by_id: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**|  | 
 **experiment_account_partial** | [**ExperimentAccountPartial**](ExperimentAccountPartial.md)|  | [optional] 

### Return type

void (empty response body)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | ExperimentAccount PATCH success |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

