# BeamlineIncludeFilterItems


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**relation** | **str** |  | [optional] 
**scope** | [**BeamlineScopeFilter**](BeamlineScopeFilter.md) |  | [optional] 

## Example

```python
from py_scibec_openapi_client.models.beamline_include_filter_items import BeamlineIncludeFilterItems

# TODO update the JSON string below
json = "{}"
# create an instance of BeamlineIncludeFilterItems from a JSON string
beamline_include_filter_items_instance = BeamlineIncludeFilterItems.from_json(json)
# print the JSON string representation of the object
print(BeamlineIncludeFilterItems.to_json())

# convert the object into a dict
beamline_include_filter_items_dict = beamline_include_filter_items_instance.to_dict()
# create an instance of BeamlineIncludeFilterItems from a dict
beamline_include_filter_items_form_dict = beamline_include_filter_items.from_dict(beamline_include_filter_items_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


