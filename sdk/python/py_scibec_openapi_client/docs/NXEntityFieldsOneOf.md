# NXEntityFieldsOneOf


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **bool** |  | [optional] 
**read_acl** | **bool** |  | [optional] 
**write_acl** | **bool** |  | [optional] 
**owner** | **bool** |  | [optional] 
**created_at** | **bool** |  | [optional] 
**created_by** | **bool** |  | [optional] 
**name** | **bool** |  | [optional] 
**type** | **bool** |  | [optional] 
**description** | **bool** |  | [optional] 
**data** | **bool** |  | [optional] 
**n_xclass** | **bool** |  | [optional] 
**parent_id** | **bool** |  | [optional] 
**scan_id** | **bool** |  | [optional] 

## Example

```python
from py_scibec_openapi_client.models.nx_entity_fields_one_of import NXEntityFieldsOneOf

# TODO update the JSON string below
json = "{}"
# create an instance of NXEntityFieldsOneOf from a JSON string
nx_entity_fields_one_of_instance = NXEntityFieldsOneOf.from_json(json)
# print the JSON string representation of the object
print(NXEntityFieldsOneOf.to_json())

# convert the object into a dict
nx_entity_fields_one_of_dict = nx_entity_fields_one_of_instance.to_dict()
# create an instance of NXEntityFieldsOneOf from a dict
nx_entity_fields_one_of_form_dict = nx_entity_fields_one_of.from_dict(nx_entity_fields_one_of_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


