# NewSession

(tsType: Omit<Session, 'id'>, schemaOptions: { title: 'NewSession', exclude: [ 'id' ] })

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**read_acl** | **List[str]** |  | [optional] 
**write_acl** | **List[str]** |  | [optional] 
**owner** | **List[str]** |  | [optional] 
**created_at** | **datetime** |  | [optional] 
**created_by** | **str** |  | [optional] 
**name** | **str** |  | 
**experiment_id** | **str** | Session to which this device belongs. | [optional] 
**session_config** | **object** |  | [optional] 

## Example

```python
from py_scibec_openapi_client.models.new_session import NewSession

# TODO update the JSON string below
json = "{}"
# create an instance of NewSession from a JSON string
new_session_instance = NewSession.from_json(json)
# print the JSON string representation of the object
print(NewSession.to_json())

# convert the object into a dict
new_session_dict = new_session_instance.to_dict()
# create an instance of NewSession from a dict
new_session_form_dict = new_session.from_dict(new_session_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


