# ExperimentAccountWithRelations

(tsType: ExperimentAccountWithRelations, schemaOptions: { includeRelations: true })

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** |  | [optional] 
**read_acl** | **List[str]** |  | [optional] 
**write_acl** | **List[str]** |  | [optional] 
**owner** | **List[str]** |  | [optional] 
**created_at** | **datetime** |  | [optional] 
**created_by** | **str** |  | [optional] 
**name** | **str** |  | 
**read** | **bool** |  | 
**write** | **bool** |  | 
**remote** | **bool** |  | 
**token** | **str** |  | 
**is_functional** | **bool** |  | [optional] 
**experiment_id** | **str** | Experiment to which this account belongs. | [optional] 
**experiment** | [**ExperimentWithRelations**](ExperimentWithRelations.md) |  | [optional] 

## Example

```python
from py_scibec_openapi_client.models.experiment_account_with_relations import ExperimentAccountWithRelations

# TODO update the JSON string below
json = "{}"
# create an instance of ExperimentAccountWithRelations from a JSON string
experiment_account_with_relations_instance = ExperimentAccountWithRelations.from_json(json)
# print the JSON string representation of the object
print(ExperimentAccountWithRelations.to_json())

# convert the object into a dict
experiment_account_with_relations_dict = experiment_account_with_relations_instance.to_dict()
# create an instance of ExperimentAccountWithRelations from a dict
experiment_account_with_relations_form_dict = experiment_account_with_relations.from_dict(experiment_account_with_relations_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


