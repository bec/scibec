# ScanPartial

(tsType: Partial<Scan>, schemaOptions: { partial: true })

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** |  | [optional] 
**read_acl** | **List[str]** |  | [optional] 
**write_acl** | **List[str]** |  | [optional] 
**owner** | **List[str]** |  | [optional] 
**created_at** | **datetime** |  | [optional] 
**created_by** | **str** |  | [optional] 
**scan_type** | **str** |  | [optional] 
**scan_parameter** | **object** |  | [optional] 
**user_parameter** | **object** |  | [optional] 
**scan_id** | **str** |  | [optional] 
**request_id** | **str** |  | [optional] 
**queue_id** | **str** |  | [optional] 
**exit_status** | **str** |  | [optional] 
**scan_number** | **float** |  | [optional] 
**metadata** | **object** |  | [optional] 
**files** | **object** |  | [optional] 
**session_id** | **str** |  | [optional] 
**dataset_id** | **str** |  | [optional] 
**experiment_id** | **str** |  | [optional] 

## Example

```python
from py_scibec_openapi_client.models.scan_partial import ScanPartial

# TODO update the JSON string below
json = "{}"
# create an instance of ScanPartial from a JSON string
scan_partial_instance = ScanPartial.from_json(json)
# print the JSON string representation of the object
print(ScanPartial.to_json())

# convert the object into a dict
scan_partial_dict = scan_partial_instance.to_dict()
# create an instance of ScanPartial from a dict
scan_partial_form_dict = scan_partial.from_dict(scan_partial_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


