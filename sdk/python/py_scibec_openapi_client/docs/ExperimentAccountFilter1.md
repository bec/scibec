# ExperimentAccountFilter1


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**offset** | **int** |  | [optional] 
**limit** | **int** |  | [optional] 
**skip** | **int** |  | [optional] 
**order** | [**AccessAccountFilterOrder**](AccessAccountFilterOrder.md) |  | [optional] 
**where** | **Dict[str, object]** |  | [optional] 
**fields** | [**ExperimentAccountFields**](ExperimentAccountFields.md) |  | [optional] 
**include** | [**List[ExperimentAccountIncludeFilterInner]**](ExperimentAccountIncludeFilterInner.md) |  | [optional] 

## Example

```python
from py_scibec_openapi_client.models.experiment_account_filter1 import ExperimentAccountFilter1

# TODO update the JSON string below
json = "{}"
# create an instance of ExperimentAccountFilter1 from a JSON string
experiment_account_filter1_instance = ExperimentAccountFilter1.from_json(json)
# print the JSON string representation of the object
print(ExperimentAccountFilter1.to_json())

# convert the object into a dict
experiment_account_filter1_dict = experiment_account_filter1_instance.to_dict()
# create an instance of ExperimentAccountFilter1 from a dict
experiment_account_filter1_form_dict = experiment_account_filter1.from_dict(experiment_account_filter1_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


