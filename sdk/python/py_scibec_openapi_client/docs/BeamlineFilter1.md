# BeamlineFilter1


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**offset** | **int** |  | [optional] 
**limit** | **int** |  | [optional] 
**skip** | **int** |  | [optional] 
**order** | [**AccessAccountFilterOrder**](AccessAccountFilterOrder.md) |  | [optional] 
**where** | **Dict[str, object]** |  | [optional] 
**fields** | [**BeamlineFields**](BeamlineFields.md) |  | [optional] 
**include** | [**List[BeamlineIncludeFilterInner]**](BeamlineIncludeFilterInner.md) |  | [optional] 

## Example

```python
from py_scibec_openapi_client.models.beamline_filter1 import BeamlineFilter1

# TODO update the JSON string below
json = "{}"
# create an instance of BeamlineFilter1 from a JSON string
beamline_filter1_instance = BeamlineFilter1.from_json(json)
# print the JSON string representation of the object
print(BeamlineFilter1.to_json())

# convert the object into a dict
beamline_filter1_dict = beamline_filter1_instance.to_dict()
# create an instance of BeamlineFilter1 from a dict
beamline_filter1_form_dict = beamline_filter1.from_dict(beamline_filter1_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


