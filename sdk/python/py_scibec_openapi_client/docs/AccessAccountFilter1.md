# AccessAccountFilter1


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**offset** | **int** |  | [optional] 
**limit** | **int** |  | [optional] 
**skip** | **int** |  | [optional] 
**order** | [**AccessAccountFilterOrder**](AccessAccountFilterOrder.md) |  | [optional] 
**where** | **Dict[str, object]** |  | [optional] 
**fields** | [**AccessAccountFields**](AccessAccountFields.md) |  | [optional] 

## Example

```python
from py_scibec_openapi_client.models.access_account_filter1 import AccessAccountFilter1

# TODO update the JSON string below
json = "{}"
# create an instance of AccessAccountFilter1 from a JSON string
access_account_filter1_instance = AccessAccountFilter1.from_json(json)
# print the JSON string representation of the object
print(AccessAccountFilter1.to_json())

# convert the object into a dict
access_account_filter1_dict = access_account_filter1_instance.to_dict()
# create an instance of AccessAccountFilter1 from a dict
access_account_filter1_form_dict = access_account_filter1.from_dict(access_account_filter1_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


