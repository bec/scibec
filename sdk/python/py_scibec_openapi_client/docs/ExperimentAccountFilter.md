# ExperimentAccountFilter


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**offset** | **int** |  | [optional] 
**limit** | **int** |  | [optional] 
**skip** | **int** |  | [optional] 
**order** | [**AccessAccountFilterOrder**](AccessAccountFilterOrder.md) |  | [optional] 
**fields** | [**ExperimentAccountFields**](ExperimentAccountFields.md) |  | [optional] 
**include** | [**List[ExperimentAccountIncludeFilterInner]**](ExperimentAccountIncludeFilterInner.md) |  | [optional] 

## Example

```python
from py_scibec_openapi_client.models.experiment_account_filter import ExperimentAccountFilter

# TODO update the JSON string below
json = "{}"
# create an instance of ExperimentAccountFilter from a JSON string
experiment_account_filter_instance = ExperimentAccountFilter.from_json(json)
# print the JSON string representation of the object
print(ExperimentAccountFilter.to_json())

# convert the object into a dict
experiment_account_filter_dict = experiment_account_filter_instance.to_dict()
# create an instance of ExperimentAccountFilter from a dict
experiment_account_filter_form_dict = experiment_account_filter.from_dict(experiment_account_filter_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


