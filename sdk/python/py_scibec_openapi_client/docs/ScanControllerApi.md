# py_scibec_openapi_client.ScanControllerApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**scan_controller_count**](ScanControllerApi.md#scan_controller_count) | **GET** /scans/count | 
[**scan_controller_create**](ScanControllerApi.md#scan_controller_create) | **POST** /scans | 
[**scan_controller_delete_by_id**](ScanControllerApi.md#scan_controller_delete_by_id) | **DELETE** /scans/{id} | 
[**scan_controller_find**](ScanControllerApi.md#scan_controller_find) | **GET** /scans | 
[**scan_controller_find_by_id**](ScanControllerApi.md#scan_controller_find_by_id) | **GET** /scans/{id} | 
[**scan_controller_update_all**](ScanControllerApi.md#scan_controller_update_all) | **PATCH** /scans | 
[**scan_controller_update_by_id**](ScanControllerApi.md#scan_controller_update_by_id) | **PATCH** /scans/{id} | 


# **scan_controller_count**
> LoopbackCount scan_controller_count(where=where)



### Example

* Bearer (JWT) Authentication (jwt):

```python
import py_scibec_openapi_client
from py_scibec_openapi_client.models.loopback_count import LoopbackCount
from py_scibec_openapi_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = py_scibec_openapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization (JWT): jwt
configuration = py_scibec_openapi_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
with py_scibec_openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = py_scibec_openapi_client.ScanControllerApi(api_client)
    where = None # Dict[str, object] |  (optional)

    try:
        api_response = api_instance.scan_controller_count(where=where)
        print("The response of ScanControllerApi->scan_controller_count:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling ScanControllerApi->scan_controller_count: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | [**Dict[str, object]**](object.md)|  | [optional] 

### Return type

[**LoopbackCount**](LoopbackCount.md)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Scan model count |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **scan_controller_create**
> Scan scan_controller_create(new_scan=new_scan)



### Example

* Bearer (JWT) Authentication (jwt):

```python
import py_scibec_openapi_client
from py_scibec_openapi_client.models.new_scan import NewScan
from py_scibec_openapi_client.models.scan import Scan
from py_scibec_openapi_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = py_scibec_openapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization (JWT): jwt
configuration = py_scibec_openapi_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
with py_scibec_openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = py_scibec_openapi_client.ScanControllerApi(api_client)
    new_scan = py_scibec_openapi_client.NewScan() # NewScan |  (optional)

    try:
        api_response = api_instance.scan_controller_create(new_scan=new_scan)
        print("The response of ScanControllerApi->scan_controller_create:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling ScanControllerApi->scan_controller_create: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **new_scan** | [**NewScan**](NewScan.md)|  | [optional] 

### Return type

[**Scan**](Scan.md)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Scan model instance |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **scan_controller_delete_by_id**
> scan_controller_delete_by_id(id)



### Example

* Bearer (JWT) Authentication (jwt):

```python
import py_scibec_openapi_client
from py_scibec_openapi_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = py_scibec_openapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization (JWT): jwt
configuration = py_scibec_openapi_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
with py_scibec_openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = py_scibec_openapi_client.ScanControllerApi(api_client)
    id = 'id_example' # str | 

    try:
        api_instance.scan_controller_delete_by_id(id)
    except Exception as e:
        print("Exception when calling ScanControllerApi->scan_controller_delete_by_id: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**|  | 

### Return type

void (empty response body)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | Scan DELETE success |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **scan_controller_find**
> List[ScanWithRelations] scan_controller_find(filter=filter)



### Example

* Bearer (JWT) Authentication (jwt):

```python
import py_scibec_openapi_client
from py_scibec_openapi_client.models.scan_filter1 import ScanFilter1
from py_scibec_openapi_client.models.scan_with_relations import ScanWithRelations
from py_scibec_openapi_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = py_scibec_openapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization (JWT): jwt
configuration = py_scibec_openapi_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
with py_scibec_openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = py_scibec_openapi_client.ScanControllerApi(api_client)
    filter = py_scibec_openapi_client.ScanFilter1() # ScanFilter1 |  (optional)

    try:
        api_response = api_instance.scan_controller_find(filter=filter)
        print("The response of ScanControllerApi->scan_controller_find:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling ScanControllerApi->scan_controller_find: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | [**ScanFilter1**](.md)|  | [optional] 

### Return type

[**List[ScanWithRelations]**](ScanWithRelations.md)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Array of Scan model instances |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **scan_controller_find_by_id**
> ScanWithRelations scan_controller_find_by_id(id, filter=filter)



### Example

* Bearer (JWT) Authentication (jwt):

```python
import py_scibec_openapi_client
from py_scibec_openapi_client.models.scan_filter import ScanFilter
from py_scibec_openapi_client.models.scan_with_relations import ScanWithRelations
from py_scibec_openapi_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = py_scibec_openapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization (JWT): jwt
configuration = py_scibec_openapi_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
with py_scibec_openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = py_scibec_openapi_client.ScanControllerApi(api_client)
    id = 'id_example' # str | 
    filter = py_scibec_openapi_client.ScanFilter() # ScanFilter |  (optional)

    try:
        api_response = api_instance.scan_controller_find_by_id(id, filter=filter)
        print("The response of ScanControllerApi->scan_controller_find_by_id:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling ScanControllerApi->scan_controller_find_by_id: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**|  | 
 **filter** | [**ScanFilter**](.md)|  | [optional] 

### Return type

[**ScanWithRelations**](ScanWithRelations.md)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Scan model instance |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **scan_controller_update_all**
> LoopbackCount scan_controller_update_all(where=where, scan_partial=scan_partial)



### Example

* Bearer (JWT) Authentication (jwt):

```python
import py_scibec_openapi_client
from py_scibec_openapi_client.models.loopback_count import LoopbackCount
from py_scibec_openapi_client.models.scan_partial import ScanPartial
from py_scibec_openapi_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = py_scibec_openapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization (JWT): jwt
configuration = py_scibec_openapi_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
with py_scibec_openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = py_scibec_openapi_client.ScanControllerApi(api_client)
    where = None # Dict[str, object] |  (optional)
    scan_partial = py_scibec_openapi_client.ScanPartial() # ScanPartial |  (optional)

    try:
        api_response = api_instance.scan_controller_update_all(where=where, scan_partial=scan_partial)
        print("The response of ScanControllerApi->scan_controller_update_all:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling ScanControllerApi->scan_controller_update_all: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | [**Dict[str, object]**](object.md)|  | [optional] 
 **scan_partial** | [**ScanPartial**](ScanPartial.md)|  | [optional] 

### Return type

[**LoopbackCount**](LoopbackCount.md)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Scan PATCH success count |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **scan_controller_update_by_id**
> scan_controller_update_by_id(id, scan_partial=scan_partial)



### Example

* Bearer (JWT) Authentication (jwt):

```python
import py_scibec_openapi_client
from py_scibec_openapi_client.models.scan_partial import ScanPartial
from py_scibec_openapi_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = py_scibec_openapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization (JWT): jwt
configuration = py_scibec_openapi_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
with py_scibec_openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = py_scibec_openapi_client.ScanControllerApi(api_client)
    id = 'id_example' # str | 
    scan_partial = py_scibec_openapi_client.ScanPartial() # ScanPartial |  (optional)

    try:
        api_instance.scan_controller_update_by_id(id, scan_partial=scan_partial)
    except Exception as e:
        print("Exception when calling ScanControllerApi->scan_controller_update_by_id: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**|  | 
 **scan_partial** | [**ScanPartial**](ScanPartial.md)|  | [optional] 

### Return type

void (empty response body)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | Scan PATCH success |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

