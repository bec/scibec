# FunctionalAccountFields


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **bool** |  | [optional] 
**read_acl** | **bool** |  | [optional] 
**write_acl** | **bool** |  | [optional] 
**owner** | **bool** |  | [optional] 
**created_at** | **bool** |  | [optional] 
**created_by** | **bool** |  | [optional] 
**name** | **bool** |  | [optional] 
**read** | **bool** |  | [optional] 
**write** | **bool** |  | [optional] 
**remote** | **bool** |  | [optional] 
**token** | **bool** |  | [optional] 
**is_functional** | **bool** |  | [optional] 
**access_config_id** | **bool** |  | [optional] 

## Example

```python
from py_scibec_openapi_client.models.functional_account_fields import FunctionalAccountFields

# TODO update the JSON string below
json = "{}"
# create an instance of FunctionalAccountFields from a JSON string
functional_account_fields_instance = FunctionalAccountFields.from_json(json)
# print the JSON string representation of the object
print(FunctionalAccountFields.to_json())

# convert the object into a dict
functional_account_fields_dict = functional_account_fields_instance.to_dict()
# create an instance of FunctionalAccountFields from a dict
functional_account_fields_form_dict = functional_account_fields.from_dict(functional_account_fields_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


