# ExperimentAccountPartial

(tsType: Partial<ExperimentAccount>, schemaOptions: { partial: true })

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** |  | [optional] 
**read_acl** | **List[str]** |  | [optional] 
**write_acl** | **List[str]** |  | [optional] 
**owner** | **List[str]** |  | [optional] 
**created_at** | **datetime** |  | [optional] 
**created_by** | **str** |  | [optional] 
**name** | **str** |  | [optional] 
**read** | **bool** |  | [optional] 
**write** | **bool** |  | [optional] 
**remote** | **bool** |  | [optional] 
**token** | **str** |  | [optional] 
**is_functional** | **bool** |  | [optional] 
**experiment_id** | **str** | Experiment to which this account belongs. | [optional] 

## Example

```python
from py_scibec_openapi_client.models.experiment_account_partial import ExperimentAccountPartial

# TODO update the JSON string below
json = "{}"
# create an instance of ExperimentAccountPartial from a JSON string
experiment_account_partial_instance = ExperimentAccountPartial.from_json(json)
# print the JSON string representation of the object
print(ExperimentAccountPartial.to_json())

# convert the object into a dict
experiment_account_partial_dict = experiment_account_partial_instance.to_dict()
# create an instance of ExperimentAccountPartial from a dict
experiment_account_partial_form_dict = experiment_account_partial.from_dict(experiment_account_partial_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


