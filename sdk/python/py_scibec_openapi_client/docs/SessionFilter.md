# SessionFilter


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**offset** | **int** |  | [optional] 
**limit** | **int** |  | [optional] 
**skip** | **int** |  | [optional] 
**order** | [**AccessAccountFilterOrder**](AccessAccountFilterOrder.md) |  | [optional] 
**fields** | [**SessionFields**](SessionFields.md) |  | [optional] 
**include** | [**List[SessionIncludeFilterInner]**](SessionIncludeFilterInner.md) |  | [optional] 

## Example

```python
from py_scibec_openapi_client.models.session_filter import SessionFilter

# TODO update the JSON string below
json = "{}"
# create an instance of SessionFilter from a JSON string
session_filter_instance = SessionFilter.from_json(json)
# print the JSON string representation of the object
print(SessionFilter.to_json())

# convert the object into a dict
session_filter_dict = session_filter_instance.to_dict()
# create an instance of SessionFilter from a dict
session_filter_form_dict = session_filter.from_dict(session_filter_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


