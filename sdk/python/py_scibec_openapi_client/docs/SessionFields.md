# SessionFields


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **bool** |  | [optional] 
**read_acl** | **bool** |  | [optional] 
**write_acl** | **bool** |  | [optional] 
**owner** | **bool** |  | [optional] 
**created_at** | **bool** |  | [optional] 
**created_by** | **bool** |  | [optional] 
**name** | **bool** |  | [optional] 
**experiment_id** | **bool** |  | [optional] 
**session_config** | **bool** |  | [optional] 

## Example

```python
from py_scibec_openapi_client.models.session_fields import SessionFields

# TODO update the JSON string below
json = "{}"
# create an instance of SessionFields from a JSON string
session_fields_instance = SessionFields.from_json(json)
# print the JSON string representation of the object
print(SessionFields.to_json())

# convert the object into a dict
session_fields_dict = session_fields_instance.to_dict()
# create an instance of SessionFields from a dict
session_fields_form_dict = session_fields.from_dict(session_fields_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


