# LoopbackCount


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**count** | **float** |  | [optional] 

## Example

```python
from py_scibec_openapi_client.models.loopback_count import LoopbackCount

# TODO update the JSON string below
json = "{}"
# create an instance of LoopbackCount from a JSON string
loopback_count_instance = LoopbackCount.from_json(json)
# print the JSON string representation of the object
print(LoopbackCount.to_json())

# convert the object into a dict
loopback_count_dict = loopback_count_instance.to_dict()
# create an instance of LoopbackCount from a dict
loopback_count_form_dict = loopback_count.from_dict(loopback_count_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


