# AccessAccountPartial

(tsType: Partial<AccessAccount>, schemaOptions: { partial: true })

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** |  | [optional] 
**read_acl** | **List[str]** |  | [optional] 
**write_acl** | **List[str]** |  | [optional] 
**owner** | **List[str]** |  | [optional] 
**created_at** | **datetime** |  | [optional] 
**created_by** | **str** |  | [optional] 
**name** | **str** |  | [optional] 
**read** | **bool** |  | [optional] 
**write** | **bool** |  | [optional] 
**remote** | **bool** |  | [optional] 
**token** | **str** |  | [optional] 
**is_functional** | **bool** |  | [optional] 

## Example

```python
from py_scibec_openapi_client.models.access_account_partial import AccessAccountPartial

# TODO update the JSON string below
json = "{}"
# create an instance of AccessAccountPartial from a JSON string
access_account_partial_instance = AccessAccountPartial.from_json(json)
# print the JSON string representation of the object
print(AccessAccountPartial.to_json())

# convert the object into a dict
access_account_partial_dict = access_account_partial_instance.to_dict()
# create an instance of AccessAccountPartial from a dict
access_account_partial_form_dict = access_account_partial.from_dict(access_account_partial_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


