# DeviceFieldsOneOf


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **bool** |  | [optional] 
**read_acl** | **bool** |  | [optional] 
**write_acl** | **bool** |  | [optional] 
**owner** | **bool** |  | [optional] 
**created_at** | **bool** |  | [optional] 
**created_by** | **bool** |  | [optional] 
**name** | **bool** |  | [optional] 
**description** | **bool** |  | [optional] 
**parent_id** | **bool** |  | [optional] 
**session_id** | **bool** |  | [optional] 
**enabled** | **bool** |  | [optional] 
**read_only** | **bool** |  | [optional] 
**software_trigger** | **bool** |  | [optional] 
**device_class** | **bool** |  | [optional] 
**device_tags** | **bool** |  | [optional] 
**device_config** | **bool** |  | [optional] 
**readout_priority** | **bool** |  | [optional] 
**on_failure** | **bool** |  | [optional] 
**user_parameter** | **bool** |  | [optional] 

## Example

```python
from py_scibec_openapi_client.models.device_fields_one_of import DeviceFieldsOneOf

# TODO update the JSON string below
json = "{}"
# create an instance of DeviceFieldsOneOf from a JSON string
device_fields_one_of_instance = DeviceFieldsOneOf.from_json(json)
# print the JSON string representation of the object
print(DeviceFieldsOneOf.to_json())

# convert the object into a dict
device_fields_one_of_dict = device_fields_one_of_instance.to_dict()
# create an instance of DeviceFieldsOneOf from a dict
device_fields_one_of_form_dict = device_fields_one_of.from_dict(device_fields_one_of_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


