# SessionIncludeFilterInner


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**relation** | **str** |  | [optional] 
**scope** | [**SessionScopeFilter**](SessionScopeFilter.md) |  | [optional] 

## Example

```python
from py_scibec_openapi_client.models.session_include_filter_inner import SessionIncludeFilterInner

# TODO update the JSON string below
json = "{}"
# create an instance of SessionIncludeFilterInner from a JSON string
session_include_filter_inner_instance = SessionIncludeFilterInner.from_json(json)
# print the JSON string representation of the object
print(SessionIncludeFilterInner.to_json())

# convert the object into a dict
session_include_filter_inner_dict = session_include_filter_inner_instance.to_dict()
# create an instance of SessionIncludeFilterInner from a dict
session_include_filter_inner_form_dict = session_include_filter_inner.from_dict(session_include_filter_inner_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


