# ScanDataFields


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **bool** |  | [optional] 
**read_acl** | **bool** |  | [optional] 
**write_acl** | **bool** |  | [optional] 
**owner** | **bool** |  | [optional] 
**created_at** | **bool** |  | [optional] 
**created_by** | **bool** |  | [optional] 
**name** | **bool** |  | [optional] 
**data** | **bool** |  | [optional] 
**file_path** | **bool** |  | [optional] 
**scan_id** | **bool** |  | [optional] 

## Example

```python
from py_scibec_openapi_client.models.scan_data_fields import ScanDataFields

# TODO update the JSON string below
json = "{}"
# create an instance of ScanDataFields from a JSON string
scan_data_fields_instance = ScanDataFields.from_json(json)
# print the JSON string representation of the object
print(ScanDataFields.to_json())

# convert the object into a dict
scan_data_fields_dict = scan_data_fields_instance.to_dict()
# create an instance of ScanDataFields from a dict
scan_data_fields_form_dict = scan_data_fields.from_dict(scan_data_fields_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


