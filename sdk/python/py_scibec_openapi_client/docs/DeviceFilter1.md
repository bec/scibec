# DeviceFilter1


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**offset** | **int** |  | [optional] 
**limit** | **int** |  | [optional] 
**skip** | **int** |  | [optional] 
**order** | [**AccessAccountFilterOrder**](AccessAccountFilterOrder.md) |  | [optional] 
**where** | **Dict[str, object]** |  | [optional] 
**fields** | [**DeviceFields**](DeviceFields.md) |  | [optional] 
**include** | [**List[DeviceIncludeFilterInner]**](DeviceIncludeFilterInner.md) |  | [optional] 

## Example

```python
from py_scibec_openapi_client.models.device_filter1 import DeviceFilter1

# TODO update the JSON string below
json = "{}"
# create an instance of DeviceFilter1 from a JSON string
device_filter1_instance = DeviceFilter1.from_json(json)
# print the JSON string representation of the object
print(DeviceFilter1.to_json())

# convert the object into a dict
device_filter1_dict = device_filter1_instance.to_dict()
# create an instance of DeviceFilter1 from a dict
device_filter1_form_dict = device_filter1.from_dict(device_filter1_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


