# DatasetPartial

(tsType: Partial<Dataset>, schemaOptions: { partial: true })

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** |  | [optional] 
**read_acl** | **List[str]** |  | [optional] 
**write_acl** | **List[str]** |  | [optional] 
**owner** | **List[str]** |  | [optional] 
**created_at** | **datetime** |  | [optional] 
**created_by** | **str** |  | [optional] 
**name** | **str** |  | [optional] 
**number** | **float** |  | [optional] 
**experiment_id** | **str** |  | [optional] 

## Example

```python
from py_scibec_openapi_client.models.dataset_partial import DatasetPartial

# TODO update the JSON string below
json = "{}"
# create an instance of DatasetPartial from a JSON string
dataset_partial_instance = DatasetPartial.from_json(json)
# print the JSON string representation of the object
print(DatasetPartial.to_json())

# convert the object into a dict
dataset_partial_dict = dataset_partial_instance.to_dict()
# create an instance of DatasetPartial from a dict
dataset_partial_form_dict = dataset_partial.from_dict(dataset_partial_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


