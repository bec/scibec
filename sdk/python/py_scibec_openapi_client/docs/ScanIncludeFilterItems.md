# ScanIncludeFilterItems


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**relation** | **str** |  | [optional] 
**scope** | [**ScanScopeFilter**](ScanScopeFilter.md) |  | [optional] 

## Example

```python
from py_scibec_openapi_client.models.scan_include_filter_items import ScanIncludeFilterItems

# TODO update the JSON string below
json = "{}"
# create an instance of ScanIncludeFilterItems from a JSON string
scan_include_filter_items_instance = ScanIncludeFilterItems.from_json(json)
# print the JSON string representation of the object
print(ScanIncludeFilterItems.to_json())

# convert the object into a dict
scan_include_filter_items_dict = scan_include_filter_items_instance.to_dict()
# create an instance of ScanIncludeFilterItems from a dict
scan_include_filter_items_form_dict = scan_include_filter_items.from_dict(scan_include_filter_items_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


