# DatasetIncludeFilterItems


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**relation** | **str** |  | [optional] 
**scope** | [**DatasetScopeFilter**](DatasetScopeFilter.md) |  | [optional] 

## Example

```python
from py_scibec_openapi_client.models.dataset_include_filter_items import DatasetIncludeFilterItems

# TODO update the JSON string below
json = "{}"
# create an instance of DatasetIncludeFilterItems from a JSON string
dataset_include_filter_items_instance = DatasetIncludeFilterItems.from_json(json)
# print the JSON string representation of the object
print(DatasetIncludeFilterItems.to_json())

# convert the object into a dict
dataset_include_filter_items_dict = dataset_include_filter_items_instance.to_dict()
# create an instance of DatasetIncludeFilterItems from a dict
dataset_include_filter_items_form_dict = dataset_include_filter_items.from_dict(dataset_include_filter_items_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


