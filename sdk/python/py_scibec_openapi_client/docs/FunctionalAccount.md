# FunctionalAccount


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** |  | [optional] 
**read_acl** | **List[str]** |  | [optional] 
**write_acl** | **List[str]** |  | [optional] 
**owner** | **List[str]** |  | [optional] 
**created_at** | **datetime** |  | [optional] 
**created_by** | **str** |  | [optional] 
**name** | **str** |  | 
**read** | **bool** |  | 
**write** | **bool** |  | 
**remote** | **bool** |  | 
**token** | **str** |  | 
**is_functional** | **bool** |  | [optional] 
**access_config_id** | **str** | AccessConfig to which this account belongs. | [optional] 

## Example

```python
from py_scibec_openapi_client.models.functional_account import FunctionalAccount

# TODO update the JSON string below
json = "{}"
# create an instance of FunctionalAccount from a JSON string
functional_account_instance = FunctionalAccount.from_json(json)
# print the JSON string representation of the object
print(FunctionalAccount.to_json())

# convert the object into a dict
functional_account_dict = functional_account_instance.to_dict()
# create an instance of FunctionalAccount from a dict
functional_account_form_dict = functional_account.from_dict(functional_account_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


