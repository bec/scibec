# NXEntityFilter


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**offset** | **int** |  | [optional] 
**limit** | **int** |  | [optional] 
**skip** | **int** |  | [optional] 
**order** | [**AccessAccountFilterOrder**](AccessAccountFilterOrder.md) |  | [optional] 
**fields** | [**NXEntityFields**](NXEntityFields.md) |  | [optional] 
**include** | [**List[NXEntityIncludeFilterInner]**](NXEntityIncludeFilterInner.md) |  | [optional] 

## Example

```python
from py_scibec_openapi_client.models.nx_entity_filter import NXEntityFilter

# TODO update the JSON string below
json = "{}"
# create an instance of NXEntityFilter from a JSON string
nx_entity_filter_instance = NXEntityFilter.from_json(json)
# print the JSON string representation of the object
print(NXEntityFilter.to_json())

# convert the object into a dict
nx_entity_filter_dict = nx_entity_filter_instance.to_dict()
# create an instance of NXEntityFilter from a dict
nx_entity_filter_form_dict = nx_entity_filter.from_dict(nx_entity_filter_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


