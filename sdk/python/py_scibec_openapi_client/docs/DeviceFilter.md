# DeviceFilter


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**offset** | **int** |  | [optional] 
**limit** | **int** |  | [optional] 
**skip** | **int** |  | [optional] 
**order** | [**AccessAccountFilterOrder**](AccessAccountFilterOrder.md) |  | [optional] 
**fields** | [**DeviceFields**](DeviceFields.md) |  | [optional] 
**include** | [**List[DeviceIncludeFilterInner]**](DeviceIncludeFilterInner.md) |  | [optional] 

## Example

```python
from py_scibec_openapi_client.models.device_filter import DeviceFilter

# TODO update the JSON string below
json = "{}"
# create an instance of DeviceFilter from a JSON string
device_filter_instance = DeviceFilter.from_json(json)
# print the JSON string representation of the object
print(DeviceFilter.to_json())

# convert the object into a dict
device_filter_dict = device_filter_instance.to_dict()
# create an instance of DeviceFilter from a dict
device_filter_form_dict = device_filter.from_dict(device_filter_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


