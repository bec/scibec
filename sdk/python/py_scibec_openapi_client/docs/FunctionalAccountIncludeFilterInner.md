# FunctionalAccountIncludeFilterInner


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**relation** | **str** |  | [optional] 
**scope** | [**FunctionalAccountScopeFilter**](FunctionalAccountScopeFilter.md) |  | [optional] 

## Example

```python
from py_scibec_openapi_client.models.functional_account_include_filter_inner import FunctionalAccountIncludeFilterInner

# TODO update the JSON string below
json = "{}"
# create an instance of FunctionalAccountIncludeFilterInner from a JSON string
functional_account_include_filter_inner_instance = FunctionalAccountIncludeFilterInner.from_json(json)
# print the JSON string representation of the object
print(FunctionalAccountIncludeFilterInner.to_json())

# convert the object into a dict
functional_account_include_filter_inner_dict = functional_account_include_filter_inner_instance.to_dict()
# create an instance of FunctionalAccountIncludeFilterInner from a dict
functional_account_include_filter_inner_form_dict = functional_account_include_filter_inner.from_dict(functional_account_include_filter_inner_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


