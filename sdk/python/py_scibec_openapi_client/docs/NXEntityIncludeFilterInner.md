# NXEntityIncludeFilterInner


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**relation** | **str** |  | [optional] 
**scope** | [**NXEntityScopeFilter**](NXEntityScopeFilter.md) |  | [optional] 

## Example

```python
from py_scibec_openapi_client.models.nx_entity_include_filter_inner import NXEntityIncludeFilterInner

# TODO update the JSON string below
json = "{}"
# create an instance of NXEntityIncludeFilterInner from a JSON string
nx_entity_include_filter_inner_instance = NXEntityIncludeFilterInner.from_json(json)
# print the JSON string representation of the object
print(NXEntityIncludeFilterInner.to_json())

# convert the object into a dict
nx_entity_include_filter_inner_dict = nx_entity_include_filter_inner_instance.to_dict()
# create an instance of NXEntityIncludeFilterInner from a dict
nx_entity_include_filter_inner_form_dict = nx_entity_include_filter_inner.from_dict(nx_entity_include_filter_inner_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


