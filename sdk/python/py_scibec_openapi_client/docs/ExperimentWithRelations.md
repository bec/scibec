# ExperimentWithRelations

(tsType: ExperimentWithRelations, schemaOptions: { includeRelations: true })

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** |  | [optional] 
**read_acl** | **List[str]** |  | [optional] 
**write_acl** | **List[str]** |  | [optional] 
**owner** | **List[str]** |  | [optional] 
**created_at** | **datetime** |  | [optional] 
**created_by** | **str** |  | [optional] 
**name** | **str** |  | 
**beamline_id** | **str** |  | 
**write_account** | **str** |  | [optional] 
**user_id** | **str** |  | [optional] 
**logbook** | **str** |  | [optional] 
**samples** | **List[str]** |  | [optional] 
**experiment_config** | **object** |  | [optional] 
**experiment_info** | **object** |  | [optional] 
**active_session** | **str** |  | [optional] 
**datasets** | [**List[SessionWithRelations]**](SessionWithRelations.md) |  | [optional] 
**scans** | [**List[ScanWithRelations]**](ScanWithRelations.md) |  | [optional] 
**experiment_accounts** | [**List[ExperimentAccountWithRelations]**](ExperimentAccountWithRelations.md) |  | [optional] 
**sessions** | [**List[SessionWithRelations]**](SessionWithRelations.md) |  | [optional] 

## Example

```python
from py_scibec_openapi_client.models.experiment_with_relations import ExperimentWithRelations

# TODO update the JSON string below
json = "{}"
# create an instance of ExperimentWithRelations from a JSON string
experiment_with_relations_instance = ExperimentWithRelations.from_json(json)
# print the JSON string representation of the object
print(ExperimentWithRelations.to_json())

# convert the object into a dict
experiment_with_relations_dict = experiment_with_relations_instance.to_dict()
# create an instance of ExperimentWithRelations from a dict
experiment_with_relations_form_dict = experiment_with_relations.from_dict(experiment_with_relations_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


