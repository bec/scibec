# ScanDataIncludeFilterInner


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**relation** | **str** |  | [optional] 
**scope** | [**ScanDataScopeFilter**](ScanDataScopeFilter.md) |  | [optional] 

## Example

```python
from py_scibec_openapi_client.models.scan_data_include_filter_inner import ScanDataIncludeFilterInner

# TODO update the JSON string below
json = "{}"
# create an instance of ScanDataIncludeFilterInner from a JSON string
scan_data_include_filter_inner_instance = ScanDataIncludeFilterInner.from_json(json)
# print the JSON string representation of the object
print(ScanDataIncludeFilterInner.to_json())

# convert the object into a dict
scan_data_include_filter_inner_dict = scan_data_include_filter_inner_instance.to_dict()
# create an instance of ScanDataIncludeFilterInner from a dict
scan_data_include_filter_inner_form_dict = scan_data_include_filter_inner.from_dict(scan_data_include_filter_inner_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


