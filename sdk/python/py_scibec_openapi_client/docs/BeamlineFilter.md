# BeamlineFilter


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**offset** | **int** |  | [optional] 
**limit** | **int** |  | [optional] 
**skip** | **int** |  | [optional] 
**order** | [**AccessAccountFilterOrder**](AccessAccountFilterOrder.md) |  | [optional] 
**fields** | [**BeamlineFields**](BeamlineFields.md) |  | [optional] 
**include** | [**List[BeamlineIncludeFilterInner]**](BeamlineIncludeFilterInner.md) |  | [optional] 

## Example

```python
from py_scibec_openapi_client.models.beamline_filter import BeamlineFilter

# TODO update the JSON string below
json = "{}"
# create an instance of BeamlineFilter from a JSON string
beamline_filter_instance = BeamlineFilter.from_json(json)
# print the JSON string representation of the object
print(BeamlineFilter.to_json())

# convert the object into a dict
beamline_filter_dict = beamline_filter_instance.to_dict()
# create an instance of BeamlineFilter from a dict
beamline_filter_form_dict = beamline_filter.from_dict(beamline_filter_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


