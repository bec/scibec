# py_scibec_openapi_client.ScanDataControllerApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**scan_data_controller_count**](ScanDataControllerApi.md#scan_data_controller_count) | **GET** /scan-data/count | 
[**scan_data_controller_create**](ScanDataControllerApi.md#scan_data_controller_create) | **POST** /scan-data | 
[**scan_data_controller_create_many**](ScanDataControllerApi.md#scan_data_controller_create_many) | **POST** /scan-data/many | 
[**scan_data_controller_delete_by_id**](ScanDataControllerApi.md#scan_data_controller_delete_by_id) | **DELETE** /scan-data/{id} | 
[**scan_data_controller_find**](ScanDataControllerApi.md#scan_data_controller_find) | **GET** /scan-data | 
[**scan_data_controller_find_by_id**](ScanDataControllerApi.md#scan_data_controller_find_by_id) | **GET** /scan-data/{id} | 
[**scan_data_controller_replace_by_id**](ScanDataControllerApi.md#scan_data_controller_replace_by_id) | **PUT** /scan-data/{id} | 
[**scan_data_controller_update_all**](ScanDataControllerApi.md#scan_data_controller_update_all) | **PATCH** /scan-data | 
[**scan_data_controller_update_by_id**](ScanDataControllerApi.md#scan_data_controller_update_by_id) | **PATCH** /scan-data/{id} | 


# **scan_data_controller_count**
> LoopbackCount scan_data_controller_count(where=where)



### Example

* Bearer (JWT) Authentication (jwt):

```python
import py_scibec_openapi_client
from py_scibec_openapi_client.models.loopback_count import LoopbackCount
from py_scibec_openapi_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = py_scibec_openapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization (JWT): jwt
configuration = py_scibec_openapi_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
with py_scibec_openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = py_scibec_openapi_client.ScanDataControllerApi(api_client)
    where = None # Dict[str, object] |  (optional)

    try:
        api_response = api_instance.scan_data_controller_count(where=where)
        print("The response of ScanDataControllerApi->scan_data_controller_count:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling ScanDataControllerApi->scan_data_controller_count: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | [**Dict[str, object]**](object.md)|  | [optional] 

### Return type

[**LoopbackCount**](LoopbackCount.md)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | ScanData model count |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **scan_data_controller_create**
> ScanData scan_data_controller_create(new_scan_data=new_scan_data)



### Example

* Bearer (JWT) Authentication (jwt):

```python
import py_scibec_openapi_client
from py_scibec_openapi_client.models.new_scan_data import NewScanData
from py_scibec_openapi_client.models.scan_data import ScanData
from py_scibec_openapi_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = py_scibec_openapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization (JWT): jwt
configuration = py_scibec_openapi_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
with py_scibec_openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = py_scibec_openapi_client.ScanDataControllerApi(api_client)
    new_scan_data = py_scibec_openapi_client.NewScanData() # NewScanData |  (optional)

    try:
        api_response = api_instance.scan_data_controller_create(new_scan_data=new_scan_data)
        print("The response of ScanDataControllerApi->scan_data_controller_create:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling ScanDataControllerApi->scan_data_controller_create: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **new_scan_data** | [**NewScanData**](NewScanData.md)|  | [optional] 

### Return type

[**ScanData**](ScanData.md)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | ScanData model instance |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **scan_data_controller_create_many**
> NewScanDataMany scan_data_controller_create_many(new_scan_data=new_scan_data)



### Example

* Bearer (JWT) Authentication (jwt):

```python
import py_scibec_openapi_client
from py_scibec_openapi_client.models.new_scan_data import NewScanData
from py_scibec_openapi_client.models.new_scan_data_many import NewScanDataMany
from py_scibec_openapi_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = py_scibec_openapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization (JWT): jwt
configuration = py_scibec_openapi_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
with py_scibec_openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = py_scibec_openapi_client.ScanDataControllerApi(api_client)
    new_scan_data = py_scibec_openapi_client.NewScanData() # NewScanData |  (optional)

    try:
        api_response = api_instance.scan_data_controller_create_many(new_scan_data=new_scan_data)
        print("The response of ScanDataControllerApi->scan_data_controller_create_many:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling ScanDataControllerApi->scan_data_controller_create_many: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **new_scan_data** | [**NewScanData**](NewScanData.md)|  | [optional] 

### Return type

[**NewScanDataMany**](NewScanDataMany.md)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | ScanData model instance |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **scan_data_controller_delete_by_id**
> scan_data_controller_delete_by_id(id)



### Example

* Bearer (JWT) Authentication (jwt):

```python
import py_scibec_openapi_client
from py_scibec_openapi_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = py_scibec_openapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization (JWT): jwt
configuration = py_scibec_openapi_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
with py_scibec_openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = py_scibec_openapi_client.ScanDataControllerApi(api_client)
    id = 'id_example' # str | 

    try:
        api_instance.scan_data_controller_delete_by_id(id)
    except Exception as e:
        print("Exception when calling ScanDataControllerApi->scan_data_controller_delete_by_id: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**|  | 

### Return type

void (empty response body)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | ScanData DELETE success |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **scan_data_controller_find**
> List[ScanDataWithRelations] scan_data_controller_find(filter=filter)



### Example

* Bearer (JWT) Authentication (jwt):

```python
import py_scibec_openapi_client
from py_scibec_openapi_client.models.scan_data_filter1 import ScanDataFilter1
from py_scibec_openapi_client.models.scan_data_with_relations import ScanDataWithRelations
from py_scibec_openapi_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = py_scibec_openapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization (JWT): jwt
configuration = py_scibec_openapi_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
with py_scibec_openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = py_scibec_openapi_client.ScanDataControllerApi(api_client)
    filter = py_scibec_openapi_client.ScanDataFilter1() # ScanDataFilter1 |  (optional)

    try:
        api_response = api_instance.scan_data_controller_find(filter=filter)
        print("The response of ScanDataControllerApi->scan_data_controller_find:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling ScanDataControllerApi->scan_data_controller_find: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | [**ScanDataFilter1**](.md)|  | [optional] 

### Return type

[**List[ScanDataWithRelations]**](ScanDataWithRelations.md)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Array of ScanData model instances |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **scan_data_controller_find_by_id**
> ScanDataWithRelations scan_data_controller_find_by_id(id, filter=filter)



### Example

* Bearer (JWT) Authentication (jwt):

```python
import py_scibec_openapi_client
from py_scibec_openapi_client.models.scan_data_filter import ScanDataFilter
from py_scibec_openapi_client.models.scan_data_with_relations import ScanDataWithRelations
from py_scibec_openapi_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = py_scibec_openapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization (JWT): jwt
configuration = py_scibec_openapi_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
with py_scibec_openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = py_scibec_openapi_client.ScanDataControllerApi(api_client)
    id = 'id_example' # str | 
    filter = py_scibec_openapi_client.ScanDataFilter() # ScanDataFilter |  (optional)

    try:
        api_response = api_instance.scan_data_controller_find_by_id(id, filter=filter)
        print("The response of ScanDataControllerApi->scan_data_controller_find_by_id:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling ScanDataControllerApi->scan_data_controller_find_by_id: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**|  | 
 **filter** | [**ScanDataFilter**](.md)|  | [optional] 

### Return type

[**ScanDataWithRelations**](ScanDataWithRelations.md)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | ScanData model instance |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **scan_data_controller_replace_by_id**
> scan_data_controller_replace_by_id(id, scan_data=scan_data)



### Example

* Bearer (JWT) Authentication (jwt):

```python
import py_scibec_openapi_client
from py_scibec_openapi_client.models.scan_data import ScanData
from py_scibec_openapi_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = py_scibec_openapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization (JWT): jwt
configuration = py_scibec_openapi_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
with py_scibec_openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = py_scibec_openapi_client.ScanDataControllerApi(api_client)
    id = 'id_example' # str | 
    scan_data = py_scibec_openapi_client.ScanData() # ScanData |  (optional)

    try:
        api_instance.scan_data_controller_replace_by_id(id, scan_data=scan_data)
    except Exception as e:
        print("Exception when calling ScanDataControllerApi->scan_data_controller_replace_by_id: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**|  | 
 **scan_data** | [**ScanData**](ScanData.md)|  | [optional] 

### Return type

void (empty response body)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | ScanData PUT success |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **scan_data_controller_update_all**
> LoopbackCount scan_data_controller_update_all(where=where, scan_data_partial=scan_data_partial)



### Example

* Bearer (JWT) Authentication (jwt):

```python
import py_scibec_openapi_client
from py_scibec_openapi_client.models.loopback_count import LoopbackCount
from py_scibec_openapi_client.models.scan_data_partial import ScanDataPartial
from py_scibec_openapi_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = py_scibec_openapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization (JWT): jwt
configuration = py_scibec_openapi_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
with py_scibec_openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = py_scibec_openapi_client.ScanDataControllerApi(api_client)
    where = None # Dict[str, object] |  (optional)
    scan_data_partial = py_scibec_openapi_client.ScanDataPartial() # ScanDataPartial |  (optional)

    try:
        api_response = api_instance.scan_data_controller_update_all(where=where, scan_data_partial=scan_data_partial)
        print("The response of ScanDataControllerApi->scan_data_controller_update_all:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling ScanDataControllerApi->scan_data_controller_update_all: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | [**Dict[str, object]**](object.md)|  | [optional] 
 **scan_data_partial** | [**ScanDataPartial**](ScanDataPartial.md)|  | [optional] 

### Return type

[**LoopbackCount**](LoopbackCount.md)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | ScanData PATCH success count |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **scan_data_controller_update_by_id**
> scan_data_controller_update_by_id(id, scan_data_partial=scan_data_partial)



### Example

* Bearer (JWT) Authentication (jwt):

```python
import py_scibec_openapi_client
from py_scibec_openapi_client.models.scan_data_partial import ScanDataPartial
from py_scibec_openapi_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = py_scibec_openapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization (JWT): jwt
configuration = py_scibec_openapi_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
with py_scibec_openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = py_scibec_openapi_client.ScanDataControllerApi(api_client)
    id = 'id_example' # str | 
    scan_data_partial = py_scibec_openapi_client.ScanDataPartial() # ScanDataPartial |  (optional)

    try:
        api_instance.scan_data_controller_update_by_id(id, scan_data_partial=scan_data_partial)
    except Exception as e:
        print("Exception when calling ScanDataControllerApi->scan_data_controller_update_by_id: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**|  | 
 **scan_data_partial** | [**ScanDataPartial**](ScanDataPartial.md)|  | [optional] 

### Return type

void (empty response body)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | ScanData PATCH success |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

