# ExperimentIncludeFilterItems


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**relation** | **str** |  | [optional] 
**scope** | [**ExperimentScopeFilter**](ExperimentScopeFilter.md) |  | [optional] 

## Example

```python
from py_scibec_openapi_client.models.experiment_include_filter_items import ExperimentIncludeFilterItems

# TODO update the JSON string below
json = "{}"
# create an instance of ExperimentIncludeFilterItems from a JSON string
experiment_include_filter_items_instance = ExperimentIncludeFilterItems.from_json(json)
# print the JSON string representation of the object
print(ExperimentIncludeFilterItems.to_json())

# convert the object into a dict
experiment_include_filter_items_dict = experiment_include_filter_items_instance.to_dict()
# create an instance of ExperimentIncludeFilterItems from a dict
experiment_include_filter_items_form_dict = experiment_include_filter_items.from_dict(experiment_include_filter_items_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


