# ScanFilter


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**offset** | **int** |  | [optional] 
**limit** | **int** |  | [optional] 
**skip** | **int** |  | [optional] 
**order** | [**AccessAccountFilterOrder**](AccessAccountFilterOrder.md) |  | [optional] 
**fields** | [**ScanFields**](ScanFields.md) |  | [optional] 
**include** | [**List[ScanIncludeFilterInner]**](ScanIncludeFilterInner.md) |  | [optional] 

## Example

```python
from py_scibec_openapi_client.models.scan_filter import ScanFilter

# TODO update the JSON string below
json = "{}"
# create an instance of ScanFilter from a JSON string
scan_filter_instance = ScanFilter.from_json(json)
# print the JSON string representation of the object
print(ScanFilter.to_json())

# convert the object into a dict
scan_filter_dict = scan_filter_instance.to_dict()
# create an instance of ScanFilter from a dict
scan_filter_form_dict = scan_filter.from_dict(scan_filter_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


