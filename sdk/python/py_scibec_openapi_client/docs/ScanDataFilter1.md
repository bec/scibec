# ScanDataFilter1


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**offset** | **int** |  | [optional] 
**limit** | **int** |  | [optional] 
**skip** | **int** |  | [optional] 
**order** | [**AccessAccountFilterOrder**](AccessAccountFilterOrder.md) |  | [optional] 
**where** | **Dict[str, object]** |  | [optional] 
**fields** | [**ScanDataFields**](ScanDataFields.md) |  | [optional] 
**include** | [**List[ScanDataIncludeFilterInner]**](ScanDataIncludeFilterInner.md) |  | [optional] 

## Example

```python
from py_scibec_openapi_client.models.scan_data_filter1 import ScanDataFilter1

# TODO update the JSON string below
json = "{}"
# create an instance of ScanDataFilter1 from a JSON string
scan_data_filter1_instance = ScanDataFilter1.from_json(json)
# print the JSON string representation of the object
print(ScanDataFilter1.to_json())

# convert the object into a dict
scan_data_filter1_dict = scan_data_filter1_instance.to_dict()
# create an instance of ScanDataFilter1 from a dict
scan_data_filter1_form_dict = scan_data_filter1.from_dict(scan_data_filter1_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


