# NewScanDataMany


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**count** | **float** |  | [optional] 

## Example

```python
from py_scibec_openapi_client.models.new_scan_data_many import NewScanDataMany

# TODO update the JSON string below
json = "{}"
# create an instance of NewScanDataMany from a JSON string
new_scan_data_many_instance = NewScanDataMany.from_json(json)
# print the JSON string representation of the object
print(NewScanDataMany.to_json())

# convert the object into a dict
new_scan_data_many_dict = new_scan_data_many_instance.to_dict()
# create an instance of NewScanDataMany from a dict
new_scan_data_many_form_dict = new_scan_data_many.from_dict(new_scan_data_many_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


