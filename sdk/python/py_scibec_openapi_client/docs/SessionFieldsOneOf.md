# SessionFieldsOneOf


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **bool** |  | [optional] 
**read_acl** | **bool** |  | [optional] 
**write_acl** | **bool** |  | [optional] 
**owner** | **bool** |  | [optional] 
**created_at** | **bool** |  | [optional] 
**created_by** | **bool** |  | [optional] 
**name** | **bool** |  | [optional] 
**experiment_id** | **bool** |  | [optional] 
**session_config** | **bool** |  | [optional] 

## Example

```python
from py_scibec_openapi_client.models.session_fields_one_of import SessionFieldsOneOf

# TODO update the JSON string below
json = "{}"
# create an instance of SessionFieldsOneOf from a JSON string
session_fields_one_of_instance = SessionFieldsOneOf.from_json(json)
# print the JSON string representation of the object
print(SessionFieldsOneOf.to_json())

# convert the object into a dict
session_fields_one_of_dict = session_fields_one_of_instance.to_dict()
# create an instance of SessionFieldsOneOf from a dict
session_fields_one_of_form_dict = session_fields_one_of.from_dict(session_fields_one_of_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


