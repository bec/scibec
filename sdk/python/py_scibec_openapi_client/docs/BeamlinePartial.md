# BeamlinePartial

(tsType: Partial<Beamline>, schemaOptions: { partial: true })

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** |  | [optional] 
**read_acl** | **List[str]** |  | [optional] 
**write_acl** | **List[str]** |  | [optional] 
**owner** | **List[str]** |  | [optional] 
**created_at** | **datetime** |  | [optional] 
**created_by** | **str** |  | [optional] 
**name** | **str** |  | [optional] 
**active_experiment** | **str** |  | [optional] 

## Example

```python
from py_scibec_openapi_client.models.beamline_partial import BeamlinePartial

# TODO update the JSON string below
json = "{}"
# create an instance of BeamlinePartial from a JSON string
beamline_partial_instance = BeamlinePartial.from_json(json)
# print the JSON string representation of the object
print(BeamlinePartial.to_json())

# convert the object into a dict
beamline_partial_dict = beamline_partial_instance.to_dict()
# create an instance of BeamlinePartial from a dict
beamline_partial_form_dict = beamline_partial.from_dict(beamline_partial_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


