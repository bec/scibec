# FunctionalAccountIncludeFilterItems


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**relation** | **str** |  | [optional] 
**scope** | [**FunctionalAccountScopeFilter**](FunctionalAccountScopeFilter.md) |  | [optional] 

## Example

```python
from py_scibec_openapi_client.models.functional_account_include_filter_items import FunctionalAccountIncludeFilterItems

# TODO update the JSON string below
json = "{}"
# create an instance of FunctionalAccountIncludeFilterItems from a JSON string
functional_account_include_filter_items_instance = FunctionalAccountIncludeFilterItems.from_json(json)
# print the JSON string representation of the object
print(FunctionalAccountIncludeFilterItems.to_json())

# convert the object into a dict
functional_account_include_filter_items_dict = functional_account_include_filter_items_instance.to_dict()
# create an instance of FunctionalAccountIncludeFilterItems from a dict
functional_account_include_filter_items_form_dict = functional_account_include_filter_items.from_dict(functional_account_include_filter_items_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


