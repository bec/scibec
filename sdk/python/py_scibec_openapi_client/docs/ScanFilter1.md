# ScanFilter1


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**offset** | **int** |  | [optional] 
**limit** | **int** |  | [optional] 
**skip** | **int** |  | [optional] 
**order** | [**AccessAccountFilterOrder**](AccessAccountFilterOrder.md) |  | [optional] 
**where** | **Dict[str, object]** |  | [optional] 
**fields** | [**ScanFields**](ScanFields.md) |  | [optional] 
**include** | [**List[ScanIncludeFilterInner]**](ScanIncludeFilterInner.md) |  | [optional] 

## Example

```python
from py_scibec_openapi_client.models.scan_filter1 import ScanFilter1

# TODO update the JSON string below
json = "{}"
# create an instance of ScanFilter1 from a JSON string
scan_filter1_instance = ScanFilter1.from_json(json)
# print the JSON string representation of the object
print(ScanFilter1.to_json())

# convert the object into a dict
scan_filter1_dict = scan_filter1_instance.to_dict()
# create an instance of ScanFilter1 from a dict
scan_filter1_form_dict = scan_filter1.from_dict(scan_filter1_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


