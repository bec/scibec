# SessionFilter1


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**offset** | **int** |  | [optional] 
**limit** | **int** |  | [optional] 
**skip** | **int** |  | [optional] 
**order** | [**AccessAccountFilterOrder**](AccessAccountFilterOrder.md) |  | [optional] 
**where** | **Dict[str, object]** |  | [optional] 
**fields** | [**SessionFields**](SessionFields.md) |  | [optional] 
**include** | [**List[SessionIncludeFilterInner]**](SessionIncludeFilterInner.md) |  | [optional] 

## Example

```python
from py_scibec_openapi_client.models.session_filter1 import SessionFilter1

# TODO update the JSON string below
json = "{}"
# create an instance of SessionFilter1 from a JSON string
session_filter1_instance = SessionFilter1.from_json(json)
# print the JSON string representation of the object
print(SessionFilter1.to_json())

# convert the object into a dict
session_filter1_dict = session_filter1_instance.to_dict()
# create an instance of SessionFilter1 from a dict
session_filter1_form_dict = session_filter1.from_dict(session_filter1_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


