# NewNXEntity

(tsType: Omit<NXEntity, 'id'>, schemaOptions: { title: 'NewNXEntity', exclude: [ 'id' ] })

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**read_acl** | **List[str]** |  | [optional] 
**write_acl** | **List[str]** |  | [optional] 
**owner** | **List[str]** |  | [optional] 
**created_at** | **datetime** |  | [optional] 
**created_by** | **str** |  | [optional] 
**name** | **str** |  | [optional] 
**type** | **str** |  | [optional] 
**description** | **str** |  | [optional] 
**data** | **object** |  | [optional] 
**n_xclass** | **str** |  | [optional] 
**parent_id** | **str** |  | [optional] 
**scan_id** | **str** | The parent scan | [optional] 

## Example

```python
from py_scibec_openapi_client.models.new_nx_entity import NewNXEntity

# TODO update the JSON string below
json = "{}"
# create an instance of NewNXEntity from a JSON string
new_nx_entity_instance = NewNXEntity.from_json(json)
# print the JSON string representation of the object
print(NewNXEntity.to_json())

# convert the object into a dict
new_nx_entity_dict = new_nx_entity_instance.to_dict()
# create an instance of NewNXEntity from a dict
new_nx_entity_form_dict = new_nx_entity.from_dict(new_nx_entity_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


