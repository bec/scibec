# NXEntityWithRelations

(tsType: NXEntityWithRelations, schemaOptions: { includeRelations: true })

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** |  | [optional] 
**read_acl** | **List[str]** |  | [optional] 
**write_acl** | **List[str]** |  | [optional] 
**owner** | **List[str]** |  | [optional] 
**created_at** | **datetime** |  | [optional] 
**created_by** | **str** |  | [optional] 
**name** | **str** |  | [optional] 
**type** | **str** |  | [optional] 
**description** | **str** |  | [optional] 
**data** | **object** |  | [optional] 
**n_xclass** | **str** |  | [optional] 
**parent_id** | **str** |  | [optional] 
**scan_id** | **str** | The parent scan | [optional] 
**subentries** | [**List[NXEntityWithRelations]**](NXEntityWithRelations.md) |  | [optional] 
**parent** | [**NXEntityWithRelations**](NXEntityWithRelations.md) |  | [optional] 
**scan** | [**ScanWithRelations**](ScanWithRelations.md) |  | [optional] 

## Example

```python
from py_scibec_openapi_client.models.nx_entity_with_relations import NXEntityWithRelations

# TODO update the JSON string below
json = "{}"
# create an instance of NXEntityWithRelations from a JSON string
nx_entity_with_relations_instance = NXEntityWithRelations.from_json(json)
# print the JSON string representation of the object
print(NXEntityWithRelations.to_json())

# convert the object into a dict
nx_entity_with_relations_dict = nx_entity_with_relations_instance.to_dict()
# create an instance of NXEntityWithRelations from a dict
nx_entity_with_relations_form_dict = nx_entity_with_relations.from_dict(nx_entity_with_relations_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


