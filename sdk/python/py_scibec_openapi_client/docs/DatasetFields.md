# DatasetFields


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **bool** |  | [optional] 
**read_acl** | **bool** |  | [optional] 
**write_acl** | **bool** |  | [optional] 
**owner** | **bool** |  | [optional] 
**created_at** | **bool** |  | [optional] 
**created_by** | **bool** |  | [optional] 
**name** | **bool** |  | [optional] 
**number** | **bool** |  | [optional] 
**experiment_id** | **bool** |  | [optional] 

## Example

```python
from py_scibec_openapi_client.models.dataset_fields import DatasetFields

# TODO update the JSON string below
json = "{}"
# create an instance of DatasetFields from a JSON string
dataset_fields_instance = DatasetFields.from_json(json)
# print the JSON string representation of the object
print(DatasetFields.to_json())

# convert the object into a dict
dataset_fields_dict = dataset_fields_instance.to_dict()
# create an instance of DatasetFields from a dict
dataset_fields_form_dict = dataset_fields.from_dict(dataset_fields_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


