# DatasetWithRelations

(tsType: DatasetWithRelations, schemaOptions: { includeRelations: true })

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** |  | [optional] 
**read_acl** | **List[str]** |  | [optional] 
**write_acl** | **List[str]** |  | [optional] 
**owner** | **List[str]** |  | [optional] 
**created_at** | **datetime** |  | [optional] 
**created_by** | **str** |  | [optional] 
**name** | **str** |  | [optional] 
**number** | **float** |  | [optional] 
**experiment_id** | **str** |  | [optional] 
**scans** | [**List[ScanWithRelations]**](ScanWithRelations.md) |  | [optional] 
**experiment** | [**ExperimentWithRelations**](ExperimentWithRelations.md) |  | [optional] 

## Example

```python
from py_scibec_openapi_client.models.dataset_with_relations import DatasetWithRelations

# TODO update the JSON string below
json = "{}"
# create an instance of DatasetWithRelations from a JSON string
dataset_with_relations_instance = DatasetWithRelations.from_json(json)
# print the JSON string representation of the object
print(DatasetWithRelations.to_json())

# convert the object into a dict
dataset_with_relations_dict = dataset_with_relations_instance.to_dict()
# create an instance of DatasetWithRelations from a dict
dataset_with_relations_form_dict = dataset_with_relations.from_dict(dataset_with_relations_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


