# ExperimentFilter


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**offset** | **int** |  | [optional] 
**limit** | **int** |  | [optional] 
**skip** | **int** |  | [optional] 
**order** | [**AccessAccountFilterOrder**](AccessAccountFilterOrder.md) |  | [optional] 
**fields** | [**ExperimentFields**](ExperimentFields.md) |  | [optional] 
**include** | [**List[ExperimentIncludeFilterInner]**](ExperimentIncludeFilterInner.md) |  | [optional] 

## Example

```python
from py_scibec_openapi_client.models.experiment_filter import ExperimentFilter

# TODO update the JSON string below
json = "{}"
# create an instance of ExperimentFilter from a JSON string
experiment_filter_instance = ExperimentFilter.from_json(json)
# print the JSON string representation of the object
print(ExperimentFilter.to_json())

# convert the object into a dict
experiment_filter_dict = experiment_filter_instance.to_dict()
# create an instance of ExperimentFilter from a dict
experiment_filter_form_dict = experiment_filter.from_dict(experiment_filter_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


