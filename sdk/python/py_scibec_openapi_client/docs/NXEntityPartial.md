# NXEntityPartial

(tsType: Partial<NXEntity>, schemaOptions: { partial: true })

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** |  | [optional] 
**read_acl** | **List[str]** |  | [optional] 
**write_acl** | **List[str]** |  | [optional] 
**owner** | **List[str]** |  | [optional] 
**created_at** | **datetime** |  | [optional] 
**created_by** | **str** |  | [optional] 
**name** | **str** |  | [optional] 
**type** | **str** |  | [optional] 
**description** | **str** |  | [optional] 
**data** | **object** |  | [optional] 
**n_xclass** | **str** |  | [optional] 
**parent_id** | **str** |  | [optional] 
**scan_id** | **str** | The parent scan | [optional] 

## Example

```python
from py_scibec_openapi_client.models.nx_entity_partial import NXEntityPartial

# TODO update the JSON string below
json = "{}"
# create an instance of NXEntityPartial from a JSON string
nx_entity_partial_instance = NXEntityPartial.from_json(json)
# print the JSON string representation of the object
print(NXEntityPartial.to_json())

# convert the object into a dict
nx_entity_partial_dict = nx_entity_partial_instance.to_dict()
# create an instance of NXEntityPartial from a dict
nx_entity_partial_form_dict = nx_entity_partial.from_dict(nx_entity_partial_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


