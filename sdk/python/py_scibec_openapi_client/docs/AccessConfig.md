# AccessConfig


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** |  | [optional] 
**read_acl** | **List[str]** |  | [optional] 
**write_acl** | **List[str]** |  | [optional] 
**owner** | **List[str]** |  | [optional] 
**created_at** | **datetime** |  | [optional] 
**created_by** | **str** |  | [optional] 
**target_account** | **str** |  | [optional] 
**beamline_id** | **str** | Beamline to which this config belongs. | 
**auth_enabled** | **bool** |  | 
**active_accounts** | **List[str]** |  | [optional] 
**use_passwords** | **bool** |  | 

## Example

```python
from py_scibec_openapi_client.models.access_config import AccessConfig

# TODO update the JSON string below
json = "{}"
# create an instance of AccessConfig from a JSON string
access_config_instance = AccessConfig.from_json(json)
# print the JSON string representation of the object
print(AccessConfig.to_json())

# convert the object into a dict
access_config_dict = access_config_instance.to_dict()
# create an instance of AccessConfig from a dict
access_config_form_dict = access_config.from_dict(access_config_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


