# ExperimentAccountFieldsOneOf


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **bool** |  | [optional] 
**read_acl** | **bool** |  | [optional] 
**write_acl** | **bool** |  | [optional] 
**owner** | **bool** |  | [optional] 
**created_at** | **bool** |  | [optional] 
**created_by** | **bool** |  | [optional] 
**name** | **bool** |  | [optional] 
**read** | **bool** |  | [optional] 
**write** | **bool** |  | [optional] 
**remote** | **bool** |  | [optional] 
**token** | **bool** |  | [optional] 
**is_functional** | **bool** |  | [optional] 
**experiment_id** | **bool** |  | [optional] 

## Example

```python
from py_scibec_openapi_client.models.experiment_account_fields_one_of import ExperimentAccountFieldsOneOf

# TODO update the JSON string below
json = "{}"
# create an instance of ExperimentAccountFieldsOneOf from a JSON string
experiment_account_fields_one_of_instance = ExperimentAccountFieldsOneOf.from_json(json)
# print the JSON string representation of the object
print(ExperimentAccountFieldsOneOf.to_json())

# convert the object into a dict
experiment_account_fields_one_of_dict = experiment_account_fields_one_of_instance.to_dict()
# create an instance of ExperimentAccountFieldsOneOf from a dict
experiment_account_fields_one_of_form_dict = experiment_account_fields_one_of.from_dict(experiment_account_fields_one_of_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


