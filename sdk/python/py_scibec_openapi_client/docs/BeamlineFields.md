# BeamlineFields


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **bool** |  | [optional] 
**read_acl** | **bool** |  | [optional] 
**write_acl** | **bool** |  | [optional] 
**owner** | **bool** |  | [optional] 
**created_at** | **bool** |  | [optional] 
**created_by** | **bool** |  | [optional] 
**name** | **bool** |  | [optional] 
**active_experiment** | **bool** |  | [optional] 

## Example

```python
from py_scibec_openapi_client.models.beamline_fields import BeamlineFields

# TODO update the JSON string below
json = "{}"
# create an instance of BeamlineFields from a JSON string
beamline_fields_instance = BeamlineFields.from_json(json)
# print the JSON string representation of the object
print(BeamlineFields.to_json())

# convert the object into a dict
beamline_fields_dict = beamline_fields_instance.to_dict()
# create an instance of BeamlineFields from a dict
beamline_fields_form_dict = beamline_fields.from_dict(beamline_fields_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


