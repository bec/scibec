# AccessConfigWithRelations

(tsType: AccessConfigWithRelations, schemaOptions: { includeRelations: true })

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** |  | [optional] 
**read_acl** | **List[str]** |  | [optional] 
**write_acl** | **List[str]** |  | [optional] 
**owner** | **List[str]** |  | [optional] 
**created_at** | **datetime** |  | [optional] 
**created_by** | **str** |  | [optional] 
**target_account** | **str** |  | [optional] 
**beamline_id** | **str** | Beamline to which this config belongs. | 
**auth_enabled** | **bool** |  | 
**active_accounts** | **List[str]** |  | [optional] 
**use_passwords** | **bool** |  | 
**beamline** | [**BeamlineWithRelations**](BeamlineWithRelations.md) |  | [optional] 
**functional_accounts** | [**List[FunctionalAccountWithRelations]**](FunctionalAccountWithRelations.md) |  | [optional] 

## Example

```python
from py_scibec_openapi_client.models.access_config_with_relations import AccessConfigWithRelations

# TODO update the JSON string below
json = "{}"
# create an instance of AccessConfigWithRelations from a JSON string
access_config_with_relations_instance = AccessConfigWithRelations.from_json(json)
# print the JSON string representation of the object
print(AccessConfigWithRelations.to_json())

# convert the object into a dict
access_config_with_relations_dict = access_config_with_relations_instance.to_dict()
# create an instance of AccessConfigWithRelations from a dict
access_config_with_relations_form_dict = access_config_with_relations.from_dict(access_config_with_relations_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


