# DeviceIncludeFilterItems


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**relation** | **str** |  | [optional] 
**scope** | [**DeviceScopeFilter**](DeviceScopeFilter.md) |  | [optional] 

## Example

```python
from py_scibec_openapi_client.models.device_include_filter_items import DeviceIncludeFilterItems

# TODO update the JSON string below
json = "{}"
# create an instance of DeviceIncludeFilterItems from a JSON string
device_include_filter_items_instance = DeviceIncludeFilterItems.from_json(json)
# print the JSON string representation of the object
print(DeviceIncludeFilterItems.to_json())

# convert the object into a dict
device_include_filter_items_dict = device_include_filter_items_instance.to_dict()
# create an instance of DeviceIncludeFilterItems from a dict
device_include_filter_items_form_dict = device_include_filter_items.from_dict(device_include_filter_items_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


