# ScanFields


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **bool** |  | [optional] 
**read_acl** | **bool** |  | [optional] 
**write_acl** | **bool** |  | [optional] 
**owner** | **bool** |  | [optional] 
**created_at** | **bool** |  | [optional] 
**created_by** | **bool** |  | [optional] 
**scan_type** | **bool** |  | [optional] 
**scan_parameter** | **bool** |  | [optional] 
**user_parameter** | **bool** |  | [optional] 
**scan_id** | **bool** |  | [optional] 
**request_id** | **bool** |  | [optional] 
**queue_id** | **bool** |  | [optional] 
**exit_status** | **bool** |  | [optional] 
**scan_number** | **bool** |  | [optional] 
**metadata** | **bool** |  | [optional] 
**files** | **bool** |  | [optional] 
**session_id** | **bool** |  | [optional] 
**dataset_id** | **bool** |  | [optional] 
**experiment_id** | **bool** |  | [optional] 

## Example

```python
from py_scibec_openapi_client.models.scan_fields import ScanFields

# TODO update the JSON string below
json = "{}"
# create an instance of ScanFields from a JSON string
scan_fields_instance = ScanFields.from_json(json)
# print the JSON string representation of the object
print(ScanFields.to_json())

# convert the object into a dict
scan_fields_dict = scan_fields_instance.to_dict()
# create an instance of ScanFields from a dict
scan_fields_form_dict = scan_fields.from_dict(scan_fields_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


