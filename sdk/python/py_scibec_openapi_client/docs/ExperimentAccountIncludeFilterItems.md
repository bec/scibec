# ExperimentAccountIncludeFilterItems


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**relation** | **str** |  | [optional] 
**scope** | [**ExperimentAccountScopeFilter**](ExperimentAccountScopeFilter.md) |  | [optional] 

## Example

```python
from py_scibec_openapi_client.models.experiment_account_include_filter_items import ExperimentAccountIncludeFilterItems

# TODO update the JSON string below
json = "{}"
# create an instance of ExperimentAccountIncludeFilterItems from a JSON string
experiment_account_include_filter_items_instance = ExperimentAccountIncludeFilterItems.from_json(json)
# print the JSON string representation of the object
print(ExperimentAccountIncludeFilterItems.to_json())

# convert the object into a dict
experiment_account_include_filter_items_dict = experiment_account_include_filter_items_instance.to_dict()
# create an instance of ExperimentAccountIncludeFilterItems from a dict
experiment_account_include_filter_items_form_dict = experiment_account_include_filter_items.from_dict(experiment_account_include_filter_items_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


