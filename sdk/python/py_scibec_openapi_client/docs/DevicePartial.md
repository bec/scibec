# DevicePartial

(tsType: Partial<Device>, schemaOptions: { partial: true })

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** |  | [optional] 
**read_acl** | **List[str]** |  | [optional] 
**write_acl** | **List[str]** |  | [optional] 
**owner** | **List[str]** |  | [optional] 
**created_at** | **datetime** |  | [optional] 
**created_by** | **str** |  | [optional] 
**name** | **str** |  | [optional] 
**description** | **str** |  | [optional] 
**parent_id** | **str** |  | [optional] 
**session_id** | **str** | Session to which this device belongs. | [optional] 
**enabled** | **bool** | True if the device should be enabled. | [optional] 
**read_only** | **bool** | True if the device is read-only. | [optional] 
**software_trigger** | **bool** | True if the device should receive a trigger call from BEC. | [optional] 
**device_class** | **str** | Ophyd device class | [optional] 
**device_tags** | **List[str]** |  | [optional] 
**device_config** | **object** | Device config, including the ophyd init arguments. Must at least contain name and label. | [optional] 
**readout_priority** | **str** | Readout priority. \&quot;on_request\&quot; will only read the device when requested. \&quot;baseline\&quot; will read the device once at the beginning of the session. \&quot;monitored\&quot; will read the device at every trigger from BEC. \&quot;async\&quot; will read the device asynchronously. \&quot;continuous\&quot; will read the device at its self-defined frequency, beyond a single scan. | [optional] 
**on_failure** | **str** | Defines how device failures are handled. \&quot;raise\&quot; raises an error immediately. \&quot;buffer\&quot; will try fall back to old values, should this not be possible, an error will be raised. \&quot;retry\&quot; will retry once before raising an error. | [optional] 
**user_parameter** | **object** | Additional fields for user settings such as in and out positions. | [optional] 

## Example

```python
from py_scibec_openapi_client.models.device_partial import DevicePartial

# TODO update the JSON string below
json = "{}"
# create an instance of DevicePartial from a JSON string
device_partial_instance = DevicePartial.from_json(json)
# print the JSON string representation of the object
print(DevicePartial.to_json())

# convert the object into a dict
device_partial_dict = device_partial_instance.to_dict()
# create an instance of DevicePartial from a dict
device_partial_form_dict = device_partial.from_dict(device_partial_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


