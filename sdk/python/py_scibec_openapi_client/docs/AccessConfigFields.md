# AccessConfigFields


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **bool** |  | [optional] 
**read_acl** | **bool** |  | [optional] 
**write_acl** | **bool** |  | [optional] 
**owner** | **bool** |  | [optional] 
**created_at** | **bool** |  | [optional] 
**created_by** | **bool** |  | [optional] 
**target_account** | **bool** |  | [optional] 
**beamline_id** | **bool** |  | [optional] 
**auth_enabled** | **bool** |  | [optional] 
**active_accounts** | **bool** |  | [optional] 
**use_passwords** | **bool** |  | [optional] 

## Example

```python
from py_scibec_openapi_client.models.access_config_fields import AccessConfigFields

# TODO update the JSON string below
json = "{}"
# create an instance of AccessConfigFields from a JSON string
access_config_fields_instance = AccessConfigFields.from_json(json)
# print the JSON string representation of the object
print(AccessConfigFields.to_json())

# convert the object into a dict
access_config_fields_dict = access_config_fields_instance.to_dict()
# create an instance of AccessConfigFields from a dict
access_config_fields_form_dict = access_config_fields.from_dict(access_config_fields_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


