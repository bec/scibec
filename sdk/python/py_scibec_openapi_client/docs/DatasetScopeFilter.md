# DatasetScopeFilter


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**offset** | **int** |  | [optional] 
**limit** | **int** |  | [optional] 
**skip** | **int** |  | [optional] 
**order** | [**AccessAccountFilterOrder**](AccessAccountFilterOrder.md) |  | [optional] 
**where** | **Dict[str, object]** |  | [optional] 
**fields** | [**AccessConfigScopeFilterFields**](AccessConfigScopeFilterFields.md) |  | [optional] 
**include** | **List[Dict[str, object]]** |  | [optional] 

## Example

```python
from py_scibec_openapi_client.models.dataset_scope_filter import DatasetScopeFilter

# TODO update the JSON string below
json = "{}"
# create an instance of DatasetScopeFilter from a JSON string
dataset_scope_filter_instance = DatasetScopeFilter.from_json(json)
# print the JSON string representation of the object
print(DatasetScopeFilter.to_json())

# convert the object into a dict
dataset_scope_filter_dict = dataset_scope_filter_instance.to_dict()
# create an instance of DatasetScopeFilter from a dict
dataset_scope_filter_form_dict = dataset_scope_filter.from_dict(dataset_scope_filter_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


