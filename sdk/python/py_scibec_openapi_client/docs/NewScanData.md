# NewScanData

(tsType: Omit<ScanData, 'id'>, schemaOptions: { title: 'NewScanData', exclude: [ 'id' ] })

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**read_acl** | **List[str]** |  | [optional] 
**write_acl** | **List[str]** |  | [optional] 
**owner** | **List[str]** |  | [optional] 
**created_at** | **datetime** |  | [optional] 
**created_by** | **str** |  | [optional] 
**name** | **str** |  | [optional] 
**data** | **object** |  | [optional] 
**file_path** | **str** |  | [optional] 
**scan_id** | **str** | The parent scan | [optional] 

## Example

```python
from py_scibec_openapi_client.models.new_scan_data import NewScanData

# TODO update the JSON string below
json = "{}"
# create an instance of NewScanData from a JSON string
new_scan_data_instance = NewScanData.from_json(json)
# print the JSON string representation of the object
print(NewScanData.to_json())

# convert the object into a dict
new_scan_data_dict = new_scan_data_instance.to_dict()
# create an instance of NewScanData from a dict
new_scan_data_form_dict = new_scan_data.from_dict(new_scan_data_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


