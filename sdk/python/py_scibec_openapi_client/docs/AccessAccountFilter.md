# AccessAccountFilter


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**offset** | **int** |  | [optional] 
**limit** | **int** |  | [optional] 
**skip** | **int** |  | [optional] 
**order** | [**AccessAccountFilterOrder**](AccessAccountFilterOrder.md) |  | [optional] 
**fields** | [**AccessAccountFields**](AccessAccountFields.md) |  | [optional] 

## Example

```python
from py_scibec_openapi_client.models.access_account_filter import AccessAccountFilter

# TODO update the JSON string below
json = "{}"
# create an instance of AccessAccountFilter from a JSON string
access_account_filter_instance = AccessAccountFilter.from_json(json)
# print the JSON string representation of the object
print(AccessAccountFilter.to_json())

# convert the object into a dict
access_account_filter_dict = access_account_filter_instance.to_dict()
# create an instance of AccessAccountFilter from a dict
access_account_filter_form_dict = access_account_filter.from_dict(access_account_filter_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


