# FunctionalAccountPartial

(tsType: Partial<FunctionalAccount>, schemaOptions: { partial: true })

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** |  | [optional] 
**read_acl** | **List[str]** |  | [optional] 
**write_acl** | **List[str]** |  | [optional] 
**owner** | **List[str]** |  | [optional] 
**created_at** | **datetime** |  | [optional] 
**created_by** | **str** |  | [optional] 
**name** | **str** |  | [optional] 
**read** | **bool** |  | [optional] 
**write** | **bool** |  | [optional] 
**remote** | **bool** |  | [optional] 
**token** | **str** |  | [optional] 
**is_functional** | **bool** |  | [optional] 
**access_config_id** | **str** | AccessConfig to which this account belongs. | [optional] 

## Example

```python
from py_scibec_openapi_client.models.functional_account_partial import FunctionalAccountPartial

# TODO update the JSON string below
json = "{}"
# create an instance of FunctionalAccountPartial from a JSON string
functional_account_partial_instance = FunctionalAccountPartial.from_json(json)
# print the JSON string representation of the object
print(FunctionalAccountPartial.to_json())

# convert the object into a dict
functional_account_partial_dict = functional_account_partial_instance.to_dict()
# create an instance of FunctionalAccountPartial from a dict
functional_account_partial_form_dict = functional_account_partial.from_dict(functional_account_partial_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


