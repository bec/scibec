# ScanDataPartial

(tsType: Partial<ScanData>, schemaOptions: { partial: true })

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** |  | [optional] 
**read_acl** | **List[str]** |  | [optional] 
**write_acl** | **List[str]** |  | [optional] 
**owner** | **List[str]** |  | [optional] 
**created_at** | **datetime** |  | [optional] 
**created_by** | **str** |  | [optional] 
**name** | **str** |  | [optional] 
**data** | **object** |  | [optional] 
**file_path** | **str** |  | [optional] 
**scan_id** | **str** | The parent scan | [optional] 

## Example

```python
from py_scibec_openapi_client.models.scan_data_partial import ScanDataPartial

# TODO update the JSON string below
json = "{}"
# create an instance of ScanDataPartial from a JSON string
scan_data_partial_instance = ScanDataPartial.from_json(json)
# print the JSON string representation of the object
print(ScanDataPartial.to_json())

# convert the object into a dict
scan_data_partial_dict = scan_data_partial_instance.to_dict()
# create an instance of ScanDataPartial from a dict
scan_data_partial_form_dict = scan_data_partial.from_dict(scan_data_partial_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


