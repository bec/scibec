# UserControllerLogin200Response


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**token** | **str** |  | [optional] 

## Example

```python
from py_scibec_openapi_client.models.user_controller_login200_response import UserControllerLogin200Response

# TODO update the JSON string below
json = "{}"
# create an instance of UserControllerLogin200Response from a JSON string
user_controller_login200_response_instance = UserControllerLogin200Response.from_json(json)
# print the JSON string representation of the object
print(UserControllerLogin200Response.to_json())

# convert the object into a dict
user_controller_login200_response_dict = user_controller_login200_response_instance.to_dict()
# create an instance of UserControllerLogin200Response from a dict
user_controller_login200_response_form_dict = user_controller_login200_response.from_dict(user_controller_login200_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


