# AccessConfigIncludeFilterInner


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**relation** | **str** |  | [optional] 
**scope** | [**AccessConfigScopeFilter**](AccessConfigScopeFilter.md) |  | [optional] 

## Example

```python
from py_scibec_openapi_client.models.access_config_include_filter_inner import AccessConfigIncludeFilterInner

# TODO update the JSON string below
json = "{}"
# create an instance of AccessConfigIncludeFilterInner from a JSON string
access_config_include_filter_inner_instance = AccessConfigIncludeFilterInner.from_json(json)
# print the JSON string representation of the object
print(AccessConfigIncludeFilterInner.to_json())

# convert the object into a dict
access_config_include_filter_inner_dict = access_config_include_filter_inner_instance.to_dict()
# create an instance of AccessConfigIncludeFilterInner from a dict
access_config_include_filter_inner_form_dict = access_config_include_filter_inner.from_dict(access_config_include_filter_inner_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


