# py_scibec_openapi_client.FunctionalAccountControllerApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**functional_account_controller_count**](FunctionalAccountControllerApi.md#functional_account_controller_count) | **GET** /functional-accounts/count | 
[**functional_account_controller_create**](FunctionalAccountControllerApi.md#functional_account_controller_create) | **POST** /functional-accounts | 
[**functional_account_controller_delete_by_id**](FunctionalAccountControllerApi.md#functional_account_controller_delete_by_id) | **DELETE** /functional-accounts/{id} | 
[**functional_account_controller_find**](FunctionalAccountControllerApi.md#functional_account_controller_find) | **GET** /functional-accounts | 
[**functional_account_controller_find_by_id**](FunctionalAccountControllerApi.md#functional_account_controller_find_by_id) | **GET** /functional-accounts/{id} | 
[**functional_account_controller_update_all**](FunctionalAccountControllerApi.md#functional_account_controller_update_all) | **PATCH** /functional-accounts | 
[**functional_account_controller_update_by_id**](FunctionalAccountControllerApi.md#functional_account_controller_update_by_id) | **PATCH** /functional-accounts/{id} | 


# **functional_account_controller_count**
> LoopbackCount functional_account_controller_count(where=where)



### Example

* Bearer (JWT) Authentication (jwt):

```python
import py_scibec_openapi_client
from py_scibec_openapi_client.models.loopback_count import LoopbackCount
from py_scibec_openapi_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = py_scibec_openapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization (JWT): jwt
configuration = py_scibec_openapi_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
with py_scibec_openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = py_scibec_openapi_client.FunctionalAccountControllerApi(api_client)
    where = None # Dict[str, object] |  (optional)

    try:
        api_response = api_instance.functional_account_controller_count(where=where)
        print("The response of FunctionalAccountControllerApi->functional_account_controller_count:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling FunctionalAccountControllerApi->functional_account_controller_count: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | [**Dict[str, object]**](object.md)|  | [optional] 

### Return type

[**LoopbackCount**](LoopbackCount.md)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | FunctionalAccount model count |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **functional_account_controller_create**
> FunctionalAccount functional_account_controller_create(new_functional_account=new_functional_account)



### Example

* Bearer (JWT) Authentication (jwt):

```python
import py_scibec_openapi_client
from py_scibec_openapi_client.models.functional_account import FunctionalAccount
from py_scibec_openapi_client.models.new_functional_account import NewFunctionalAccount
from py_scibec_openapi_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = py_scibec_openapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization (JWT): jwt
configuration = py_scibec_openapi_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
with py_scibec_openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = py_scibec_openapi_client.FunctionalAccountControllerApi(api_client)
    new_functional_account = py_scibec_openapi_client.NewFunctionalAccount() # NewFunctionalAccount |  (optional)

    try:
        api_response = api_instance.functional_account_controller_create(new_functional_account=new_functional_account)
        print("The response of FunctionalAccountControllerApi->functional_account_controller_create:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling FunctionalAccountControllerApi->functional_account_controller_create: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **new_functional_account** | [**NewFunctionalAccount**](NewFunctionalAccount.md)|  | [optional] 

### Return type

[**FunctionalAccount**](FunctionalAccount.md)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | FunctionalAccount model instance |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **functional_account_controller_delete_by_id**
> functional_account_controller_delete_by_id(id)



### Example

* Bearer (JWT) Authentication (jwt):

```python
import py_scibec_openapi_client
from py_scibec_openapi_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = py_scibec_openapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization (JWT): jwt
configuration = py_scibec_openapi_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
with py_scibec_openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = py_scibec_openapi_client.FunctionalAccountControllerApi(api_client)
    id = 'id_example' # str | 

    try:
        api_instance.functional_account_controller_delete_by_id(id)
    except Exception as e:
        print("Exception when calling FunctionalAccountControllerApi->functional_account_controller_delete_by_id: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**|  | 

### Return type

void (empty response body)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | FunctionalAccount DELETE success |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **functional_account_controller_find**
> List[FunctionalAccountWithRelations] functional_account_controller_find(filter=filter)



### Example

* Bearer (JWT) Authentication (jwt):

```python
import py_scibec_openapi_client
from py_scibec_openapi_client.models.functional_account_filter1 import FunctionalAccountFilter1
from py_scibec_openapi_client.models.functional_account_with_relations import FunctionalAccountWithRelations
from py_scibec_openapi_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = py_scibec_openapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization (JWT): jwt
configuration = py_scibec_openapi_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
with py_scibec_openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = py_scibec_openapi_client.FunctionalAccountControllerApi(api_client)
    filter = py_scibec_openapi_client.FunctionalAccountFilter1() # FunctionalAccountFilter1 |  (optional)

    try:
        api_response = api_instance.functional_account_controller_find(filter=filter)
        print("The response of FunctionalAccountControllerApi->functional_account_controller_find:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling FunctionalAccountControllerApi->functional_account_controller_find: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | [**FunctionalAccountFilter1**](.md)|  | [optional] 

### Return type

[**List[FunctionalAccountWithRelations]**](FunctionalAccountWithRelations.md)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Array of FunctionalAccount model instances |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **functional_account_controller_find_by_id**
> FunctionalAccountWithRelations functional_account_controller_find_by_id(id, filter=filter)



### Example

* Bearer (JWT) Authentication (jwt):

```python
import py_scibec_openapi_client
from py_scibec_openapi_client.models.functional_account_filter import FunctionalAccountFilter
from py_scibec_openapi_client.models.functional_account_with_relations import FunctionalAccountWithRelations
from py_scibec_openapi_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = py_scibec_openapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization (JWT): jwt
configuration = py_scibec_openapi_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
with py_scibec_openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = py_scibec_openapi_client.FunctionalAccountControllerApi(api_client)
    id = 'id_example' # str | 
    filter = py_scibec_openapi_client.FunctionalAccountFilter() # FunctionalAccountFilter |  (optional)

    try:
        api_response = api_instance.functional_account_controller_find_by_id(id, filter=filter)
        print("The response of FunctionalAccountControllerApi->functional_account_controller_find_by_id:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling FunctionalAccountControllerApi->functional_account_controller_find_by_id: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**|  | 
 **filter** | [**FunctionalAccountFilter**](.md)|  | [optional] 

### Return type

[**FunctionalAccountWithRelations**](FunctionalAccountWithRelations.md)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | FunctionalAccount model instance |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **functional_account_controller_update_all**
> LoopbackCount functional_account_controller_update_all(where=where, functional_account_partial=functional_account_partial)



### Example

* Bearer (JWT) Authentication (jwt):

```python
import py_scibec_openapi_client
from py_scibec_openapi_client.models.functional_account_partial import FunctionalAccountPartial
from py_scibec_openapi_client.models.loopback_count import LoopbackCount
from py_scibec_openapi_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = py_scibec_openapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization (JWT): jwt
configuration = py_scibec_openapi_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
with py_scibec_openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = py_scibec_openapi_client.FunctionalAccountControllerApi(api_client)
    where = None # Dict[str, object] |  (optional)
    functional_account_partial = py_scibec_openapi_client.FunctionalAccountPartial() # FunctionalAccountPartial |  (optional)

    try:
        api_response = api_instance.functional_account_controller_update_all(where=where, functional_account_partial=functional_account_partial)
        print("The response of FunctionalAccountControllerApi->functional_account_controller_update_all:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling FunctionalAccountControllerApi->functional_account_controller_update_all: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | [**Dict[str, object]**](object.md)|  | [optional] 
 **functional_account_partial** | [**FunctionalAccountPartial**](FunctionalAccountPartial.md)|  | [optional] 

### Return type

[**LoopbackCount**](LoopbackCount.md)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | FunctionalAccount PATCH success count |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **functional_account_controller_update_by_id**
> functional_account_controller_update_by_id(id, functional_account_partial=functional_account_partial)



### Example

* Bearer (JWT) Authentication (jwt):

```python
import py_scibec_openapi_client
from py_scibec_openapi_client.models.functional_account_partial import FunctionalAccountPartial
from py_scibec_openapi_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = py_scibec_openapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization (JWT): jwt
configuration = py_scibec_openapi_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
with py_scibec_openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = py_scibec_openapi_client.FunctionalAccountControllerApi(api_client)
    id = 'id_example' # str | 
    functional_account_partial = py_scibec_openapi_client.FunctionalAccountPartial() # FunctionalAccountPartial |  (optional)

    try:
        api_instance.functional_account_controller_update_by_id(id, functional_account_partial=functional_account_partial)
    except Exception as e:
        print("Exception when calling FunctionalAccountControllerApi->functional_account_controller_update_by_id: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**|  | 
 **functional_account_partial** | [**FunctionalAccountPartial**](FunctionalAccountPartial.md)|  | [optional] 

### Return type

void (empty response body)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | FunctionalAccount PATCH success |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

