# AccessConfigPartial

(tsType: Partial<AccessConfig>, schemaOptions: { partial: true })

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** |  | [optional] 
**read_acl** | **List[str]** |  | [optional] 
**write_acl** | **List[str]** |  | [optional] 
**owner** | **List[str]** |  | [optional] 
**created_at** | **datetime** |  | [optional] 
**created_by** | **str** |  | [optional] 
**target_account** | **str** |  | [optional] 
**beamline_id** | **str** | Beamline to which this config belongs. | [optional] 
**auth_enabled** | **bool** |  | [optional] 
**active_accounts** | **List[str]** |  | [optional] 
**use_passwords** | **bool** |  | [optional] 

## Example

```python
from py_scibec_openapi_client.models.access_config_partial import AccessConfigPartial

# TODO update the JSON string below
json = "{}"
# create an instance of AccessConfigPartial from a JSON string
access_config_partial_instance = AccessConfigPartial.from_json(json)
# print the JSON string representation of the object
print(AccessConfigPartial.to_json())

# convert the object into a dict
access_config_partial_dict = access_config_partial_instance.to_dict()
# create an instance of AccessConfigPartial from a dict
access_config_partial_form_dict = access_config_partial.from_dict(access_config_partial_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


