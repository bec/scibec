# AccessConfigIncludeFilterItems


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**relation** | **str** |  | [optional] 
**scope** | [**AccessConfigScopeFilter**](AccessConfigScopeFilter.md) |  | [optional] 

## Example

```python
from py_scibec_openapi_client.models.access_config_include_filter_items import AccessConfigIncludeFilterItems

# TODO update the JSON string below
json = "{}"
# create an instance of AccessConfigIncludeFilterItems from a JSON string
access_config_include_filter_items_instance = AccessConfigIncludeFilterItems.from_json(json)
# print the JSON string representation of the object
print(AccessConfigIncludeFilterItems.to_json())

# convert the object into a dict
access_config_include_filter_items_dict = access_config_include_filter_items_instance.to_dict()
# create an instance of AccessConfigIncludeFilterItems from a dict
access_config_include_filter_items_form_dict = access_config_include_filter_items.from_dict(access_config_include_filter_items_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


