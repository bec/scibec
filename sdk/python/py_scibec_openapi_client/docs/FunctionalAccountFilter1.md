# FunctionalAccountFilter1


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**offset** | **int** |  | [optional] 
**limit** | **int** |  | [optional] 
**skip** | **int** |  | [optional] 
**order** | [**AccessAccountFilterOrder**](AccessAccountFilterOrder.md) |  | [optional] 
**where** | **Dict[str, object]** |  | [optional] 
**fields** | [**FunctionalAccountFields**](FunctionalAccountFields.md) |  | [optional] 
**include** | [**List[FunctionalAccountIncludeFilterInner]**](FunctionalAccountIncludeFilterInner.md) |  | [optional] 

## Example

```python
from py_scibec_openapi_client.models.functional_account_filter1 import FunctionalAccountFilter1

# TODO update the JSON string below
json = "{}"
# create an instance of FunctionalAccountFilter1 from a JSON string
functional_account_filter1_instance = FunctionalAccountFilter1.from_json(json)
# print the JSON string representation of the object
print(FunctionalAccountFilter1.to_json())

# convert the object into a dict
functional_account_filter1_dict = functional_account_filter1_instance.to_dict()
# create an instance of FunctionalAccountFilter1 from a dict
functional_account_filter1_form_dict = functional_account_filter1.from_dict(functional_account_filter1_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


