# Beamline


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** |  | [optional] 
**read_acl** | **List[str]** |  | [optional] 
**write_acl** | **List[str]** |  | [optional] 
**owner** | **List[str]** |  | [optional] 
**created_at** | **datetime** |  | [optional] 
**created_by** | **str** |  | [optional] 
**name** | **str** |  | 
**active_experiment** | **str** |  | [optional] 

## Example

```python
from py_scibec_openapi_client.models.beamline import Beamline

# TODO update the JSON string below
json = "{}"
# create an instance of Beamline from a JSON string
beamline_instance = Beamline.from_json(json)
# print the JSON string representation of the object
print(Beamline.to_json())

# convert the object into a dict
beamline_dict = beamline_instance.to_dict()
# create an instance of Beamline from a dict
beamline_form_dict = beamline.from_dict(beamline_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


