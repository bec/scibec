# py_scibec_openapi_client.AccessAccountControllerApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**access_account_controller_count**](AccessAccountControllerApi.md#access_account_controller_count) | **GET** /access-accounts/count | 
[**access_account_controller_create**](AccessAccountControllerApi.md#access_account_controller_create) | **POST** /access-accounts | 
[**access_account_controller_delete_by_id**](AccessAccountControllerApi.md#access_account_controller_delete_by_id) | **DELETE** /access-accounts/{id} | 
[**access_account_controller_find**](AccessAccountControllerApi.md#access_account_controller_find) | **GET** /access-accounts | 
[**access_account_controller_find_by_id**](AccessAccountControllerApi.md#access_account_controller_find_by_id) | **GET** /access-accounts/{id} | 
[**access_account_controller_update_all**](AccessAccountControllerApi.md#access_account_controller_update_all) | **PATCH** /access-accounts | 
[**access_account_controller_update_by_id**](AccessAccountControllerApi.md#access_account_controller_update_by_id) | **PATCH** /access-accounts/{id} | 


# **access_account_controller_count**
> LoopbackCount access_account_controller_count(where=where)



### Example

* Bearer (JWT) Authentication (jwt):

```python
import py_scibec_openapi_client
from py_scibec_openapi_client.models.loopback_count import LoopbackCount
from py_scibec_openapi_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = py_scibec_openapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization (JWT): jwt
configuration = py_scibec_openapi_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
with py_scibec_openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = py_scibec_openapi_client.AccessAccountControllerApi(api_client)
    where = None # Dict[str, object] |  (optional)

    try:
        api_response = api_instance.access_account_controller_count(where=where)
        print("The response of AccessAccountControllerApi->access_account_controller_count:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling AccessAccountControllerApi->access_account_controller_count: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | [**Dict[str, object]**](object.md)|  | [optional] 

### Return type

[**LoopbackCount**](LoopbackCount.md)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | AccessAccount model count |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **access_account_controller_create**
> AccessAccount access_account_controller_create(new_access_account=new_access_account)



### Example

* Bearer (JWT) Authentication (jwt):

```python
import py_scibec_openapi_client
from py_scibec_openapi_client.models.access_account import AccessAccount
from py_scibec_openapi_client.models.new_access_account import NewAccessAccount
from py_scibec_openapi_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = py_scibec_openapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization (JWT): jwt
configuration = py_scibec_openapi_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
with py_scibec_openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = py_scibec_openapi_client.AccessAccountControllerApi(api_client)
    new_access_account = py_scibec_openapi_client.NewAccessAccount() # NewAccessAccount |  (optional)

    try:
        api_response = api_instance.access_account_controller_create(new_access_account=new_access_account)
        print("The response of AccessAccountControllerApi->access_account_controller_create:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling AccessAccountControllerApi->access_account_controller_create: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **new_access_account** | [**NewAccessAccount**](NewAccessAccount.md)|  | [optional] 

### Return type

[**AccessAccount**](AccessAccount.md)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | AccessAccount model instance |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **access_account_controller_delete_by_id**
> access_account_controller_delete_by_id(id)



### Example

* Bearer (JWT) Authentication (jwt):

```python
import py_scibec_openapi_client
from py_scibec_openapi_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = py_scibec_openapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization (JWT): jwt
configuration = py_scibec_openapi_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
with py_scibec_openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = py_scibec_openapi_client.AccessAccountControllerApi(api_client)
    id = 'id_example' # str | 

    try:
        api_instance.access_account_controller_delete_by_id(id)
    except Exception as e:
        print("Exception when calling AccessAccountControllerApi->access_account_controller_delete_by_id: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**|  | 

### Return type

void (empty response body)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | AccessAccount DELETE success |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **access_account_controller_find**
> List[AccessAccountWithRelations] access_account_controller_find(filter=filter)



### Example

* Bearer (JWT) Authentication (jwt):

```python
import py_scibec_openapi_client
from py_scibec_openapi_client.models.access_account_filter1 import AccessAccountFilter1
from py_scibec_openapi_client.models.access_account_with_relations import AccessAccountWithRelations
from py_scibec_openapi_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = py_scibec_openapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization (JWT): jwt
configuration = py_scibec_openapi_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
with py_scibec_openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = py_scibec_openapi_client.AccessAccountControllerApi(api_client)
    filter = py_scibec_openapi_client.AccessAccountFilter1() # AccessAccountFilter1 |  (optional)

    try:
        api_response = api_instance.access_account_controller_find(filter=filter)
        print("The response of AccessAccountControllerApi->access_account_controller_find:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling AccessAccountControllerApi->access_account_controller_find: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | [**AccessAccountFilter1**](.md)|  | [optional] 

### Return type

[**List[AccessAccountWithRelations]**](AccessAccountWithRelations.md)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Array of AccessAccount model instances |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **access_account_controller_find_by_id**
> AccessAccountWithRelations access_account_controller_find_by_id(id, filter=filter)



### Example

* Bearer (JWT) Authentication (jwt):

```python
import py_scibec_openapi_client
from py_scibec_openapi_client.models.access_account_filter import AccessAccountFilter
from py_scibec_openapi_client.models.access_account_with_relations import AccessAccountWithRelations
from py_scibec_openapi_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = py_scibec_openapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization (JWT): jwt
configuration = py_scibec_openapi_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
with py_scibec_openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = py_scibec_openapi_client.AccessAccountControllerApi(api_client)
    id = 'id_example' # str | 
    filter = py_scibec_openapi_client.AccessAccountFilter() # AccessAccountFilter |  (optional)

    try:
        api_response = api_instance.access_account_controller_find_by_id(id, filter=filter)
        print("The response of AccessAccountControllerApi->access_account_controller_find_by_id:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling AccessAccountControllerApi->access_account_controller_find_by_id: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**|  | 
 **filter** | [**AccessAccountFilter**](.md)|  | [optional] 

### Return type

[**AccessAccountWithRelations**](AccessAccountWithRelations.md)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | AccessAccount model instance |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **access_account_controller_update_all**
> LoopbackCount access_account_controller_update_all(where=where, access_account_partial=access_account_partial)



### Example

* Bearer (JWT) Authentication (jwt):

```python
import py_scibec_openapi_client
from py_scibec_openapi_client.models.access_account_partial import AccessAccountPartial
from py_scibec_openapi_client.models.loopback_count import LoopbackCount
from py_scibec_openapi_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = py_scibec_openapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization (JWT): jwt
configuration = py_scibec_openapi_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
with py_scibec_openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = py_scibec_openapi_client.AccessAccountControllerApi(api_client)
    where = None # Dict[str, object] |  (optional)
    access_account_partial = py_scibec_openapi_client.AccessAccountPartial() # AccessAccountPartial |  (optional)

    try:
        api_response = api_instance.access_account_controller_update_all(where=where, access_account_partial=access_account_partial)
        print("The response of AccessAccountControllerApi->access_account_controller_update_all:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling AccessAccountControllerApi->access_account_controller_update_all: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | [**Dict[str, object]**](object.md)|  | [optional] 
 **access_account_partial** | [**AccessAccountPartial**](AccessAccountPartial.md)|  | [optional] 

### Return type

[**LoopbackCount**](LoopbackCount.md)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | AccessAccount PATCH success count |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **access_account_controller_update_by_id**
> access_account_controller_update_by_id(id, access_account_partial=access_account_partial)



### Example

* Bearer (JWT) Authentication (jwt):

```python
import py_scibec_openapi_client
from py_scibec_openapi_client.models.access_account_partial import AccessAccountPartial
from py_scibec_openapi_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = py_scibec_openapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization (JWT): jwt
configuration = py_scibec_openapi_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
with py_scibec_openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = py_scibec_openapi_client.AccessAccountControllerApi(api_client)
    id = 'id_example' # str | 
    access_account_partial = py_scibec_openapi_client.AccessAccountPartial() # AccessAccountPartial |  (optional)

    try:
        api_instance.access_account_controller_update_by_id(id, access_account_partial=access_account_partial)
    except Exception as e:
        print("Exception when calling AccessAccountControllerApi->access_account_controller_update_by_id: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**|  | 
 **access_account_partial** | [**AccessAccountPartial**](AccessAccountPartial.md)|  | [optional] 

### Return type

void (empty response body)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | AccessAccount PATCH success |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

