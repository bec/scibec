# AccessConfigScopeFilterFields


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------

## Example

```python
from py_scibec_openapi_client.models.access_config_scope_filter_fields import AccessConfigScopeFilterFields

# TODO update the JSON string below
json = "{}"
# create an instance of AccessConfigScopeFilterFields from a JSON string
access_config_scope_filter_fields_instance = AccessConfigScopeFilterFields.from_json(json)
# print the JSON string representation of the object
print(AccessConfigScopeFilterFields.to_json())

# convert the object into a dict
access_config_scope_filter_fields_dict = access_config_scope_filter_fields_instance.to_dict()
# create an instance of AccessConfigScopeFilterFields from a dict
access_config_scope_filter_fields_form_dict = access_config_scope_filter_fields.from_dict(access_config_scope_filter_fields_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


