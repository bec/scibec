# py_scibec_openapi_client.OIDCControllerApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**o_idc_controller_login_to_third_party**](OIDCControllerApi.md#o_idc_controller_login_to_third_party) | **GET** /auth/login | 
[**o_idc_controller_logout**](OIDCControllerApi.md#o_idc_controller_logout) | **GET** /auth/logout | 
[**o_idc_controller_third_party_call_back**](OIDCControllerApi.md#o_idc_controller_third_party_call_back) | **GET** /auth/callback | 


# **o_idc_controller_login_to_third_party**
> o_idc_controller_login_to_third_party()



### Example


```python
import py_scibec_openapi_client
from py_scibec_openapi_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = py_scibec_openapi_client.Configuration(
    host = "http://localhost"
)


# Enter a context with an instance of the API client
with py_scibec_openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = py_scibec_openapi_client.OIDCControllerApi(api_client)

    try:
        api_instance.o_idc_controller_login_to_third_party()
    except Exception as e:
        print("Exception when calling OIDCControllerApi->o_idc_controller_login_to_third_party: %s\n" % e)
```



### Parameters

This endpoint does not need any parameter.

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Return value of OIDCController.loginToThirdParty |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **o_idc_controller_logout**
> o_idc_controller_logout()



### Example


```python
import py_scibec_openapi_client
from py_scibec_openapi_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = py_scibec_openapi_client.Configuration(
    host = "http://localhost"
)


# Enter a context with an instance of the API client
with py_scibec_openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = py_scibec_openapi_client.OIDCControllerApi(api_client)

    try:
        api_instance.o_idc_controller_logout()
    except Exception as e:
        print("Exception when calling OIDCControllerApi->o_idc_controller_logout: %s\n" % e)
```



### Parameters

This endpoint does not need any parameter.

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Return value of OIDCController.logout |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **o_idc_controller_third_party_call_back**
> o_idc_controller_third_party_call_back()



### Example


```python
import py_scibec_openapi_client
from py_scibec_openapi_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = py_scibec_openapi_client.Configuration(
    host = "http://localhost"
)


# Enter a context with an instance of the API client
with py_scibec_openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = py_scibec_openapi_client.OIDCControllerApi(api_client)

    try:
        api_instance.o_idc_controller_third_party_call_back()
    except Exception as e:
        print("Exception when calling OIDCControllerApi->o_idc_controller_third_party_call_back: %s\n" % e)
```



### Parameters

This endpoint does not need any parameter.

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Return value of OIDCController.thirdPartyCallBack |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

