# UserControllerLoginRequest


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**principal** | **str** |  | 
**password** | **str** |  | 

## Example

```python
from py_scibec_openapi_client.models.user_controller_login_request import UserControllerLoginRequest

# TODO update the JSON string below
json = "{}"
# create an instance of UserControllerLoginRequest from a JSON string
user_controller_login_request_instance = UserControllerLoginRequest.from_json(json)
# print the JSON string representation of the object
print(UserControllerLoginRequest.to_json())

# convert the object into a dict
user_controller_login_request_dict = user_controller_login_request_instance.to_dict()
# create an instance of UserControllerLoginRequest from a dict
user_controller_login_request_form_dict = user_controller_login_request.from_dict(user_controller_login_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


