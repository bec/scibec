#!/bin/bash

# first arg is the version number
version=$1
# replace the version number in sdk/python/py_scibec/setup.py
sed -i "s/__version__ = .*/__version__ = \"$version\"/g" ./setup.py

sed -i "s/VERSION = .*/VERSION = \"$version\"/g" ../py_scibec_openapi_client/setup.py