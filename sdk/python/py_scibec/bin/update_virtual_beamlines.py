if __name__ == "__main__":
    import argparse

    from dotenv import dotenv_values

    import py_scibec

    parser = argparse.ArgumentParser()
    parser.add_argument("--index", type=int, required=True)
    parser.add_argument("--delete", action="store_true", default=False)

    args = parser.parse_args()
    index = args.index
    delete = args.delete

    beamline = f"TESTBL{index:03d}"
    pgroups = [f"p{num:05d}" for num in range((index - 1) * 5, (index - 1) * 5 + 5)]

    bl_access_group = beamline.lower()

    # load env variables
    config = dotenv_values(".env")

    # login
    scibec = py_scibec.SciBecCore(host=config["HOST"])
    scibec.login(config["USER"], config["SECRET"])

    if delete:
        # delete beamline
        bl_filter = py_scibec.bec.AccessAccountFilterWhere(where={"name": beamline})
        bl = scibec.beamline.beamline_controller_find(filter=bl_filter)
        if bl.body:
            scibec.beamline.beamline_controller_delete_by_id(id=bl.body[0]["id"])

        # delete experiments
        for pgroup in pgroups:
            exp_filter = py_scibec.bec.AccessAccountFilterWhere(where={"writeAccount": pgroup})
            exp = scibec.experiment.experiment_controller_find(filter=exp_filter)
            if exp.body:
                scibec.experiment.experiment_controller_delete_by_id(id=exp.body[0]["id"])

        exit()

    beamline_read_acl = [bl_access_group]
    beamline_read_acl.extend(pgroups)
    bl = scibec.beamline.beamline_controller_create(
        py_scibec.bec.NewBeamline(
            name=beamline, readACL=beamline_read_acl, writeACL=["admin"], owner=["admin"]
        )
    )

    for num in range((index - 1) * 5, (index - 1) * 5 + 5):
        pgroup = f"p{num:05d}"
        experiment = {
            "name": f"Simulated Proposal {num:05d}",
            "readACL": [bl_access_group, pgroup],
            "writeACL": ["admin"],
            "owner": ["admin"],
            "beamlineId": bl.id,
            "writeAccount": pgroup,
            "experimentInfo": {
                "title": f"Simulated Proposal {num:05d}",
                "abstract": f"Simulated Proposal {num:05d}",
                "PI": "admin",
            },
        }
        res = scibec.experiment.experiment_controller_create(
            py_scibec.bec.NewExperiment(**experiment)
        )

    # set active experiment
    scibec.beamline.beamline_controller_update_by_id(
        id=bl.id, beamline_partial=py_scibec.bec.BeamlinePartial(activeExperiment=res.id)
    )
