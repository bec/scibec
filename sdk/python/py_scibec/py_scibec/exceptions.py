from py_scibec_openapi_client.exceptions import ApiException


class SciBecError(Exception):
    pass
