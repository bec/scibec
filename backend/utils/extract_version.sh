#!/bin/bash

version=$(npx semantic-release --dry-run --no-ci | grep "next release version" | awk '{print $11}')

# if the version number is not provided, take the version from the package.json
if [ -z "$version" ]
then
  # take the version from the packa
  version=$(cat package.json | grep version | awk '{print $2}' | sed 's/[",]//g')
fi

echo "$version" > ./VERSION


# replace the version number in sdk/python/py_scibec/setup.py
sed -i "s/__version__ = .*/__version__ = \"$version\"/g" ../sdk/python/py_scibec/setup.py