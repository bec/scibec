import {ApplicationConfig, ScibecApplication} from './application';
import * as fs from 'fs';
const path = require('path');
export * from './application';
var pckg = require('../package.json');
var version = pckg.version.split('.').shift();

export async function main(options: ApplicationConfig = {}) {
  const app = new ScibecApplication(options);
  await app.boot();
  await app.start();
  await app.startWebsocket();
  // app.restServer.httpServer!.server.keepAliveTimeout = 76000;
  // app.restServer.httpServer!.server.headersTimeout = 77000;
  const url = app.restServer.url;
  console.log(`Server is running at ${url}`);
  console.log(`Try ${url}/ping`);

  return app;
}

if (require.main === module) {
  // Run the application
  const port = process.env.PORT ?? 3000;

  const config = {
    rest: {
      // port: +(process.env.PORT ?? 3000),
      port,
      host: process.env.HOST,
      // get the API version from package.json
      basePath: process.env.BASE_PATH ?? 'api/v' + version,
      // basePath: process.env.BASE_PATH ?? 'api/v1',
      // The `gracePeriodForClose` provides a graceful close for http/https
      // servers with keep-alive clients. The default value is `Infinity`
      // (don't force-close). If you want to immediately destroy all sockets
      // upon stop, set its value to `0`.
      // See https://www.npmjs.com/package/stoppable
      gracePeriodForClose: 5000, // 5 seconds
      openApiSpec: {
        // useful when used with OpenAPI-to-GraphQL to locate your application
        setServersFromRequest: true,
      },
      websocket: {port},
    },
    databaseSeeding: false,
    oidcOptions: fs.existsSync(path.resolve(__dirname, '../oidc.json'))
      ? require('../oidc.json')
      : undefined,
  };
  console.log('options: ', config.oidcOptions);

  main(config).catch(err => {
    console.error('Cannot start the application.', err);
    process.exit(1);
  });
}
