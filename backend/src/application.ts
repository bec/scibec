import {AuthenticationComponent, registerAuthenticationStrategy} from '@loopback/authentication';
import {BootMixin} from '@loopback/boot';
import {ApplicationConfig, BindingKey, BindingScope, createBindingFromClass} from '@loopback/core';
import {
  RepositoryMixin,
  model,
  property,
  AnyObject,
  SchemaMigrationOptions,
} from '@loopback/repository';
import {ExpressRequestHandler, RestApplication, toInterceptor} from '@loopback/rest';
import {RestExplorerBindings, RestExplorerComponent} from '@loopback/rest-explorer';
import {ServiceMixin} from '@loopback/service-proxy';
import path from 'path';
import {OIDCAuthentication} from './authentication-strategies/oidc';
import {MySequence} from './sequence';
import {startWebsocket} from './utils/websocket';
import passport from 'passport';
import {JWTAuthenticationComponent, TokenServiceBindings} from '@loopback/authentication-jwt';
import {AuthorizationComponent} from '@loopback/authorization';
import {JWTService} from './services/jwt-service';
import {PasswordHasherBindings, UserServiceBindings} from './keys';
import {BcryptHasher} from './services/hash.password.bcryptjs';
import {SecuritySpecEnhancer} from './services/jwt-spec.enhancer';
import {MyUserService} from './services/user-service';
import * as crypto from 'crypto';
import {UserRepository} from './repositories';
import {ExpressRequestHandlersProvider} from './express-handlers/middleware-sequence';
import fs from 'fs';
import YAML = require('yaml');
import _ from 'lodash';
import {User} from './models';

export {ApplicationConfig};

/**
 * Information from package.json
 */
export interface PackageInfo {
  name: string;
  version: string;
  description: string;
}
export const PackageKey = BindingKey.create<PackageInfo>('application.package');
const pkg: PackageInfo = require('../package.json');

@model()
export class NewUser extends User {
  @property({
    type: 'string',
    required: true,
  })
  password: string;
}

export class ScibecApplication extends BootMixin(ServiceMixin(RepositoryMixin(RestApplication))) {
  constructor(options: ApplicationConfig = {}) {
    super(options);

    this.component(JWTAuthenticationComponent);
    this.component(AuthorizationComponent);
    this.component(AuthenticationComponent);
    this.setUpBindings();

    // Set up the custom sequence
    this.sequence(MySequence);

    // Set up default home page
    this.static('/', path.join(__dirname, '../public'));

    // Customize @loopback/rest-explorer configuration here
    this.configure(RestExplorerBindings.COMPONENT).to({
      path: '/explorer',
    });
    this.component(RestExplorerComponent);

    this.projectRoot = __dirname;
    // Customize @loopback/boot Booter Conventions here
    this.bootOptions = {
      controllers: {
        // Customize ControllerBooter Conventions here
        dirs: ['controllers'],
        extensions: ['.controller.js'],
        nested: true,
      },
    };
  }
  setUpBindings(): void {
    // Bind package.json to the application context
    this.bind(PackageKey).to(pkg);

    // // Bind bcrypt hash services
    this.bind(PasswordHasherBindings.ROUNDS).to(10);
    this.bind(PasswordHasherBindings.PASSWORD_HASHER).toClass(BcryptHasher);
    this.bind(TokenServiceBindings.TOKEN_SERVICE).toClass(JWTService);
    if (process.env.JWT_ACCESS_TOKEN_EXPIRES_IN)
      this.bind(TokenServiceBindings.TOKEN_EXPIRES_IN).to(process.env.JWT_ACCESS_TOKEN_EXPIRES_IN);
    this.bind(TokenServiceBindings.TOKEN_SECRET).to(
      (process.env.JWT_SECRET as string) ?? crypto.randomBytes(12).toString('hex'),
    );
    this.bind(UserServiceBindings.USER_SERVICE).toClass(MyUserService);

    this.add(createBindingFromClass(SecuritySpecEnhancer));

    // Bind datasource config
    this.configureDatasourceFromFile('../datasource.json', 'datasources.config.mongo');
    this.bind('middleware.sequence')
      .toProvider(ExpressRequestHandlersProvider)
      .inScope(BindingScope.SINGLETON);

    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    passport.serializeUser(function (user: any, done) {
      done(null, user);
    });
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    passport.deserializeUser(function (user: any, done) {
      done(null, user);
    });

    // LoopBack 4 style authentication strategies

    // Express style middleware interceptors
    if (this.options.oidcOptions) {
      this.bind('oidcOptions').to(this.options.oidcOptions);
      registerAuthenticationStrategy(this, OIDCAuthentication);
      this.add(createBindingFromClass(OIDCAuthentication));
      this.bind('passport-init-mw').to(
        toInterceptor(passport.initialize() as ExpressRequestHandler),
      );
      this.bind('passport-session-mw').to(toInterceptor(passport.session()));
      this.bind('passport-oidc').to(toInterceptor(passport.authenticate('openidconnect')));
    }
  }

  configureDatasourceFromFile(datasourceFile: string, datasourceConfigBindingKey: string): void {
    try {
      const mongoConfig: AnyObject = require(datasourceFile);
      this.bind(datasourceConfigBindingKey).to(mongoConfig);
    } catch {
      console.debug('missing datasource config file, applying defaults');
    }
  }

  // Unfortunately, TypeScript does not allow overriding methods inherited
  // from mapped types. https://github.com/microsoft/TypeScript/issues/38496
  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-ignore
  async start(): Promise<void> {
    // Use `databaseSeeding` flag to control if products/users should be pre
    // populated into the database. Its value is default to `true`.
    if (this.options.databaseSeeding !== false) {
      console.error('Warning: Database seeding enabled' + JSON.stringify(this.options, null, 3));
      await this.migrateSchema();
    }
    return super.start();
  }

  async startWebsocket(): Promise<void> {
    // return startWebsocket(this);
    return;
  }
  async migrateSchema(options?: SchemaMigrationOptions): Promise<void> {
    await super.migrateSchema(options);

    // Pre-populate products
    // TODO replace this by snippet data

    /*     const productRepo = await this.getRepository(ProductRepository);
        await productRepo.deleteAll();
        const productsDir = path.join(__dirname, '../fixtures/products');
        const productFiles = fs.readdirSync(productsDir);
    
        for (const file of productFiles) {
          if (file.endsWith('.yml')) {
            const productFile = path.join(productsDir, file);
            const yamlString = fs.readFileSync(productFile, 'utf8');
            const product = YAML.parse(yamlString);
            await productRepo.create(product);
          }
        } */

    // Pre-populate users
    const passwordHasher = await this.get(PasswordHasherBindings.PASSWORD_HASHER);
    const userRepo = await this.getRepository(UserRepository);
    await userRepo.deleteAll();
    const usersDir = path.join(__dirname, '../fixtures/users');
    const userFiles = fs.readdirSync(usersDir);

    for (const file of userFiles) {
      if (file.endsWith('.yml')) {
        const userFile = path.join(usersDir, file);
        const yamlString = YAML.parse(fs.readFileSync(userFile, 'utf8'));
        const input = new NewUser(yamlString);
        const password = await passwordHasher.hashPassword(input.password);
        input.password = password;
        const user = await userRepo.create(_.omit(input, 'password'));

        await userRepo.userCredentials(user.id).create({password});
      }
    }
  }
}
