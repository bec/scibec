import {ScibecApplication} from '../application';
import {MongoDataSource} from '../datasources';
import {AnyObject} from '@loopback/repository';

export interface WebsocketClient {
  // eslint-disable-next-line  @typescript-eslint/no-explicit-any
  ws?: any;
  // eslint-disable-next-line  @typescript-eslint/no-explicit-any
  user?: any;
  // eslint-disable-next-line  @typescript-eslint/no-explicit-any
  config?: any;
}
export interface WebsocketContainer {
  [index: string]: WebsocketClient[];
}

export async function startWebsocket(app: ScibecApplication) {
  const WebSocket = require('ws');
  const Mongo = require('mongodb');
  const websocketMap: WebsocketContainer = {};

  // console.log(app.restServer)
  const wss = new WebSocket.Server({server: app.restServer.httpServer?.server});

  // console.log(app.restServer)
  // eslint-disable-next-line  @typescript-eslint/no-explicit-any
  wss.on('connection', function connection(ws: any) {
    console.log('connected wss');

    // eslint-disable-next-line  @typescript-eslint/no-explicit-any
    ws.on('message', async (message: any) => {
      let msgContainer: any;
      try {
        msgContainer = JSON.parse(message);
        console.log('msgContainer: ', msgContainer);
      } catch (error) {
        console.log('Failed to parse websocket json message: ' + message.toString());
        console.log(error.message);
        return;
      }

      // eslint-disable-next-line  no-prototype-builtins
      if (!msgContainer.hasOwnProperty('message')) {
        return;
      }

      // eslint-disable-next-line  no-prototype-builtins
      if (!msgContainer['message'].hasOwnProperty('join')) {
        return;
      }

      // eslint-disable-next-line  no-prototype-builtins
      if (!msgContainer['message'].hasOwnProperty('token')) {
        return;
      }

      const clientID: string = msgContainer['message']['join'];
      console.log('clientID: ', clientID);
      // eslint-disable-next-line  @typescript-eslint/no-explicit-any
      const config: any = msgContainer['message']['config'];
      // eslint-disable-next-line  no-prototype-builtins

      if (websocketMap.hasOwnProperty(clientID)) {
        websocketMap[clientID].push({
          ws: ws,
          config: config,
        });
      } else {
        websocketMap[clientID] = [{ws: ws, config: config}]; //push({ws: ws, user: userProfileFromToken});
      }
      console.log('websocketmap:');
      console.log(websocketMap);

      ws.send(JSON.stringify({ping: 'ping'})); //works to send message back, what to send?
      // send update from mongodb when its updated, how to send from mongo to here?

      // console.log("ws", ws);
    });

    ws.isAlive = true;
    ws.on('pong', () => {
      heartbeat(ws);
    });

    // handle disconnection
    // eslint-disable-next-line  @typescript-eslint/no-explicit-any
    ws.on('close', function close(msg: any) {
      // console.log("Disconnected", msg);
      console.log('disconnected', websocketMap);
      for (const key in websocketMap) {
        websocketMap[key] = websocketMap[key].filter(client => {
          // console.log(client.ws.readyState)
          return client.ws.readyState === WebSocket.OPEN;
        });
        if (websocketMap[key].length === 0) {
          delete websocketMap[key];
        }
      }
      // console.log(websocketMap)
    });

    // eslint-disable-next-line  @typescript-eslint/no-explicit-any
    ws.on('error', (err: any) => {
      console.log(err);
    });
  });

  // eslint-disable-next-line  @typescript-eslint/no-explicit-any
  const heartbeat = (ws: any) => {
    ws.isAlive = true;
    // console.log("alive")
  };
  // eslint-disable-next-line  @typescript-eslint/no-explicit-any
  const ping = (ws: any) => {
    ws.send(JSON.stringify({ping: 'ping'}));
    // console.log('server: ping');
  };

  const getId = (function () {
    const findParentLogbook = (
      // eslint-disable-next-line  @typescript-eslint/no-explicit-any
      db: any,
      parentId: string,
      // eslint-disable-next-line  @typescript-eslint/no-explicit-any
      sendSocketMessage: any,
    ) => {
      const parentDoc = db.collection('Scan').findOne({_id: Mongo.ObjectId(parentId)});
      console.log(parentDoc);

      // eslint-disable-next-line  @typescript-eslint/no-explicit-any
      return parentDoc.then((document: any) => {
        if (document.snippetType === 'logbook') {
          sendSocketMessage(document._id);
          return document._id;
        } else if (document.parentId) {
          findParentLogbook(db, document.parentId, sendSocketMessage).then(
            // eslint-disable-next-line  @typescript-eslint/no-explicit-any
            (d: any) => {
              // handle case when last parent is not a a logbook (document is null)
              if (d) {
                return d._id;
              } else {
                return null;
              }
            },
          );
        } else {
          return null;
        }
      });
    };
    return {
      // eslint-disable-next-line  @typescript-eslint/no-explicit-any
      parentId: function (db: any, parentId: string, callback: any) {
        return findParentLogbook(db, parentId, callback);
      },
    };
  })();

  const dataSourceSettings: AnyObject = (app.getSync('datasources.mongo') as MongoDataSource)
    .settings;
  Mongo.MongoClient.connect(dataSourceSettings.url, {
    useUnifiedTopology: dataSourceSettings.useUnifiedTopology,
    // eslint-disable-next-line  @typescript-eslint/no-explicit-any
  }).then((client: any) => {
    const db = client.db(dataSourceSettings.database);
    const collection = db.collection('Scan');
    const changeStream = collection.watch(); //[{'$match': {'fullDocument.ownerGroup': 'p17301'}}]
    // eslint-disable-next-line  @typescript-eslint/no-explicit-any
    console.log('initiating changeStream');
    console.log('db: ', db);
    console.log('collection: ', collection);

    changeStream.on('change', function (change: any) {
      console.log('change: ', change);

      if (change.operationType !== 'delete') {
        console.log('its not delete');

        let id = 'clientID1';
        console.log(websocketMap[id]);
        if (websocketMap[id]) {
          websocketMap[id].forEach((c: any) => {
            c.ws.send(JSON.stringify({new_notification: change}));
          });
        }

        // getId.parentId(db, change.documentKey._id, async (id: string) => {
        //   console.log("get parent id");
        //   // let id = change.documentKey._id
        //   if (id != null) {
        //     // console.log("sending data to group ", id);
        //     console.log("change: ", change);

        //     // make sure all subscribers have the permission to read the changestream
        //     // const doc = await collection.findOne({
        //     //   _id: Mongo.ObjectId(change.documentKey._id),
        //     // });
        //     console.log(websocketMap[id])
        //     if (typeof websocketMap[id] != 'undefined') {
        //       // eslint-disable-next-line  @typescript-eslint/no-explicit-any
        //       websocketMap[id].forEach((c: any) => {
        //         // if (
        //         //   doc['readACL']?.some((r: string) => c.user.roles.includes(r))
        //         // ) {
        //         //   if (matchesFilterSettings(doc, c.config))
        //         c.ws.send(JSON.stringify({ 'new-notification': change }));
        //         // }
        //       });
        //     }
        //   }
        // });
      }
    });
    // eslint-disable-next-line  @typescript-eslint/no-explicit-any
    changeStream.on('error', (err: any) => {
      console.log('Error in ChangeStream:');
      console.log(err);
    });
  });
}

// eslint-disable-next-line  @typescript-eslint/no-explicit-any
function matchesFilterSettings(snippet: any, config: any): boolean {
  const acceptSnippet = true;
  if (typeof config.filter.tags != 'undefined') {
    // eslint-disable-next-line  @typescript-eslint/no-explicit-any
    config.filter.tags.forEach((tag: any) => {
      if (!snippet.tags.includes(tag)) {
        return false;
      }
    });
  }

  if (typeof config.filter.snippetType != 'undefined') {
    if (!config.filter.snippetType.includes(snippet.snippetType)) {
      return false;
    }
  }
  return acceptSnippet;
}
