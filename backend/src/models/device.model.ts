import {belongsTo, Entity, hasMany, model, property} from '@loopback/repository';
import {Session} from '.';
import {SciBecEntity} from './scibecentity.model';

export interface DeviceConfig {
  name?: string;
  label?: string;
}

enum ReadoutPriority {
  ON_REQUEST = 'on_request',
  BASELINE = 'baseline',
  MONITORED = 'monitored',
  ASYNC = 'async',
  CONTINUOUS = 'continuous',
}

enum FailureType {
  RAISE = 'raise',
  RETRY = 'retry',
  BUFFER = 'buffer',
}

@model()
export class Device extends SciBecEntity {
  @property({
    type: 'string',
    required: true,
  })
  name: string;

  @property({
    type: 'string',
    required: false,
  })
  description: string;

  @hasMany(() => Device, {keyTo: 'parentId'})
  subdevices?: Device[];

  @belongsTo(
    () => Device,
    {}, //relation metadata goes in here
    {
      // property definition goes in here
      mongodb: {dataType: 'ObjectId'},
    },
  )
  parentId?: string;

  @belongsTo(
    () => Session,
    {}, //relation metadata goes in here
    {
      // property definition goes in here
      description: 'Session to which this device belongs.',
      mongodb: {dataType: 'ObjectId'},
    },
  )
  sessionId?: string;

  @property({
    type: 'boolean',
    required: true,
    description: 'True if the device should be enabled.',
  })
  enabled: boolean;

  @property({
    type: 'boolean',
    required: false,
    description: 'True if the device is read-only.',
  })
  readOnly: boolean;

  @property({
    type: 'boolean',
    required: false,
    description: 'True if the device should receive a trigger call from BEC.',
  })
  softwareTrigger: boolean;

  @property({
    type: 'string',
    required: true,
    description: 'Ophyd device class',
  })
  deviceClass: string;

  @property.array(String, {
    description: 'User-defined tags for easier access and grouping.',
  })
  deviceTags?: string[];

  @property({
    type: 'object',
    required: true,
    description:
      'Device config, including the ophyd init arguments. Must at least contain name and label.',
  })
  deviceConfig: DeviceConfig;

  @property({
    type: 'string',
    jsonSchema: {
      enum: Object.values(ReadoutPriority),
    },
    required: true,
    description:
      'Readout priority. "on_request" will only read the device when requested. "baseline" will read the device once at the beginning of the session. "monitored" will read the device at every trigger from BEC. "async" will read the device asynchronously. "continuous" will read the device at its self-defined frequency, beyond a single scan.',
  })
  readoutPriority: ReadoutPriority;

  @property({
    type: 'string',
    jsonSchema: {
      enum: Object.values(FailureType),
    },
    description:
      'Defines how device failures are handled. "raise" raises an error immediately. "buffer" will try fall back to old values, should this not be possible, an error will be raised. "retry" will retry once before raising an error.',
    default: 'retry',
  })
  onFailure?: FailureType;

  @property({
    type: 'object',
    required: false,
    description: 'Additional fields for user settings such as in and out positions.',
  })
  userParameter: object;

  constructor(data?: Partial<Device>) {
    super(data);
  }
}

export interface DeviceRelations {
  // describe navigational properties here
}

export type DeviceWithRelations = Device & DeviceRelations;
