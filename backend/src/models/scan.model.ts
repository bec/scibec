import {belongsTo, hasMany, model, property} from '@loopback/repository';
import {
  Dataset,
  DatasetWithRelations,
  Session,
  SessionWithRelations,
  Experiment,
  ScanData,
  ScanDataWithRelations,
} from '.';
import {SciBecEntity} from './scibecentity.model';

@model()
export class Scan extends SciBecEntity {
  @property({
    type: 'string',
  })
  scanType: string;

  @property({
    type: 'object',
  })
  scanParameter: Object;

  @property({
    type: 'object',
  })
  userParameter: Object;

  @property({
    type: 'string',
  })
  scanId: string;

  @property({
    type: 'string',
  })
  requestId: string;

  @property({
    type: 'string',
  })
  queueId: string;

  @property({
    type: 'string',
  })
  exitStatus: string;

  @property({
    type: 'number',
  })
  scanNumber?: number;

  @property({
    type: 'object',
  })
  metadata: Object;

  @property({
    type: 'object',
  })
  files?: Object;

  @belongsTo(
    () => Session,
    {}, //relation metadata goes in here
    {
      // property definition goes in here
      mongodb: {dataType: 'ObjectId'},
    },
  )
  sessionId?: string;

  @belongsTo(
    () => Dataset,
    {}, //relation metadata goes in here
    {
      // property definition goes in here
      mongodb: {dataType: 'ObjectId'},
    },
  )
  datasetId?: string;

  @belongsTo(
    () => Experiment,
    {}, //relation metadata goes in here
    {
      mongodb: {dataType: 'ObjectId'},
    },
  )
  experimentId?: string;

  @hasMany(() => ScanData, {keyTo: 'scanId'})
  scanData?: ScanData[];

  constructor(data?: Partial<Scan>) {
    super(data);
  }
}

export interface ScanRelations {
  // describe navigational properties here
  session?: SessionWithRelations;
  datasets?: DatasetWithRelations[];
  scanData?: ScanDataWithRelations[];
}

export type ScanWithRelations = Scan & ScanRelations;
