import {belongsTo, hasMany, model, property} from '@loopback/repository';

import {NXEntity} from './nxentity.model';

@model()
export class NXEntityNested extends NXEntity {
  @property({
    type: 'array',
    itemType: NXEntityNested,
  })
  subentries?: NXEntityNested[];

  constructor(data?: Partial<NXEntityNested>) {
    super(data);
  }
}

export interface NXEntityNestedRelations {
  // describe navigational properties here
}

export type NXEntityNestedWithRelations = NXEntityNested & NXEntityNestedRelations;
