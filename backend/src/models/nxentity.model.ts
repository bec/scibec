import {belongsTo, hasMany, model, property} from '@loopback/repository';

import {SciBecEntity} from './scibecentity.model';
import {Scan} from './scan.model';

enum NXTypes {
  GROUP = 'group',
  DATASET = 'dataset',
  ATTRIBUTE = 'attribute',
}

@model()
export class NXEntity extends SciBecEntity {
  @property({
    type: 'string',
  })
  name: string;

  @property({
    type: 'string',
    jsonSchema: {
      enum: Object.values(NXTypes),
    },
  })
  type: string;

  @property({
    type: 'string',
  })
  description?: string;

  @property({
    type: 'any',
  })
  data?: any;

  @property({
    type: 'string',
  })
  NXclass: string;

  @hasMany(() => NXEntity, {keyTo: 'parentId'})
  subentries?: NXEntity[];

  @belongsTo(
    () => NXEntity,
    {}, //relation metadata goes in here
    {
      // property definition goes in here
      mongodb: {dataType: 'ObjectId'},
    },
  )
  parentId?: string;

  @belongsTo(
    () => Scan,
    {}, //relation metadata goes in here
    {
      // property definition goes in here
      description: 'The parent scan',
      mongodb: {dataType: 'ObjectId'},
    },
  )
  scanId?: string;

  constructor(data?: Partial<NXEntity>) {
    super(data);
  }
}

export interface NXEntityRelations {
  // describe navigational properties here
}

export type NXEntityWithRelations = NXEntity & NXEntityRelations;
