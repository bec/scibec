import {hasMany, model, property} from '@loopback/repository';
import {ExperimentAccount} from './experiment-account.model';
import {SciBecEntity} from './scibecentity.model';
import {Session} from './session.model';
import {Dataset, DatasetWithRelations} from './dataset.model';
import {Scan, ScanWithRelations} from './scan.model';

@model()
export class Experiment extends SciBecEntity {
  @property({
    type: 'string',
    required: true,
  })
  name: string;

  @hasMany(() => Dataset, {keyTo: 'experimentId'})
  datasets?: Dataset[];

  @hasMany(() => Scan, {keyTo: 'experimentId'})
  scans?: Scan[];

  @property({
    type: 'string',
    required: true,
  })
  beamlineId: string;

  @property({
    type: 'string',
  })
  writeAccount: string;

  @hasMany(() => ExperimentAccount, {keyTo: 'experimentId'})
  experimentAccounts?: ExperimentAccount[];

  @property({
    type: 'string',
  })
  userId?: string;

  @property({
    type: 'string',
  })
  logbook?: string;

  @property({
    type: 'array',
    itemType: 'string',
  })
  samples?: string[];

  @property({
    type: 'object',
  })
  experimentConfig?: object;

  @property({
    type: 'object',
  })
  experimentInfo?: object;

  @property({
    type: 'string',
  })
  activeSession?: string;

  @hasMany(() => Session, {keyTo: 'experimentId'})
  sessions?: Session[];

  constructor(data?: Partial<Experiment>) {
    super(data);
  }
}

export interface ExperimentRelations {
  // describe navigational properties here
  datasets?: DatasetWithRelations[];
  scans?: ScanWithRelations[];
}

export type ExperimentWithRelations = Experiment & ExperimentRelations;
