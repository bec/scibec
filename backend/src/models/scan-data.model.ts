import {belongsTo, model, property} from '@loopback/repository';
import {Device} from './device.model';
import {Scan} from './scan.model';
import {SciBecEntity} from './scibecentity.model';

@model({
  settings: {
    strict: false,
  },
})
export class ScanData extends SciBecEntity {
  @property({
    type: 'string',
  })
  name?: string;

  @property({
    type: 'Object',
  })
  data?: Object;

  @property({
    type: 'string',
  })
  filePath?: string;

  @belongsTo(
    () => Scan,
    {}, //relation metadata goes in here
    {
      // property definition goes in here
      description: 'The parent scan',
      mongodb: {dataType: 'ObjectId'},
    },
  )
  scanId?: string;

  constructor(data?: Partial<ScanData>) {
    super(data);
  }
}

export interface ScanDataRelations {
  // describe navigational properties here
}

export type ScanDataWithRelations = ScanData & ScanDataRelations;
