import {inject} from '@loopback/core';
import {MongoDataSource} from '../datasources';
import {ScanData, ScanDataRelations} from '../models';
import {AutoAddRepository} from './autoadd.repository';

export class ScanDataRepository extends AutoAddRepository<
  ScanData,
  typeof ScanData.prototype.id,
  ScanDataRelations
> {
  constructor(@inject('datasources.mongo') dataSource: MongoDataSource) {
    super(ScanData, dataSource);
  }
}
