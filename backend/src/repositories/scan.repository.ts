import {Getter, inject} from '@loopback/core';
import {BelongsToAccessor, HasManyRepositoryFactory, repository} from '@loopback/repository';
import {MongoDataSource} from '../datasources';
import {Scan, ScanRelations, Session, ScanData, Experiment, Dataset} from '../models';
import {AutoAddRepository} from './autoadd.repository';
import {ScanDataRepository} from './scan-data.repository';
import {SessionRepository} from './session.repository';
import {ExperimentRepository} from './experiment.repository';
import {DatasetRepository} from './dataset.repository';

export class ScanRepository extends AutoAddRepository<
  Scan,
  typeof Scan.prototype.id,
  ScanRelations
> {
  public readonly session: BelongsToAccessor<Session, any>;
  public readonly scanData: HasManyRepositoryFactory<ScanData, string>;
  public readonly experiments: BelongsToAccessor<Experiment, any>;
  public readonly datasets: BelongsToAccessor<Dataset, any>;

  constructor(
    @inject('datasources.mongo') dataSource: MongoDataSource,
    @repository.getter('SessionRepository')
    sessionRepositoryGetter: Getter<SessionRepository>,
    @repository.getter('ScanDataRepository')
    scanDataRepositoryGetter: Getter<ScanDataRepository>,
    @repository.getter('ExperimentRepository')
    experimentRepositoryGetter: Getter<ExperimentRepository>,
    @repository.getter('DatasetRepository')
    datasetRepositoryGetter: Getter<DatasetRepository>,
  ) {
    super(Scan, dataSource);
    this.session = this.createBelongsToAccessorFor('session', sessionRepositoryGetter);
    this.scanData = this.createHasManyRepositoryFactoryFor('scanData', scanDataRepositoryGetter);
    this.experiments = this.createBelongsToAccessorFor('experiment', experimentRepositoryGetter);

    this.datasets = this.createBelongsToAccessorFor('dataset', datasetRepositoryGetter);

    // add these lines to register inclusion resolver.
    this.registerInclusionResolver('session', this.session.inclusionResolver);
    this.registerInclusionResolver('scanData', this.scanData.inclusionResolver);
    this.registerInclusionResolver('experiment', this.experiments.inclusionResolver);
    this.registerInclusionResolver('dataset', this.datasets.inclusionResolver);
  }
}
