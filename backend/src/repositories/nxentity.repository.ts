import {Getter, inject} from '@loopback/core';
import {MongoDataSource} from '../datasources';
import {AutoAddRepository} from './autoadd.repository';
import {NXEntity, NXEntityRelations} from '../models/nxentity.model';
import {BelongsToAccessor, HasManyRepositoryFactory, repository} from '@loopback/repository';
import {Scan} from '../models';
import {ScanRepository} from './scan.repository';

export class NXEntityRepository extends AutoAddRepository<
  NXEntity,
  typeof NXEntity.prototype.id,
  NXEntityRelations
> {
  public readonly subentries: HasManyRepositoryFactory<NXEntity, any>;
  public readonly parent: BelongsToAccessor<NXEntity, any>;
  public readonly scan: BelongsToAccessor<Scan, any>;

  constructor(
    @inject('datasources.mongo') dataSource: MongoDataSource,
    @repository.getter('NXEntityRepository')
    nxentityRepositoryGetter: Getter<NXEntityRepository>,
    @repository.getter('ScanRepository')
    scanRepositoryGetter: Getter<ScanRepository>,
  ) {
    super(NXEntity, dataSource);
    this.subentries = this.createHasManyRepositoryFactoryFor(
      'subentries',
      nxentityRepositoryGetter,
    );
    this.parent = this.createBelongsToAccessorFor('parent', nxentityRepositoryGetter);
    this.scan = this.createBelongsToAccessorFor('scan', scanRepositoryGetter);
    this.registerInclusionResolver('subentries', this.subentries.inclusionResolver);
    this.registerInclusionResolver('scan', this.scan.inclusionResolver);
    this.registerInclusionResolver('parent', this.parent.inclusionResolver);
  }
}
