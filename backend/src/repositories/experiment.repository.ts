import {Getter, inject} from '@loopback/core';
import {HasManyRepositoryFactory, repository} from '@loopback/repository';
import {MongoDataSource} from '../datasources';
import {
  Experiment,
  ExperimentAccount,
  ExperimentRelations,
  Session,
  Dataset,
  Scan,
} from '../models';
import {AutoAddRepository} from './autoadd.repository';
import {ExperimentAccountRepository} from './experiment-account.repository';
import {SessionRepository} from './session.repository';
import {DatasetRepository} from './dataset.repository';
import {ScanRepository} from './scan.repository';

export class ExperimentRepository extends AutoAddRepository<
  Experiment,
  typeof Experiment.prototype.id,
  ExperimentRelations
> {
  public readonly sessions: HasManyRepositoryFactory<Session, string>;
  public readonly experimentAccounts: HasManyRepositoryFactory<
    ExperimentAccount,
    string
  >;
  public readonly datasets: HasManyRepositoryFactory<Dataset, string>;
  public readonly scans: HasManyRepositoryFactory<Scan, string>;

  constructor(
    @inject('datasources.mongo') dataSource: MongoDataSource,
    @repository.getter('SessionRepository')
    sessionRepositoryGetter: Getter<SessionRepository>,
    @repository.getter('ExperimentAccountRepository')
    experimentAccountRepositoryGetter: Getter<ExperimentAccountRepository>,
    @repository.getter('DatasetRepository')
    datasetRepositoryGetter: Getter<DatasetRepository>,
    @repository.getter('ScanRepository')
    scanRepositoryGetter: Getter<ScanRepository>,
  ) {
    super(Experiment, dataSource);
    this.sessions = this.createHasManyRepositoryFactoryFor(
      'sessions',
      sessionRepositoryGetter,
    );
    this.experimentAccounts = this.createHasManyRepositoryFactoryFor(
      'experimentAccounts',
      experimentAccountRepositoryGetter,
    );
    this.datasets = this.createHasManyRepositoryFactoryFor(
      'datasets',
      datasetRepositoryGetter,
    );
    this.scans = this.createHasManyRepositoryFactoryFor(
      'scans',
      scanRepositoryGetter,
    );

    // add these lines to register inclusion resolver.
    this.registerInclusionResolver('sessions', this.sessions.inclusionResolver);
    this.registerInclusionResolver(
      'experimentAccounts',
      this.experimentAccounts.inclusionResolver,
    );
    this.registerInclusionResolver('datasets', this.datasets.inclusionResolver);
    this.registerInclusionResolver('scans', this.scans.inclusionResolver);
  }
}
