import {DefaultCrudRepository, Entity, juggler, Model} from '@loopback/repository';
import {HttpErrors} from '@loopback/rest';

export class AutoAddRepository<
  T extends Entity,
  ID,
  Relations extends object = {},
> extends DefaultCrudRepository<T, ID, Relations> {
  constructor(
    entityClass: typeof Entity & {
      prototype: T;
    },
    dataSource: juggler.DataSource,
  ) {
    super(entityClass, dataSource);
  }

  definePersistedModel(entityClass: typeof Model) {
    const modelClass = super.definePersistedModel(entityClass);
    modelClass.observe('before save', async ctx => {
      let currentUser: any;
      if (!ctx?.options.hasOwnProperty('currentUser')) {
        throw new Error('Unexpected user context: Current user cannot be retrieved.');
      } else {
        currentUser = ctx.options.currentUser;
      }

      if (currentUser?.roles.includes('beamline-readonly')) {
        throw new HttpErrors[405]('This user is limited to read operations.');
      }
      // console.log(`going to save ${ctx.Model.modelName} ${ctx}`);
      // PATCH case
      if (ctx.data) {
        // console.error("PATCH case")
        ctx.data.updatedAt = new Date();
        ctx.data.updatedBy = currentUser?.email ?? 'unknown user';
        delete ctx.data.createdAt;
        delete ctx.data.createdBy;
        let groups = [...ctx?.options?.currentUser?.roles];
        var groupCondition = {
          or: [{owner: {inq: groups}}, {writeACL: {inq: groups}}],
        };
        ctx.where = {
          and: [ctx.where, groupCondition],
        };
        // remove the writeACL and owner fields from the data if they are not in the groups
        if (!groups.includes('admin')) {
          if (ctx.data.hasOwnProperty('writeACL')) {
            ctx.data.writeACL = ctx.data.writeACL.filter((value: string) => groups.includes(value));
            if (ctx.data.writeACL.length === 0) {
              delete ctx.data.writeACL;
            }
          }
          if (ctx.data.hasOwnProperty('owner')) {
            ctx.data.owner = ctx.data.owner.filter((value: string) => groups.includes(value));
            if (ctx.data.owner.length === 0) {
              delete ctx.data.owner;
            }
          }
        }
        console.log('query:', JSON.stringify(ctx.where, null, 3));
      } else {
        if (ctx.isNewInstance) {
          // POST case
          // console.error("POST case")
          ctx.instance.createdAt = new Date();
          ctx.instance.createdBy = currentUser?.email ?? 'unknown user';

          // remove the writeACL and owner fields from the data if they are not in the groups and the user is not admin
          let groups = [...ctx?.options?.currentUser?.roles];
          if (!groups.includes('admin')) {
            if (ctx.instance.hasOwnProperty('writeACL')) {
              ctx.instance.writeACL = ctx.instance.writeACL.filter((value: string) =>
                groups.includes(value),
              );
              if (ctx.instance.writeACL.length === 0) {
                delete ctx.instance.writeACL;
              }
            }
            if (ctx.instance.hasOwnProperty('owner')) {
              ctx.instance.owner = ctx.instance.owner.filter((value: string) =>
                groups.includes(value),
              );
              if (ctx.instance.owner.length === 0) {
                delete ctx.instance.owner;
              }
            }
          }
          ctx.instance.readACL = ctx.instance.readACL ?? [currentUser?.email];
          ctx.instance.writeACL = ctx.instance.writeACL ?? [currentUser?.email];
          ctx.instance.owner = ctx.instance.owner ?? [currentUser?.email];
        } else {
          // PUT case
          // console.error("PUT case")
          // TODO restore auto generated fields, which would otherwise be lost
          ctx.instance.unsetAttribute('id');
        }
        // POST and PUT case
        ctx.instance.updatedAt = new Date();
        ctx.instance.updatedBy = currentUser?.email ?? 'unknown user';
      }
      //console.error("going to save:" + JSON.stringify(ctx, null, 3))
    });

    modelClass.observe('access', async ctx => {
      let currentUser: any;
      if (!ctx?.options.hasOwnProperty('currentUser')) {
        throw new Error('Unexpected user context: Current user cannot be retrieved.');
      } else {
        currentUser = ctx.options.currentUser;
      }

      // console.log('roles:', currentUser?.roles);
      // console.log('access case:', JSON.stringify(ctx, null, 3));
      let groups = [...ctx?.options?.currentUser?.roles];
      if (!groups.includes('admin')) {
        // exclude ingestor users from find and patch operations
        // if (currentUser?.roles.includes('ingestor') && ctx.query) {
        //   throw new HttpErrors[405](
        //     'ingestor users are not allowed to access this model',
        //   );
        // }
        var groupCondition = {
          or: [{owner: {inq: groups}}, {readACL: {inq: groups}}],
        };
        if (!ctx.query.where) {
          ctx.query.where = groupCondition;
        } else {
          ctx.query.where = {
            and: [ctx.query.where, groupCondition],
          };
        }
      }
      console.log('query:', JSON.stringify(ctx.query, null, 3));
    });
    return modelClass;
  }
}
