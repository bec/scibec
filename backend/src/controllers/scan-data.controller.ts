import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {post, param, get, getModelSchemaRef, patch, put, del, requestBody} from '@loopback/rest';
import {ScanData} from '../models';
import {ScanDataRepository} from '../repositories';
import {authenticate} from '@loopback/authentication';
import {authorize} from '@loopback/authorization';
import {basicAuthorization} from '../services/basic.authorizor';
import {SecurityBindings, UserProfile} from '@loopback/security';
import {inject} from '@loopback/core';
import {OPERATION_SECURITY_SPEC} from '../utils/security-spec';
import _ from 'lodash';

@authenticate('jwt')
@authorize({
  allowedRoles: ['any-authenticated-user'],
  voters: [basicAuthorization],
})
export class ScanDataController {
  constructor(
    @inject(SecurityBindings.USER) private user: UserProfile,
    @repository(ScanDataRepository)
    public scanDataRepository: ScanDataRepository,
  ) {}

  @post('/scan-data', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'ScanData model instance',
        content: {'application/json': {schema: getModelSchemaRef(ScanData)}},
      },
    },
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(ScanData, {
            title: 'NewScanData',
            exclude: ['id'],
          }),
        },
      },
    })
    scanData: Omit<ScanData, 'id'>,
  ): Promise<ScanData> {
    return this.scanDataRepository.create(scanData, {currentUser: this.user});
  }

  @post('/scan-data/many', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'ScanData model instance',
        content: {
          'application/json': {
            schema: {
              type: 'object',
              title: 'NewScanDataMany',
              properties: {
                count: {
                  type: 'number',
                },
              },
            },
          },
        },
      },
    },
  })
  async createMany(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(ScanData, {
            title: 'NewScanData',
            exclude: ['id'],
          }),
        },
      },
    })
    scanData: Omit<ScanData, 'id'>,
  ): Promise<any> {
    let count = 0;
    for (const key in scanData.data) {
      const element: Object = scanData.data[key as keyof Object];
      const newScanData = new ScanData();
      newScanData.scanId = scanData.scanId;
      newScanData.readACL = scanData.readACL;
      newScanData.writeACL = scanData.writeACL;
      newScanData.owner = scanData.owner;
      newScanData.name = key;
      newScanData.data = element;
      newScanData.filePath = scanData.filePath;
      await this.scanDataRepository.create(newScanData, {
        currentUser: this.user,
      });
      count++;
    }

    return Promise.resolve({count: count});
  }

  @get('/scan-data/count', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'ScanData model count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async count(@param.where(ScanData) where?: Where<ScanData>): Promise<Count> {
    return this.scanDataRepository.count(where, {currentUser: this.user});
  }

  @get('/scan-data', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'Array of ScanData model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(ScanData, {includeRelations: true}),
            },
          },
        },
      },
    },
  })
  async find(@param.filter(ScanData) filter?: Filter<ScanData>): Promise<ScanData[]> {
    return this.scanDataRepository.find(filter, {currentUser: this.user});
  }

  @patch('/scan-data', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'ScanData PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(ScanData, {partial: true}),
        },
      },
    })
    scanData: ScanData,
    @param.where(ScanData) where?: Where<ScanData>,
  ): Promise<Count> {
    return this.scanDataRepository.updateAll(scanData, where, {
      currentUser: this.user,
    });
  }

  @get('/scan-data/{id}', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'ScanData model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(ScanData, {includeRelations: true}),
          },
        },
      },
    },
  })
  async findById(
    @param.path.string('id') id: string,
    @param.filter(ScanData, {exclude: 'where'})
    filter?: FilterExcludingWhere<ScanData>,
  ): Promise<ScanData> {
    return this.scanDataRepository.findById(id, filter, {
      currentUser: this.user,
    });
  }

  @patch('/scan-data/{id}', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '204': {
        description: 'ScanData PATCH success',
      },
    },
  })
  async updateById(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(ScanData, {partial: true}),
        },
      },
    })
    scanData: ScanData,
  ): Promise<void> {
    await this.scanDataRepository.updateById(id, scanData, {
      currentUser: this.user,
    });
  }

  @put('/scan-data/{id}', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '204': {
        description: 'ScanData PUT success',
      },
    },
  })
  async replaceById(
    @param.path.string('id') id: string,
    @requestBody() scanData: ScanData,
  ): Promise<void> {
    await this.scanDataRepository.replaceById(id, scanData, {
      currentUser: this.user,
    });
  }

  @del('/scan-data/{id}', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '204': {
        description: 'ScanData DELETE success',
      },
    },
  })
  async deleteById(@param.path.string('id') id: string): Promise<void> {
    await this.scanDataRepository.deleteById(id, {currentUser: this.user});
  }
}
