import {inject} from '@loopback/core';
import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  del,
  get,
  getModelSchemaRef,
  param,
  patch,
  post,
  requestBody,
  response,
} from '@loopback/rest';
import {Scan} from '../models';
import {ScanRepository} from '../repositories';
import {authenticate} from '@loopback/authentication';
import {authorize} from '@loopback/authorization';
import {basicAuthorization} from '../services/basic.authorizor';
import {SecurityBindings, UserProfile} from '@loopback/security';
import {OPERATION_SECURITY_SPEC} from '../utils/security-spec';

@authenticate('jwt')
@authorize({
  allowedRoles: ['any-authenticated-user'],
  voters: [basicAuthorization],
})
export class ScanController {
  constructor(
    @inject(SecurityBindings.USER) private user: UserProfile,
    @repository(ScanRepository)
    public scanRepository: ScanRepository,
  ) {}

  @post('/scans', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'Scan model instance',
        content: {'application/json': {schema: getModelSchemaRef(Scan)}},
      },
    },
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Scan, {
            title: 'NewScan',
            exclude: ['id'],
          }),
        },
      },
    })
    scan: Omit<Scan, 'id'>,
  ): Promise<Scan> {
    return this.scanRepository.create(scan, {currentUser: this.user});
  }

  @get('/scans/count', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'Scan model count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async count(@param.where(Scan) where?: Where<Scan>): Promise<Count> {
    return this.scanRepository.count(where, {currentUser: this.user});
  }

  @get('/scans', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'Array of Scan model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(Scan, {includeRelations: true}),
            },
          },
        },
      },
    },
  })
  async find(@param.filter(Scan) filter?: Filter<Scan>): Promise<Scan[]> {
    return this.scanRepository.find(filter, {currentUser: this.user});
  }

  @patch('/scans', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'Scan PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Scan, {partial: true}),
        },
      },
    })
    scan: Scan,
    @param.where(Scan) where?: Where<Scan>,
  ): Promise<Count> {
    return this.scanRepository.updateAll(scan, where, {currentUser: this.user});
  }

  @get('/scans/{id}', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'Scan model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(Scan, {includeRelations: true}),
          },
        },
      },
    },
  })
  async findById(
    @param.path.string('id') id: string,
    @param.filter(Scan, {exclude: 'where'}) filter?: FilterExcludingWhere<Scan>,
  ): Promise<Scan> {
    return this.scanRepository.findById(id, filter, {currentUser: this.user});
  }

  @patch('/scans/{id}', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '204': {
        description: 'Scan PATCH success',
      },
    },
  })
  async updateById(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Scan, {partial: true}),
        },
      },
    })
    scan: Scan,
  ): Promise<void> {
    await this.scanRepository.updateById(id, scan, {currentUser: this.user});
  }

  @del('/scans/{id}', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '204': {
        description: 'Scan DELETE success',
      },
    },
  })
  async deleteById(@param.path.string('id') id: string): Promise<void> {
    await this.scanRepository.deleteById(id, {currentUser: this.user});
  }
}
