import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {post, param, get, getModelSchemaRef, patch, del, requestBody} from '@loopback/rest';
import {Dataset} from '../models';
import {DatasetRepository} from '../repositories';
import {authenticate} from '@loopback/authentication';
import {authorize} from '@loopback/authorization';
import {basicAuthorization} from '../services/basic.authorizor';
import {SecurityBindings, UserProfile} from '@loopback/security';
import {inject} from '@loopback/core';
import {OPERATION_SECURITY_SPEC} from '../utils/security-spec';

@authenticate('jwt')
@authorize({
  allowedRoles: ['any-authenticated-user'],
  voters: [basicAuthorization],
})
export class DatasetController {
  constructor(
    @inject(SecurityBindings.USER) private user: UserProfile,
    @repository(DatasetRepository)
    public datasetRepository: DatasetRepository,
  ) {}

  @post('/datasets', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'Dataset model instance',
        content: {'application/json': {schema: getModelSchemaRef(Dataset)}},
      },
    },
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Dataset, {
            title: 'NewDataset',
            exclude: ['id'],
          }),
        },
      },
    })
    dataset: Omit<Dataset, 'id'>,
  ): Promise<Dataset> {
    return this.datasetRepository.create(dataset, {currentUser: this.user});
  }

  @get('/datasets/count', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'Dataset model count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async count(@param.where(Dataset) where?: Where<Dataset>): Promise<Count> {
    return this.datasetRepository.count(where, {currentUser: this.user});
  }

  @get('/datasets', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'Array of Dataset model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(Dataset, {includeRelations: true}),
            },
          },
        },
      },
    },
  })
  async find(@param.filter(Dataset) filter?: Filter<Dataset>): Promise<Dataset[]> {
    return this.datasetRepository.find(filter, {currentUser: this.user});
  }

  @patch('/datasets', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'Dataset PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Dataset, {partial: true}),
        },
      },
    })
    dataset: Dataset,
    @param.where(Dataset) where?: Where<Dataset>,
  ): Promise<Count> {
    return this.datasetRepository.updateAll(dataset, where, {
      currentUser: this.user,
    });
  }

  @get('/datasets/{id}', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'Dataset model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(Dataset, {includeRelations: true}),
          },
        },
      },
    },
  })
  async findById(
    @param.path.string('id') id: string,
    @param.filter(Dataset, {exclude: 'where'})
    filter?: FilterExcludingWhere<Dataset>,
  ): Promise<Dataset> {
    return this.datasetRepository.findById(id, filter, {
      currentUser: this.user,
    });
  }

  @patch('/datasets/{id}', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '204': {
        description: 'Dataset PATCH success',
      },
    },
  })
  async updateById(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Dataset, {partial: true}),
        },
      },
    })
    dataset: Dataset,
  ): Promise<void> {
    await this.datasetRepository.updateById(id, dataset, {
      currentUser: this.user,
    });
  }

  @del('/datasets/{id}', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '204': {
        description: 'Dataset DELETE success',
      },
    },
  })
  async deleteById(@param.path.string('id') id: string): Promise<void> {
    await this.datasetRepository.deleteById(id, {currentUser: this.user});
  }
}
