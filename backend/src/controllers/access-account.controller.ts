import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {post, param, get, getModelSchemaRef, patch, del, requestBody} from '@loopback/rest';
import {AccessAccount} from '../models';
import {AccessAccountRepository} from '../repositories';
import {authenticate} from '@loopback/authentication';
import {authorize} from '@loopback/authorization';
import {basicAuthorization} from '../services/basic.authorizor';
import {SecurityBindings, UserProfile} from '@loopback/security';
import {inject} from '@loopback/core';
import {OPERATION_SECURITY_SPEC} from '../utils/security-spec';

@authenticate('jwt')
@authorize({
  allowedRoles: ['any-authenticated-user'],
  voters: [basicAuthorization],
})
export class AccessAccountController {
  constructor(
    @inject(SecurityBindings.USER) private user: UserProfile,
    @repository(AccessAccountRepository)
    public accessAccountRepository: AccessAccountRepository,
  ) {}

  @post('/access-accounts', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'AccessAccount model instance',
        content: {
          'application/json': {schema: getModelSchemaRef(AccessAccount)},
        },
      },
    },
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(AccessAccount, {
            title: 'NewAccessAccount',
            exclude: ['id'],
          }),
        },
      },
    })
    accessAccount: Omit<AccessAccount, 'id'>,
  ): Promise<AccessAccount> {
    return this.accessAccountRepository.create(accessAccount, {
      currentUser: this.user,
    });
  }

  @get('/access-accounts/count', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'AccessAccount model count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async count(@param.where(AccessAccount) where?: Where<AccessAccount>): Promise<Count> {
    return this.accessAccountRepository.count(where, {currentUser: this.user});
  }

  @get('/access-accounts', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'Array of AccessAccount model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(AccessAccount, {includeRelations: true}),
            },
          },
        },
      },
    },
  })
  async find(
    @param.filter(AccessAccount) filter?: Filter<AccessAccount>,
  ): Promise<AccessAccount[]> {
    return this.accessAccountRepository.find(filter, {currentUser: this.user});
  }

  @patch('/access-accounts', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'AccessAccount PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(AccessAccount, {partial: true}),
        },
      },
    })
    accessAccount: AccessAccount,
    @param.where(AccessAccount) where?: Where<AccessAccount>,
  ): Promise<Count> {
    return this.accessAccountRepository.updateAll(accessAccount, where, {
      currentUser: this.user,
    });
  }

  @get('/access-accounts/{id}', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'AccessAccount model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(AccessAccount, {includeRelations: true}),
          },
        },
      },
    },
  })
  async findById(
    @param.path.string('id') id: string,
    @param.filter(AccessAccount, {exclude: 'where'})
    filter?: FilterExcludingWhere<AccessAccount>,
  ): Promise<AccessAccount> {
    return this.accessAccountRepository.findById(id, filter, {
      currentUser: this.user,
    });
  }

  @patch('/access-accounts/{id}', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '204': {
        description: 'AccessAccount PATCH success',
      },
    },
  })
  async updateById(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(AccessAccount, {partial: true}),
        },
      },
    })
    accessAccount: AccessAccount,
  ): Promise<void> {
    await this.accessAccountRepository.updateById(id, accessAccount, {
      currentUser: this.user,
    });
  }

  @del('/access-accounts/{id}', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '204': {
        description: 'AccessAccount DELETE success',
      },
    },
  })
  async deleteById(@param.path.string('id') id: string): Promise<void> {
    await this.accessAccountRepository.deleteById(id, {currentUser: this.user});
  }
}
