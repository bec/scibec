import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getModelSchemaRef,
  patch,
  del,
  requestBody,
  HttpErrors,
} from '@loopback/rest';
import {AccessConfig} from '../models';
import {AccessConfigRepository} from '../repositories';
import {authenticate} from '@loopback/authentication';
import {authorize} from '@loopback/authorization';
import {basicAuthorization} from '../services/basic.authorizor';
import {SecurityBindings, UserProfile} from '@loopback/security';
import {inject} from '@loopback/core';
import {OPERATION_SECURITY_SPEC} from '../utils/security-spec';

@authenticate('jwt')
@authorize({
  allowedRoles: ['any-authenticated-user'],
  voters: [basicAuthorization],
})
export class AccessConfigController {
  constructor(
    @inject(SecurityBindings.USER) private user: UserProfile,
    @repository(AccessConfigRepository)
    public accessConfigRepository: AccessConfigRepository,
  ) {}

  @post('/access-configs', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'AccessConfig model instance',
        content: {
          'application/json': {schema: getModelSchemaRef(AccessConfig)},
        },
      },
    },
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(AccessConfig, {
            title: 'NewAccessConfig',
            exclude: ['id'],
          }),
        },
      },
    })
    accessConfig: Omit<AccessConfig, 'id'>,
  ): Promise<AccessConfig> {
    if (accessConfig.beamlineId) {
      let existingData = await this.accessConfigRepository.findOne({
        where: {beamlineId: accessConfig.beamlineId},
      });
      if (existingData) {
        throw new HttpErrors.Conflict('The beamline has an access config already defined.');
      }
    }

    return this.accessConfigRepository.create(accessConfig, {
      currentUser: this.user,
    });
  }

  @get('/access-configs/count', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'AccessConfig model count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async count(@param.where(AccessConfig) where?: Where<AccessConfig>): Promise<Count> {
    return this.accessConfigRepository.count(where, {currentUser: this.user});
  }

  @get('/access-configs', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'Array of AccessConfig model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(AccessConfig, {includeRelations: true}),
            },
          },
        },
      },
    },
  })
  async find(@param.filter(AccessConfig) filter?: Filter<AccessConfig>): Promise<AccessConfig[]> {
    return this.accessConfigRepository.find(filter);
  }

  @patch('/access-configs', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'AccessConfig PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(AccessConfig, {partial: true}),
        },
      },
    })
    accessConfig: AccessConfig,
    @param.where(AccessConfig) where?: Where<AccessConfig>,
  ): Promise<Count> {
    return this.accessConfigRepository.updateAll(accessConfig, where, {
      currentUser: this.user,
    });
  }

  @get('/access-configs/{id}', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'AccessConfig model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(AccessConfig, {includeRelations: true}),
          },
        },
      },
    },
  })
  async findById(
    @param.path.string('id') id: string,
    @param.filter(AccessConfig, {exclude: 'where'})
    filter?: FilterExcludingWhere<AccessConfig>,
  ): Promise<AccessConfig> {
    return this.accessConfigRepository.findById(id, filter, {
      currentUser: this.user,
    });
  }

  @patch('/access-configs/{id}', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '204': {
        description: 'AccessConfig PATCH success',
      },
    },
  })
  async updateById(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(AccessConfig, {partial: true}),
        },
      },
    })
    accessConfig: AccessConfig,
  ): Promise<void> {
    await this.accessConfigRepository.updateById(id, accessConfig, {
      currentUser: this.user,
    });
  }

  @del('/access-configs/{id}', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '204': {
        description: 'AccessConfig DELETE success',
      },
    },
  })
  async deleteById(@param.path.string('id') id: string): Promise<void> {
    await this.accessConfigRepository.deleteById(id, {currentUser: this.user});
  }
}
