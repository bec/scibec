import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {post, param, get, getModelSchemaRef, patch, del, requestBody} from '@loopback/rest';
import {Device} from '../models';
import {DeviceRepository} from '../repositories';
import {authenticate} from '@loopback/authentication';
import {authorize} from '@loopback/authorization';
import {basicAuthorization} from '../services/basic.authorizor';
import {SecurityBindings, UserProfile} from '@loopback/security';
import {inject} from '@loopback/core';
import {OPERATION_SECURITY_SPEC} from '../utils/security-spec';

@authenticate('jwt')
@authorize({
  allowedRoles: ['any-authenticated-user'],
  voters: [basicAuthorization],
})
export class DeviceController {
  constructor(
    @inject(SecurityBindings.USER) private user: UserProfile,
    @repository(DeviceRepository)
    public deviceRepository: DeviceRepository,
  ) {}

  @post('/devices', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'Device model instance',
        content: {'application/json': {schema: getModelSchemaRef(Device)}},
      },
    },
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Device, {
            title: 'NewDevice',
            exclude: ['id'],
          }),
        },
      },
    })
    device: Omit<Device, 'id'>,
  ): Promise<Device> {
    return this.deviceRepository.create(device, {currentUser: this.user});
  }

  @get('/devices/count', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'Device model count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async count(@param.where(Device) where?: Where<Device>): Promise<Count> {
    return this.deviceRepository.count(where, {currentUser: this.user});
  }

  @get('/devices', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'Array of Device model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(Device, {includeRelations: true}),
            },
          },
        },
      },
    },
  })
  async find(@param.filter(Device) filter?: Filter<Device>): Promise<Device[]> {
    return this.deviceRepository.find(filter, {currentUser: this.user});
  }

  @patch('/devices', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'Device PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Device, {partial: true}),
        },
      },
    })
    device: Device,
    @param.where(Device) where?: Where<Device>,
  ): Promise<Count> {
    return this.deviceRepository.updateAll(device, where, {
      currentUser: this.user,
    });
  }

  @get('/devices/{id}', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'Device model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(Device, {includeRelations: true}),
          },
        },
      },
    },
  })
  async findById(
    @param.path.string('id') id: string,
    @param.filter(Device, {exclude: 'where'})
    filter?: FilterExcludingWhere<Device>,
  ): Promise<Device> {
    return this.deviceRepository.findById(id, filter, {currentUser: this.user});
  }

  @patch('/devices/{id}', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '204': {
        description: 'Device PATCH success',
      },
    },
  })
  async updateById(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Device, {partial: true}),
        },
      },
    })
    device: Device,
  ): Promise<void> {
    await this.deviceRepository.updateById(id, device, {
      currentUser: this.user,
    });
  }

  @del('/devices/{id}', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '204': {
        description: 'Device DELETE success',
      },
    },
  })
  async deleteById(@param.path.string('id') id: string): Promise<void> {
    await this.deviceRepository.deleteById(id, {currentUser: this.user});
  }
}
