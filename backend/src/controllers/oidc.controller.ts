import {get, RestBindings, Response, param, RequestWithSession} from '@loopback/rest';
import {authenticate, AuthenticationBindings, TokenService} from '@loopback/authentication';
import {inject, intercept} from '@loopback/core';
import {mapProfile, OIDCOptions} from '../authentication-strategies/types';
import {TokenServiceBindings, User} from '@loopback/authentication-jwt';
import {JWTService} from '../services/jwt-service';

/**
 * Login controller for third party oauth provider
 *
 * The method loginToThirdParty uses the @authenticate decorator to plugin passport strategies independently
 * The method thirdPartyCallBack uses the passport strategies as express middleware
 */
export class OIDCController {
  constructor(
    @inject(TokenServiceBindings.TOKEN_SERVICE)
    public jwtService: TokenService,
  ) {}

  @authenticate('oidc')
  @get('/auth/login')
  /**
   * This method uses the @authenticate decorator to plugin passport strategies independently
   *
   * Endpoint: '/auth/thirdparty'
   *          an endpoint for api clients to login via a third party app, redirects to third party app
   */
  loginToThirdParty(
    @inject(AuthenticationBindings.AUTHENTICATION_REDIRECT_URL)
    redirectUrl: string,
    @inject(AuthenticationBindings.AUTHENTICATION_REDIRECT_STATUS)
    status: number,
    @inject(RestBindings.Http.RESPONSE)
    response: Response,
  ) {
    response.statusCode = status || 302; //redirected to keycloak
    response.setHeader('Location', redirectUrl);
    response.end();
    return response;
  }

  @intercept('passport-oidc')
  @get('/auth/callback') //then comes here...
  /**
   * This method uses the passport strategies as express middleware
   *
   * Endpoint: '/auth/callback'
   *          an endpoint which serves as a oauth2 callback for the thirdparty app
   *          this endpoint sets the user profile in the session
   */
  async thirdPartyCallBack(
    @inject(RestBindings.Http.REQUEST) request: RequestWithSession,
    @inject('oidcOptions') oidcOptions: OIDCOptions,
    @inject(RestBindings.Http.RESPONSE) response: Response,
  ) {
    if (!request.user) throw new Error('user not found from request');
    const userProfile = mapProfile(request.user as User);

    // create a JSON Web Token based on the user profile
    const token = await this.jwtService.generateToken(userProfile);
    console.log('TOKEN:', token);

    response.redirect(`${oidcOptions.successRedirect}?token=${token}`); // redirect back to home browser localhost:4200
    return response;
  }

  // @authenticate('jwt')
  @get('/auth/logout')
  async logout(
    @inject(RestBindings.Http.REQUEST) request: RequestWithSession,
    @inject(RestBindings.Http.RESPONSE) response: Response,
    @inject('oidcOptions') oidcOptions: OIDCOptions,
  ) {
    await request.session.destroy();
    response.clearCookie('id_token');
    response.clearCookie('connect.sid');
    response.redirect(
      `${oidcOptions.endSessionEndpoint}?post_logout_redirect_uri=${oidcOptions.postLogoutRedirectUri}&client_id=${oidcOptions.clientID}`,
    );
    return response;
  }
}
