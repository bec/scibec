import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {post, param, get, getModelSchemaRef, patch, del, requestBody} from '@loopback/rest';
import {ExperimentAccount} from '../models';
import {ExperimentAccountRepository} from '../repositories';
import {authenticate} from '@loopback/authentication';
import {authorize} from '@loopback/authorization';
import {basicAuthorization} from '../services/basic.authorizor';
import {SecurityBindings, UserProfile} from '@loopback/security';
import {inject} from '@loopback/core';
import {OPERATION_SECURITY_SPEC} from '../utils/security-spec';

@authenticate('jwt')
@authorize({
  allowedRoles: ['any-authenticated-user'],
  voters: [basicAuthorization],
})
export class ExperimentAccountController {
  constructor(
    @inject(SecurityBindings.USER) private user: UserProfile,
    @repository(ExperimentAccountRepository)
    public experimentAccountRepository: ExperimentAccountRepository,
  ) {}

  @post('/experiment-accounts', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'ExperimentAccount model instance',
        content: {
          'application/json': {schema: getModelSchemaRef(ExperimentAccount)},
        },
      },
    },
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(ExperimentAccount, {
            title: 'NewExperimentAccount',
            exclude: ['id'],
          }),
        },
      },
    })
    experimentAccount: Omit<ExperimentAccount, 'id'>,
  ): Promise<ExperimentAccount> {
    return this.experimentAccountRepository.create(experimentAccount, {
      currentUser: this.user,
    });
  }

  @get('/experiment-accounts/count', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'ExperimentAccount model count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async count(@param.where(ExperimentAccount) where?: Where<ExperimentAccount>): Promise<Count> {
    return this.experimentAccountRepository.count(where, {
      currentUser: this.user,
    });
  }

  @get('/experiment-accounts', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'Array of ExperimentAccount model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(ExperimentAccount, {
                includeRelations: true,
              }),
            },
          },
        },
      },
    },
  })
  async find(
    @param.filter(ExperimentAccount) filter?: Filter<ExperimentAccount>,
  ): Promise<ExperimentAccount[]> {
    return this.experimentAccountRepository.find(filter, {
      currentUser: this.user,
    });
  }

  @patch('/experiment-accounts', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'ExperimentAccount PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(ExperimentAccount, {partial: true}),
        },
      },
    })
    experimentAccount: ExperimentAccount,
    @param.where(ExperimentAccount) where?: Where<ExperimentAccount>,
  ): Promise<Count> {
    return this.experimentAccountRepository.updateAll(experimentAccount, where, {
      currentUser: this.user,
    });
  }

  @get('/experiment-accounts/{id}', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'ExperimentAccount model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(ExperimentAccount, {
              includeRelations: true,
            }),
          },
        },
      },
    },
  })
  async findById(
    @param.path.string('id') id: string,
    @param.filter(ExperimentAccount, {exclude: 'where'})
    filter?: FilterExcludingWhere<ExperimentAccount>,
  ): Promise<ExperimentAccount> {
    return this.experimentAccountRepository.findById(id, filter, {
      currentUser: this.user,
    });
  }

  @patch('/experiment-accounts/{id}', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '204': {
        description: 'ExperimentAccount PATCH success',
      },
    },
  })
  async updateById(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(ExperimentAccount, {partial: true}),
        },
      },
    })
    experimentAccount: ExperimentAccount,
  ): Promise<void> {
    await this.experimentAccountRepository.updateById(id, experimentAccount, {
      currentUser: this.user,
    });
  }

  @del('/experiment-accounts/{id}', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '204': {
        description: 'ExperimentAccount DELETE success',
      },
    },
  })
  async deleteById(@param.path.string('id') id: string): Promise<void> {
    await this.experimentAccountRepository.deleteById(id, {
      currentUser: this.user,
    });
  }
}
