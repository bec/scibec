import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {post, param, get, getModelSchemaRef, patch, del, requestBody} from '@loopback/rest';
import {FunctionalAccount} from '../models';
import {FunctionalAccountRepository} from '../repositories';
import {authenticate} from '@loopback/authentication';
import {authorize} from '@loopback/authorization';
import {basicAuthorization} from '../services/basic.authorizor';
import {SecurityBindings, UserProfile} from '@loopback/security';
import {inject} from '@loopback/core';
import {OPERATION_SECURITY_SPEC} from '../utils/security-spec';

@authenticate('jwt')
@authorize({
  allowedRoles: ['any-authenticated-user'],
  voters: [basicAuthorization],
})
export class FunctionalAccountController {
  constructor(
    @inject(SecurityBindings.USER) private user: UserProfile,
    @repository(FunctionalAccountRepository)
    public functionalAccountRepository: FunctionalAccountRepository,
  ) {}

  @post('/functional-accounts', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'FunctionalAccount model instance',
        content: {
          'application/json': {schema: getModelSchemaRef(FunctionalAccount)},
        },
      },
    },
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(FunctionalAccount, {
            title: 'NewFunctionalAccount',
            exclude: ['id'],
          }),
        },
      },
    })
    functionalAccount: Omit<FunctionalAccount, 'id'>,
  ): Promise<FunctionalAccount> {
    return this.functionalAccountRepository.create(functionalAccount, {
      currentUser: this.user,
    });
  }

  @get('/functional-accounts/count', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'FunctionalAccount model count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async count(@param.where(FunctionalAccount) where?: Where<FunctionalAccount>): Promise<Count> {
    return this.functionalAccountRepository.count(where, {
      currentUser: this.user,
    });
  }

  @get('/functional-accounts', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'Array of FunctionalAccount model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(FunctionalAccount, {
                includeRelations: true,
              }),
            },
          },
        },
      },
    },
  })
  async find(
    @param.filter(FunctionalAccount) filter?: Filter<FunctionalAccount>,
  ): Promise<FunctionalAccount[]> {
    return this.functionalAccountRepository.find(filter, {
      currentUser: this.user,
    });
  }

  @patch('/functional-accounts', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'FunctionalAccount PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(FunctionalAccount, {partial: true}),
        },
      },
    })
    functionalAccount: FunctionalAccount,
    @param.where(FunctionalAccount) where?: Where<FunctionalAccount>,
  ): Promise<Count> {
    return this.functionalAccountRepository.updateAll(functionalAccount, where, {
      currentUser: this.user,
    });
  }

  @get('/functional-accounts/{id}', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'FunctionalAccount model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(FunctionalAccount, {
              includeRelations: true,
            }),
          },
        },
      },
    },
  })
  async findById(
    @param.path.string('id') id: string,
    @param.filter(FunctionalAccount, {exclude: 'where'})
    filter?: FilterExcludingWhere<FunctionalAccount>,
  ): Promise<FunctionalAccount> {
    return this.functionalAccountRepository.findById(id, filter, {
      currentUser: this.user,
    });
  }

  @patch('/functional-accounts/{id}', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '204': {
        description: 'FunctionalAccount PATCH success',
      },
    },
  })
  async updateById(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(FunctionalAccount, {partial: true}),
        },
      },
    })
    functionalAccount: FunctionalAccount,
  ): Promise<void> {
    await this.functionalAccountRepository.updateById(id, functionalAccount, {
      currentUser: this.user,
    });
  }

  @del('/functional-accounts/{id}', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '204': {
        description: 'FunctionalAccount DELETE success',
      },
    },
  })
  async deleteById(@param.path.string('id') id: string): Promise<void> {
    await this.functionalAccountRepository.deleteById(id, {
      currentUser: this.user,
    });
  }
}
