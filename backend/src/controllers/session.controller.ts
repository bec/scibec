import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {post, param, get, getModelSchemaRef, patch, del, requestBody} from '@loopback/rest';
import {Session} from '../models';
import {SessionRepository} from '../repositories';
import {authenticate} from '@loopback/authentication';
import {authorize} from '@loopback/authorization';
import {basicAuthorization} from '../services/basic.authorizor';
import {SecurityBindings, UserProfile} from '@loopback/security';
import {inject} from '@loopback/core';
import {OPERATION_SECURITY_SPEC} from '../utils/security-spec';

@authenticate('jwt')
@authorize({
  allowedRoles: ['any-authenticated-user'],
  voters: [basicAuthorization],
})
export class SessionController {
  constructor(
    @inject(SecurityBindings.USER) private user: UserProfile,
    @repository(SessionRepository)
    public sessionRepository: SessionRepository,
  ) {}

  @post('/sessions', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'Session model instance',
        content: {'application/json': {schema: getModelSchemaRef(Session)}},
      },
    },
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Session, {
            title: 'NewSession',
            exclude: ['id'],
          }),
        },
      },
    })
    session: Omit<Session, 'id'>,
  ): Promise<Session> {
    return this.sessionRepository.create(session, {
      currentUser: this.user,
    });
  }

  @get('/sessions/count', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'Session model count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async count(@param.where(Session) where?: Where<Session>): Promise<Count> {
    return this.sessionRepository.count(where, {
      currentUser: this.user,
    });
  }

  @get('/sessions', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'Array of Session model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(Session, {includeRelations: true}),
            },
          },
        },
      },
    },
  })
  async find(@param.filter(Session) filter?: Filter<Session>): Promise<Session[]> {
    return this.sessionRepository.find(filter, {
      currentUser: this.user,
    });
  }

  @patch('/sessions', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'Session PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Session, {partial: true}),
        },
      },
    })
    session: Session,
    @param.where(Session) where?: Where<Session>,
  ): Promise<Count> {
    return this.sessionRepository.updateAll(session, where, {
      currentUser: this.user,
    });
  }

  @get('/sessions/{id}', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'Session model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(Session, {includeRelations: true}),
          },
        },
      },
    },
  })
  async findById(
    @param.path.string('id') id: string,
    @param.filter(Session, {exclude: 'where'})
    filter?: FilterExcludingWhere<Session>,
  ): Promise<Session> {
    return this.sessionRepository.findById(id, filter, {
      currentUser: this.user,
    });
  }

  @patch('/sessions/{id}', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '204': {
        description: 'Session PATCH success',
      },
    },
  })
  async updateById(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Session, {partial: true}),
        },
      },
    })
    session: Session,
  ): Promise<void> {
    await this.sessionRepository.updateById(id, session, {
      currentUser: this.user,
    });
  }

  @del('/sessions/{id}', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '204': {
        description: 'Session DELETE success',
      },
    },
  })
  async deleteById(@param.path.string('id') id: string): Promise<void> {
    await this.sessionRepository.deleteById(id, {
      currentUser: this.user,
    });
  }
}
