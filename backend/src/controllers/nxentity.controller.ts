import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {post, param, get, getModelSchemaRef, patch, put, del, requestBody} from '@loopback/rest';
import {NXEntity} from '../models';
import {NXEntityRepository} from '../repositories';
import {authenticate} from '@loopback/authentication';
import {authorize} from '@loopback/authorization';
import {basicAuthorization} from '../services/basic.authorizor';
import {SecurityBindings, UserProfile} from '@loopback/security';
import {inject} from '@loopback/core';
import {OPERATION_SECURITY_SPEC} from '../utils/security-spec';
import {NXEntityNested} from '../models/nxentity-nested.model';
import _ from 'lodash';

@authenticate('jwt')
@authorize({
  allowedRoles: ['any-authenticated-user'],
  voters: [basicAuthorization],
})
export class NXEntityController {
  constructor(
    @inject(SecurityBindings.USER) private user: UserProfile,
    @repository(NXEntityRepository)
    public NXEntityRepository: NXEntityRepository,
  ) {}

  @post('/nxentities', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'NXEntity model instance',
        content: {'application/json': {schema: getModelSchemaRef(NXEntity)}},
      },
    },
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(NXEntity, {
            title: 'NewNXEntity',
            exclude: ['id'],
          }),
        },
      },
    })
    NXEntity: Omit<NXEntity, 'id'>,
  ): Promise<NXEntity> {
    return this.NXEntityRepository.create(NXEntity, {currentUser: this.user});
  }

  @post('/nxentities/entry', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'NXEntityNested model instance',
        content: {
          'application/json': {schema: getModelSchemaRef(NXEntityNested)},
        },
      },
    },
  })
  async createEntry(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(NXEntityNested, {
            title: 'NewNXEntityNested',
            exclude: ['id'],
          }),
        },
      },
    })
    NXEntityNested: Omit<NXEntityNested, 'id'>,
  ): Promise<NXEntityNested> {
    return this.createEntryRecursive(NXEntityNested);
  }

  async createEntryRecursive(NXEntityNested: Omit<NXEntityNested, 'id'>): Promise<NXEntity> {
    // recursively create the entry
    // first create the parent
    let parentData = _.omit(NXEntityNested, 'subentries');
    let parent = await this.NXEntityRepository.create(parentData, {
      currentUser: this.user,
    });

    // then create the children
    if (NXEntityNested.subentries) {
      let children = await Promise.all(
        NXEntityNested.subentries.map(child => {
          child.parentId = parent.id;
          return this.createEntryRecursive(child);
        }),
      );
    }

    return Promise.resolve(parent);
  }

  @get('/nxentities/count', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'NXEntity model count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async count(@param.where(NXEntity) where?: Where<NXEntity>): Promise<Count> {
    return this.NXEntityRepository.count(where, {currentUser: this.user});
  }

  @get('/nxentities', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'Array of NXEntity model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(NXEntity, {includeRelations: true}),
            },
          },
        },
      },
    },
  })
  async find(@param.filter(NXEntity) filter?: Filter<NXEntity>): Promise<NXEntity[]> {
    return this.NXEntityRepository.find(filter, {currentUser: this.user});
  }

  @patch('/nxentities', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'NXEntity PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(NXEntity, {partial: true}),
        },
      },
    })
    NXEntity: NXEntity,
    @param.where(NXEntity) where?: Where<NXEntity>,
  ): Promise<Count> {
    return this.NXEntityRepository.updateAll(NXEntity, where, {
      currentUser: this.user,
    });
  }

  @get('/nxentities/{id}', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'NXEntity model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(NXEntity, {includeRelations: true}),
          },
        },
      },
    },
  })
  async findById(
    @param.path.string('id') id: string,
    @param.filter(NXEntity, {exclude: 'where'})
    filter?: FilterExcludingWhere<NXEntity>,
  ): Promise<NXEntity> {
    return this.NXEntityRepository.findById(id, filter, {
      currentUser: this.user,
    });
  }

  @patch('/nxentities/{id}', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '204': {
        description: 'NXEntity PATCH success',
      },
    },
  })
  async updateById(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(NXEntity, {partial: true}),
        },
      },
    })
    NXEntity: NXEntity,
  ): Promise<void> {
    await this.NXEntityRepository.updateById(id, NXEntity, {
      currentUser: this.user,
    });
  }

  @put('/nxentities/{id}', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '204': {
        description: 'NXEntity PUT success',
      },
    },
  })
  async replaceById(
    @param.path.string('id') id: string,
    @requestBody() NXEntity: NXEntity,
  ): Promise<void> {
    await this.NXEntityRepository.replaceById(id, NXEntity, {
      currentUser: this.user,
    });
  }

  @del('/nxentities/{id}', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '204': {
        description: 'NXEntity DELETE success',
      },
    },
  })
  async deleteById(@param.path.string('id') id: string): Promise<void> {
    await this.NXEntityRepository.deleteById(id, {currentUser: this.user});
  }
}
