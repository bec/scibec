import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {post, param, get, getModelSchemaRef, patch, del, requestBody} from '@loopback/rest';
import {Experiment} from '../models';
import {ExperimentRepository} from '../repositories';
import {authenticate} from '@loopback/authentication';
import {authorize} from '@loopback/authorization';
import {basicAuthorization} from '../services/basic.authorizor';
import {SecurityBindings, UserProfile} from '@loopback/security';
import {inject} from '@loopback/core';
import {OPERATION_SECURITY_SPEC} from '../utils/security-spec';

@authenticate('jwt')
@authorize({
  allowedRoles: ['any-authenticated-user'],
  voters: [basicAuthorization],
})
export class ExperimentController {
  constructor(
    @inject(SecurityBindings.USER) private user: UserProfile,
    @repository(ExperimentRepository)
    public experimentRepository: ExperimentRepository,
  ) {}

  @authorize({
    allowedRoles: ['admin', 'experiment-ingestor'],
    voters: [basicAuthorization],
  })
  @post('/experiments', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'Experiment model instance',
        content: {'application/json': {schema: getModelSchemaRef(Experiment)}},
      },
    },
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Experiment, {
            title: 'NewExperiment',
            exclude: ['id'],
          }),
        },
      },
    })
    experiment: Omit<Experiment, 'id'>,
  ): Promise<Experiment> {
    return this.experimentRepository.create(experiment, {
      currentUser: this.user,
    });
  }

  @get('/experiments/count', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'Experiment model count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async count(@param.where(Experiment) where?: Where<Experiment>): Promise<Count> {
    return this.experimentRepository.count(where, {currentUser: this.user});
  }

  @get('/experiments', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'Array of Experiment model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(Experiment, {includeRelations: true}),
            },
          },
        },
      },
    },
  })
  async find(@param.filter(Experiment) filter?: Filter<Experiment>): Promise<Experiment[]> {
    return this.experimentRepository.find(filter, {currentUser: this.user});
  }

  @patch('/experiments', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'Experiment PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Experiment, {partial: true}),
        },
      },
    })
    experiment: Experiment,
    @param.where(Experiment) where?: Where<Experiment>,
  ): Promise<Count> {
    return this.experimentRepository.updateAll(experiment, where, {
      currentUser: this.user,
    });
  }

  @get('/experiments/{id}', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'Experiment model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(Experiment, {includeRelations: true}),
          },
        },
      },
    },
  })
  async findById(
    @param.path.string('id') id: string,
    @param.filter(Experiment, {exclude: 'where'})
    filter?: FilterExcludingWhere<Experiment>,
  ): Promise<Experiment> {
    return this.experimentRepository.findById(id, filter, {
      currentUser: this.user,
    });
  }

  @patch('/experiments/{id}', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '204': {
        description: 'Experiment PATCH success',
      },
    },
  })
  async updateById(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Experiment, {partial: true}),
        },
      },
    })
    experiment: Experiment,
  ): Promise<void> {
    await this.experimentRepository.updateById(id, experiment, {
      currentUser: this.user,
    });
  }

  @del('/experiments/{id}', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '204': {
        description: 'Experiment DELETE success',
      },
    },
  })
  async deleteById(@param.path.string('id') id: string): Promise<void> {
    await this.experimentRepository.deleteById(id, {currentUser: this.user});
  }
}
