# [1.15.0](https://gitlab.psi.ch/bec/scibec/compare/v1.14.6...v1.15.0) (2024-07-26)


### Features

* add py_scibec_client base on http client ([4043162](https://gitlab.psi.ch/bec/scibec/commit/40431622bc4f5d1ead06a4357db7417311c39260))

## [1.14.6](https://gitlab.psi.ch/bec/scibec/compare/v1.14.5...v1.14.6) (2024-05-13)


### Bug Fixes

* **backend:** fixed inclusion resolver for experiments ([22029ce](https://gitlab.psi.ch/bec/scibec/commit/22029ceedc4c79f8824f5693245767f39e2d0312))

## [1.14.5](https://gitlab.psi.ch/bec/scibec/compare/v1.14.4...v1.14.5) (2024-05-02)


### Bug Fixes

* updated to new openapi client syntax ([b860716](https://gitlab.psi.ch/bec/scibec/commit/b860716fb90f549bf0afdfa482a3a29e3de879a6))

## [1.14.4](https://gitlab.psi.ch/bec/scibec/compare/v1.14.3...v1.14.4) (2024-04-26)


### Bug Fixes

* updates for new openapi client ([2b9956c](https://gitlab.psi.ch/bec/scibec/commit/2b9956c79611a442c87807387e64b0565dd2dc00))

## [1.14.3](https://gitlab.psi.ch/bec/scibec/compare/v1.14.2...v1.14.3) (2024-04-11)


### Bug Fixes

* **sdk:** fixed login with email and password ([40ac476](https://gitlab.psi.ch/bec/scibec/commit/40ac4766604b8daeceda04a6c5802cf73a1834f9))

## [1.14.2](https://gitlab.psi.ch/bec/scibec/compare/v1.14.1...v1.14.2) (2024-01-23)


### Bug Fixes

* reverted openapi generator to 6.6.0 ([856a9ac](https://gitlab.psi.ch/bec/scibec/commit/856a9ac581a5c703a0e4849459c777b07893f34f))

## [1.14.1](https://gitlab.psi.ch/bec/scibec/compare/v1.14.0...v1.14.1) (2024-01-23)

### Bug Fixes

- fixed py_scibec_openapi_client generator to a specific number ([0da02c3](https://gitlab.psi.ch/bec/scibec/commit/0da02c3bf390cc1dbee65ad0135ed3e98732b560))

# [1.14.0](https://gitlab.psi.ch/bec/scibec/compare/v1.13.1...v1.14.0) (2024-01-23)

### Features

- add softwareTrigger to deviceConfig ([b7601c3](https://gitlab.psi.ch/bec/scibec/commit/b7601c358a08aa3390f41634c908cc11db3bf5a7))

## [1.13.1](https://gitlab.psi.ch/bec/scibec/compare/v1.13.0...v1.13.1) (2024-01-18)

### Bug Fixes

- fixed import for exceptions ([6d1b367](https://gitlab.psi.ch/bec/scibec/commit/6d1b36756093138ce062bfed8b035844b39f14c2))

# [1.13.0](https://gitlab.psi.ch/bec/scibec/compare/v1.12.1...v1.13.0) (2024-01-18)

### Features

- added option to retrieve a new token for a different user without affecting the current session ([36d52b4](https://gitlab.psi.ch/bec/scibec/commit/36d52b4a2455ac5f036fa082beb20a8ddeaa8741))

## [1.12.1](https://gitlab.psi.ch/bec/scibec/compare/v1.12.0...v1.12.1) (2024-01-18)

### Bug Fixes

- renamed digestor group to beamline-readonly ([ab1fcf2](https://gitlab.psi.ch/bec/scibec/commit/ab1fcf27892b7022858ab1e6e19125ea127f9ecb))

# [1.12.0](https://gitlab.psi.ch/bec/scibec/compare/v1.11.1...v1.12.0) (2024-01-17)

### Bug Fixes

- added missing filePath to scan data ([6f6d82c](https://gitlab.psi.ch/bec/scibec/commit/6f6d82cfe9508c90b8590a469078fae2ba7f3cc7))

### Features

- added support for readonly accounts ([4fa388e](https://gitlab.psi.ch/bec/scibec/commit/4fa388e40fe764b30034c8659f72b37c58ef5a23))

## [1.11.1](https://gitlab.psi.ch/bec/scibec/compare/v1.11.0...v1.11.1) (2024-01-17)

### Bug Fixes

- fixed scan data update many ([325223a](https://gitlab.psi.ch/bec/scibec/commit/325223a3dc5425aba0ac57f839290d12bd6aa389))

# [1.11.0](https://gitlab.psi.ch/bec/scibec/compare/v1.10.2...v1.11.0) (2024-01-17)

### Features

- added createMany endpoint for scan data ([f63b6dc](https://gitlab.psi.ch/bec/scibec/commit/f63b6dc47b755503b977e86373883cf878ab9f42))

## [1.10.2](https://gitlab.psi.ch/bec/scibec/compare/v1.10.1...v1.10.2) (2024-01-17)

### Bug Fixes

- reverted to more general requirements ([963b30a](https://gitlab.psi.ch/bec/scibec/commit/963b30a1c749ce1702478e93d8a377fefe0971dd))

## [1.10.1](https://gitlab.psi.ch/bec/scibec/compare/v1.10.0...v1.10.1) (2024-01-17)

### Bug Fixes

- added missing endpoints to bec high-level interface ([5b88371](https://gitlab.psi.ch/bec/scibec/commit/5b8837103570560494dab8dc0f570036711d7f6c))

# [1.10.0](https://gitlab.psi.ch/bec/scibec/compare/v1.9.0...v1.10.0) (2024-01-17)

### Bug Fixes

- added missing files ([3e0d1ca](https://gitlab.psi.ch/bec/scibec/commit/3e0d1ca80834350012b1e84bbe4362355dcddad5))

### Features

- updated py scibec ([a8c17d0](https://gitlab.psi.ch/bec/scibec/commit/a8c17d0248739de2192fb78acf14f49c32d1d78a))

# [1.9.0](https://gitlab.psi.ch/bec/scibec/compare/v1.8.6...v1.9.0) (2024-01-17)

### Features

- added scan data as event replacement ([912c73e](https://gitlab.psi.ch/bec/scibec/commit/912c73ecb48f4113542aadcceda95057ecc3ee2a))

## [1.8.6](https://gitlab.psi.ch/bec/scibec/compare/v1.8.5...v1.8.6) (2024-01-16)

### Bug Fixes

- added model imports to scibec core ([4097c7f](https://gitlab.psi.ch/bec/scibec/commit/4097c7fddb41f01ec4800d43b7e72765ed6778fa))

## [1.8.5](https://gitlab.psi.ch/bec/scibec/compare/v1.8.4...v1.8.5) (2024-01-16)

### Bug Fixes

- added model imports ([cd96117](https://gitlab.psi.ch/bec/scibec/commit/cd961173fcea9cdef87694c8de1d8d9e10d5f37b))

## [1.8.4](https://gitlab.psi.ch/bec/scibec/compare/v1.8.3...v1.8.4) (2024-01-16)

### Bug Fixes

- fixed typing depencency ([75efc96](https://gitlab.psi.ch/bec/scibec/commit/75efc9692a2d47fcbcd288663d198a7db3d2f941))

## [1.8.3](https://gitlab.psi.ch/bec/scibec/compare/v1.8.2...v1.8.3) (2024-01-16)

### Bug Fixes

- fixed typing depencency ([b16fcc0](https://gitlab.psi.ch/bec/scibec/commit/b16fcc0d54f112a6554fcb299f6661080dd77fb0))

## [1.8.2](https://gitlab.psi.ch/bec/scibec/compare/v1.8.1...v1.8.2) (2023-12-08)

### Bug Fixes

- removed keycloak access for now ([7171d75](https://gitlab.psi.ch/bec/scibec/commit/7171d752c4f223b2359210fd1bc3f32d43a613ea))

## [1.8.1](https://gitlab.psi.ch/bec/scibec/compare/v1.8.0...v1.8.1) (2023-12-01)

### Bug Fixes

- updated openapi_client to new backend ([f1ce42f](https://gitlab.psi.ch/bec/scibec/commit/f1ce42fbb974f068ed0cde6d4241ca42a773ac38))

# [1.8.0](https://gitlab.psi.ch/bec/scibec/compare/v1.7.5...v1.8.0) (2023-12-01)

### Features

- simplified device config ([3a80772](https://gitlab.psi.ch/bec/scibec/commit/3a807725bbda64f8a5c0d6f3288b6014954faab7))

## [1.7.5](https://gitlab.psi.ch/bec/scibec/compare/v1.7.4...v1.7.5) (2023-09-22)

### Bug Fixes

- fixed openapi client dependency ([256b01c](https://gitlab.psi.ch/bec/scibec/commit/256b01cc709157e8e3d2e70b33da3badd57599b4))

## [1.7.4](https://gitlab.psi.ch/bec/scibec/compare/v1.7.3...v1.7.4) (2023-09-12)

### Bug Fixes

- fixed setup file for new python structure ([6109885](https://gitlab.psi.ch/bec/scibec/commit/6109885d950428c03d12f3e837c7daf246a75e10))

## [1.7.3](https://gitlab.psi.ch/bec/scibec/compare/v1.7.2...v1.7.3) (2023-09-12)

### Bug Fixes

- fixed version extraction script ([4a7322f](https://gitlab.psi.ch/bec/scibec/commit/4a7322fae8e310e23befd419fadfaf8f830d2dfd))

## [1.7.2](https://gitlab.psi.ch/bec/scibec/compare/v1.7.1...v1.7.2) (2023-09-12)

### Bug Fixes

- fixed python client ([08635f7](https://gitlab.psi.ch/bec/scibec/commit/08635f7ca3f227ffc9ad56efb9face6f5c5eecaf))

## [1.7.1](https://gitlab.psi.ch/bec/scibec/compare/v1.7.0...v1.7.1) (2023-09-12)

### Bug Fixes

- fixed package name ([d88d2e8](https://gitlab.psi.ch/bec/scibec/commit/d88d2e84daa01218496ac116516d18ed0c56aa8e))

# [1.7.0](https://gitlab.psi.ch/bec/scibec/compare/v1.6.8...v1.7.0) (2023-09-12)

### Features

- added nexus entries ([bc3bbf1](https://gitlab.psi.ch/bec/scibec/commit/bc3bbf18e12eebb5ac21fd19848fb5768733ead2))

## [1.6.8](https://gitlab.psi.ch/bec/scibec/compare/v1.6.7...v1.6.8) (2023-09-09)

### Bug Fixes

- testing ci ([b26e472](https://gitlab.psi.ch/bec/scibec/commit/b26e472ac910e896f86eb23e627c62510e2d118b))

## [1.6.7](https://gitlab.psi.ch/bec/scibec/compare/v1.6.6...v1.6.7) (2023-09-09)

### Bug Fixes

- testing ci ([c3076e4](https://gitlab.psi.ch/bec/scibec/commit/c3076e4c3524cf8c1673b284c53f36200521a3b5))

## [1.6.6](https://gitlab.psi.ch/bec/scibec/compare/v1.6.5...v1.6.6) (2023-09-09)

### Bug Fixes

- testing ci ([0426f08](https://gitlab.psi.ch/bec/scibec/commit/0426f08b50650cf3216da1629ad76f9e383b5003))

## [1.6.5](https://gitlab.psi.ch/bec/scibec/compare/v1.6.4...v1.6.5) (2023-09-09)

### Bug Fixes

- testing ci ([2818894](https://gitlab.psi.ch/bec/scibec/commit/2818894d787d9880b3f7f5f725e8e769003a98ee))

## [1.6.4](https://gitlab.psi.ch/bec/scibec/compare/v1.6.3...v1.6.4) (2023-09-09)

### Bug Fixes

- testing ci ([6a34c9c](https://gitlab.psi.ch/bec/scibec/commit/6a34c9cf5e4b7b22e11f8410ce0ff94955c5cf07))

## [1.6.3](https://gitlab.psi.ch/bec/scibec/compare/v1.6.2...v1.6.3) (2023-09-09)

### Bug Fixes

- fixed user context ([dc291d7](https://gitlab.psi.ch/bec/scibec/commit/dc291d7492aa386d239736c4cc19e8b5e91d2fba))

## [1.6.2](https://gitlab.psi.ch/bec/scibec/compare/v1.6.1...v1.6.2) (2023-09-09)

### Bug Fixes

- updated python sdk ([920bd13](https://gitlab.psi.ch/bec/scibec/commit/920bd13f7d295389ba50152970411638535f5524))

## [1.6.1](https://gitlab.psi.ch/bec/scibec/compare/v1.6.0...v1.6.1) (2023-09-09)

### Bug Fixes

- added experimentId to scan model ([da655de](https://gitlab.psi.ch/bec/scibec/commit/da655de1f8632e5ce6826cf7404b2e21f682c625))

# [1.6.0](https://gitlab.psi.ch/bec/scibec/compare/v1.5.0...v1.6.0) (2023-08-28)

### Features

- updated to new db auth model ([c02d466](https://gitlab.psi.ch/bec/scibec/commit/c02d466633801cebb888f6b7e7587a42b76012ef))

# [1.5.0](https://gitlab.psi.ch/bec/scibec/compare/v1.4.2...v1.5.0) (2023-08-24)

### Features

- added py-scibec client ([d21ab24](https://gitlab.psi.ch/bec/scibec/commit/d21ab241979a8bfa414f6407fa75bb80ceaebc9d))

## [1.4.2](https://gitlab.psi.ch/bec/scibec/compare/v1.4.1...v1.4.2) (2023-08-22)

### Bug Fixes

- fixed config loading ([d3819b1](https://gitlab.psi.ch/bec/scibec/commit/d3819b172693cf49e9084fcc6ce857832bc05115))

## [1.4.1](https://gitlab.psi.ch/bec/scibec/compare/v1.4.0...v1.4.1) (2023-08-22)

### Bug Fixes

- fixed port and datasource loading ([1170c24](https://gitlab.psi.ch/bec/scibec/commit/1170c24fe49dcbdedf9321dfa7f5ee6cbb6a89e2))

# [1.4.0](https://gitlab.psi.ch/bec/scibec/compare/v1.3.7...v1.4.0) (2023-08-19)

### Features

- added prel. login screen; cleanup ([9fe874f](https://gitlab.psi.ch/bec/scibec/commit/9fe874fdea6a6e362e994c079dcc9ee9143aa19e))

## [1.3.7](https://gitlab.psi.ch/bec/scibec/compare/v1.3.6...v1.3.7) (2023-08-19)

### Bug Fixes

- fixed event repo mongo binding ([4e2c64b](https://gitlab.psi.ch/bec/scibec/commit/4e2c64bef711cfe5ebcacf909af58a643aa5e04f))

## [1.3.6](https://gitlab.psi.ch/bec/scibec/compare/v1.3.5...v1.3.6) (2023-08-17)

### Bug Fixes

- fixed path to openapi explorer ([2081167](https://gitlab.psi.ch/bec/scibec/commit/2081167717e11c151f13c8a325aa1cb9178dafe2))

## [1.3.5](https://gitlab.psi.ch/bec/scibec/compare/v1.3.4...v1.3.5) (2023-08-16)

### Bug Fixes

- testing versioning ([a25d4c4](https://gitlab.psi.ch/bec/scibec/commit/a25d4c48f622e4e1cc3dc497d3729b120a5ec999))

## [1.3.4](https://gitlab.psi.ch/bec/scibec/compare/v1.3.3...v1.3.4) (2023-08-16)

### Bug Fixes

- testing versioning ([3ea2481](https://gitlab.psi.ch/bec/scibec/commit/3ea24818c921c8eb86b0cd4d91e54dbeac20875f))

## [1.3.3](https://gitlab.psi.ch/bec/scibec/compare/v1.3.2...v1.3.3) (2023-08-16)

### Bug Fixes

- testing versioning ([30ed7dc](https://gitlab.psi.ch/bec/scibec/commit/30ed7dc07c117afd597b2ddc0123436bd129eb79))
- testing versioning ([1d421c1](https://gitlab.psi.ch/bec/scibec/commit/1d421c1af757facd32c4929d19c67174216d4085))
- testing versioning ([6b68b3a](https://gitlab.psi.ch/bec/scibec/commit/6b68b3a1e8c3ebd255f095f7801907ccd2b43b61))

## [1.3.2](https://gitlab.psi.ch/bec/scibec/compare/v1.3.1...v1.3.2) (2023-08-16)

### Bug Fixes

- testing versioning ([7b7e4a0](https://gitlab.psi.ch/bec/scibec/commit/7b7e4a08b60a6e99cbdc313a999b0e1664f6973a))
- testing versioning ([4f81deb](https://gitlab.psi.ch/bec/scibec/commit/4f81debf1d2a246c49f40de44ab737bc08da8bf9))

## [1.3.1](https://gitlab.psi.ch/bec/scibec/compare/v1.3.0...v1.3.1) (2023-08-16)

### Bug Fixes

- testing versioning ([c3fa83e](https://gitlab.psi.ch/bec/scibec/commit/c3fa83e8869f41dcf26ead9dfe52fe20675104c1))

# [1.3.0](https://gitlab.psi.ch/bec/scibec/compare/v1.2.0...v1.3.0) (2023-08-16)

### Features

- support for auth controller ([ead2c94](https://gitlab.psi.ch/bec/scibec/commit/ead2c94da27975b59a63c72c886ea997cef18485))

# [1.2.0](https://gitlab.psi.ch/bec/scibec/compare/v1.1.0...v1.2.0) (2023-08-15)

### Bug Fixes

- fixed releaserc file ([fc32474](https://gitlab.psi.ch/bec/scibec/commit/fc32474b456d72b4464648ef2ad88435da9e0f45))

### Features

- added changelog ([7448f0c](https://gitlab.psi.ch/bec/scibec/commit/7448f0ce8308cc912774472a67596d3e9db5fac1))
