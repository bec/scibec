
nvm use 18.17.1
export JAVA_HOME=/opt/homebrew/opt/openjdk@11
rm -rf ../sdk/python/py_scibec_openapi_client
npm run openapi-spec -o ./openapi.json
npx openapi-generator-cli version-manager set 7.4
npx openapi-generator-cli generate -g python -i openapi.json -o ../sdk/python/py_scibec_openapi_client --package-name py_scibec_openapi_client
