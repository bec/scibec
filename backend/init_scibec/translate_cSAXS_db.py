import ophyd
import ophyd.sim as ops
import ophyd_devices as opd
import yaml

# content = None
# with open("./new_cSAXS_config_epics.yaml", "r") as file:
#     file_content = file.read()
#     content = yaml.safe_load(file_content)


# out = {}
# for dev, conf in content.items():
#     out[dev] = {
#         "acquisitionConfig": {
#             "acquisitionGroup": conf["deviceGroup"],
#             "schedule": conf["acquisition"]["schedule"],
#             "readoutPriority": "baseline",
#         },
#         "deviceClass": conf["type"],
#         "deviceConfig": conf["config"],
#         "status": {"enabled": True, "enabled_set": False},
#         "deviceTags": ["cSAXS"],
#         "description": conf["desc"],
#         "onFailure": "buffer",
#     }

# with open("new_test_config_cSAXS.yaml", "w") as file:
#     file.write(yaml.dump(out))


def _get_device_class(dev_type):
    module = None
    if hasattr(ophyd, dev_type):
        module = ophyd
    elif hasattr(opd, dev_type):
        module = opd
    elif hasattr(ops, dev_type):
        module = ops
    else:
        TypeError(f"Unknown device class {dev_type}")
    return getattr(module, dev_type)


## add auto_monitor to epicssignal
content = None
with open("./test_lamni_config.yaml", "r") as file:
    file_content = file.read()
    content = yaml.safe_load(file_content)

# for dev, conf in content.items():
#     dev_class = _get_device_class(conf["deviceClass"])
#     if issubclass(dev_class, ophyd.Signal):
#         if "auto_monitor" not in conf["deviceConfig"]:
#             raise ValueError(f"{dev}")
#     if conf["deviceConfig"]["name"] != dev:
#         raise ValueError(f"{dev}")

with open("new_test_config_cSAXS_curly.yaml", "w") as file:
    file.write(yaml.dump(content))
