import { Component, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterOutlet } from '@angular/router';
import { AppConfigService } from './app-config.service';
import { HttpClientModule } from '@angular/common/http';

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [CommonModule, RouterOutlet, HttpClientModule],
  providers: [AppConfigService],
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss', '../styles.scss'],
})
export class AppComponent implements OnInit {
  title = 'BEC';
  light_mode = true;

  ngOnInit() {
    if (this.light_mode) {
      document.body.classList.add('light-mode');
      localStorage.setItem('light_mode', 'true');
    } else {
      // document.body.classList.remove('light-mode');
      localStorage.setItem('light_mode', 'false');
    }
  }
}
