export interface Scan {
  id?: string;
  scanType?: string;
  scanParameters?: object;
  scanId?: string;
  requestId?: string;
  queueId?: string;
  exitStatus?: string;
  scanNumber?: number;
  metadata?: object;
  files?: object;
  sessionId?: string;
  datasetId?: string;
  experimentId?: string;
  scanData?: object;
}
