import {
  HttpClientTestingModule,
  HttpTestingController,
} from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { HttpHeaders } from '@angular/common/http';

import { AuthDataService } from './remote-data.service';
import { AppConfigService } from '../app-config.service';

describe('AuthDataService', () => {
  let service: AuthDataService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [AuthDataService, AppConfigService],
    });
    service = TestBed.inject(AuthDataService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  afterEach(() => {
    httpMock.verify();
  });

  it('should send a POST request to login endpoint', () => {
    const principal = 'testuser';
    const password = 'testpassword';

    const expectedUrl = 'http://localhost:3000/users/login';
    const expectedHeaders = new HttpHeaders().set(
      'Content-Type',
      'application/json; charset=utf-8'
    );

    service.login(principal, password).subscribe();

    const req = httpMock.expectOne(expectedUrl);
    expect(req.request.method).toBe('POST');
    expect(req.request.headers).toEqual(expectedHeaders);
    expect(req.request.body).toEqual({ principal, password });

    req.flush({});
  });
});
