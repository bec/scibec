import { Component, Inject, OnInit } from '@angular/core';
import {
  UntypedFormBuilder,
  UntypedFormGroup,
  Validators,
} from '@angular/forms';
import {
  AppConfig,
  AppConfigService,
  Oauth2Endpoint,
} from '../app-config.service';
import { AuthService } from '../core/auth.service';
import { ActivatedRoute, Router } from '@angular/router';
import { DOCUMENT } from '@angular/common';
import { MatCardModule } from '@angular/material/card';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatTabsModule } from '@angular/material/tabs';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatIcon } from '@angular/material/icon';
import { ReactiveFormsModule } from '@angular/forms';
import { firstValueFrom } from 'rxjs';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-login',
  standalone: true,
  imports: [
    MatCardModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatTabsModule,
    MatTooltipModule,
    MatIcon,
    ReactiveFormsModule,
  ],
  templateUrl: './login.component.html',
  styleUrl: './login.component.scss',
})
export class LoginComponent implements OnInit {
  hide = true;
  form: UntypedFormGroup;
  loginMessage = ' ';
  appConfig: AppConfig = this.appConfigService.getConfig();
  oAuth2Endpoint: Oauth2Endpoint | undefined;

  constructor(
    private appConfigService: AppConfigService,
    private fb: UntypedFormBuilder,
    private authService: AuthService,
    private router: Router,
    private route: ActivatedRoute,
    @Inject(DOCUMENT) public document: Document
  ) {
    this.form = this.fb.group({
      email: ['', Validators.required],
      password: ['', Validators.required],
    });
    this.oAuth2Endpoint = this.appConfig.oAuth2Endpoint;
  }

  ngOnInit(): void {
    if (this.authService.forceReload) {
      window.location.reload();
    }
    this.route.queryParams.subscribe((params) => {
      if (params['token']) {
        this.authService.setSession({ token: params['token'] });
        this.router.navigateByUrl('/dashboard');
      }
    });
  }

  async login() {
    const val = this.form.value;
    if (val.email && val.password) {
      try {
        const data = await firstValueFrom(
          this.authService.login(val.email, val.password)
        );
        console.log(data);
        console.log('User is logged in');
        this.router.navigateByUrl('/dashboard');
      } catch (error: unknown) {
        switch ((error as HttpErrorResponse).statusText) {
          case 'Unknown Error':
            this.loginMessage = 'Authentication failed.';
            return;
          case 'Unauthorized':
            this.loginMessage =
              'User name / email or password are not correct.';
            return;
          default:
            this.loginMessage = 'Authentication failed.';
            return;
        }
      }
    }
  }

  redirectOIDC(authURL: string) {
    this.document.location.href = `${this.appConfig.lbBaseURL}/${authURL}/login`;
  }
}
