import { Component } from '@angular/core';
import { ToolbarComponent } from '../toolbar/toolbar.component';
import { SidenavComponent } from '../sidenav/sidenav.component';
import { Router } from '@angular/router';
@Component({
  selector: 'app-dashboard',
  standalone: true,
  templateUrl: './dashboard.component.html',
  styleUrl: './dashboard.component.scss',
  imports: [ToolbarComponent, SidenavComponent],
})
export class DashboardComponent {
  constructor(private router: Router) {
    this.router.navigate(['/dashboard/scan-table']);
  }
}
