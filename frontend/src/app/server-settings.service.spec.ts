import { TestBed } from '@angular/core/testing';

import { ServerSettingsService } from './server-settings.service';
import { AppConfigService } from './app-config.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { HttpClient, HttpHandler } from '@angular/common/http';

describe('ServerSettingsService', () => {
  let service: ServerSettingsService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        AppConfigService,
        HttpClientTestingModule,
        HttpClient,
        HttpHandler,
      ],
    });
    service = TestBed.inject(ServerSettingsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
  it('getSocketAddress should return string with leading ws', () => {
    expect(service.getSocketAddress().substring(0, 2)).toBe('ws');
  });
});
