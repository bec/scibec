import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ScanTableComponent } from './scan-table.component';
import { ScanDataService } from '../core/remote-data.service';

describe('ScanTableComponent', () => {
  let component: ScanTableComponent;
  let fixture: ComponentFixture<ScanTableComponent>;

  //eslint-disable-next-line
  const scanDataServiceSpy: jasmine.SpyObj<any> = jasmine.createSpyObj<any>(
    'ScanDataService',
    ['getScans']
  );
  scanDataServiceSpy.getScans.and.returnValue(Promise.resolve([]));
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ScanTableComponent],
      providers: [{ provide: ScanDataService, useValue: scanDataServiceSpy }],
    }).compileComponents();

    fixture = TestBed.createComponent(ScanTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
