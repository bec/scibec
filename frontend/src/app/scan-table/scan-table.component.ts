import { Component, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatTableModule } from '@angular/material/table';
import { ScanDataService } from '../core/remote-data.service';
import { firstValueFrom } from 'rxjs';
import { Scan } from '../core/model/scan';
@Component({
  selector: 'app-scan-table',
  standalone: true,
  imports: [MatTableModule, CommonModule],
  templateUrl: './scan-table.component.html',
  styleUrl: './scan-table.component.scss',
})
export class ScanTableComponent implements OnInit {
  data: Array<Scan> = [];
  displayedColumns: string[] = ['id', 'scanNumber', 'scanId', 'exitStatus'];

  constructor(private scanData: ScanDataService) {}

  ngOnInit(): void {
    console.log('ScanTableComponent initialized');

    this.getData();
  }

  async getData() {
    this.data = await firstValueFrom(this.scanData.getScanData());
    console.log(this.data);
  }
}
